module.exports = {
  parse: {
    serverURL: "http://localhost:1337/parse",
    appID: "appid",
    javascriptKey: "javakey",
    masterKey: "myMasterKey"
  },

  mailservice: {
    from: 'Vigile - Consulta de procesos judiciales <lebufesolution@gmail.com>',
    server: 'http://localhost:4200',
    confirmPage: 'http://localhost:4200/#/reg/confirmPage;key=',
    processDetailPage: 'http://localhost:4200/#/pages/processes/lista;objectId=',
  },
  facebook: {
    clientID: 'get_your_own',
    clientSecret: 'get_your_own',
    callbackURL: 'http://127.0.0.1:1337/auth/facebook/callback'
  },
  twitter: {
    consumerKey: 'get_your_own',
    consumerSecret: 'get_your_own',
    callbackURL: "http://127.0.0.1:1337/auth/twitter/callback"
  },
  github: {
    clientID: 'get_your_own',
    clientSecret: 'get_your_own',
    callbackURL: "http://127.0.0.1:1337/auth/github/callback"
  },
  google: {
    clientID: "get_your_own",
    clientSecret: "get_your_own-get_your_own",
    callbackURL: "http://localhost:4200/api/auth/google/callback"
  },
  instagram: {
    clientID: 'get_your_own',
    clientSecret: 'get_your_own',
    callbackURL: 'http://127.0.0.1:1337/auth/instagram/callback'
  },

  db: {
    url: "mongodb://localhost:27017/test"
  },

  emailAdmins : ["jcachuryb@outlook.com", "jcachury@htsoft.co"],

  pages : {
    root : "localhost:4200/#/",
    confirmarCuenta: 'registro/confirmar-correo/',
    processDetailPage: 'http://localhost:4200/#/pages/processes/lista;objectId=',
    adminFirmas : "pages/firmas",
    cambiarClave: "recuperar-clave/confirmacion/",
    tarifas: "inicio/planes",
  }

};

/*
  parse: {
    serverURL: "https://parseapi.back4app.com",
    appID: "pjb7nQ2yzedq7jWOrrUwbPnM3n7P1GIgVjj6oTQV",
    javascriptKey: "sKC46IhcuGCettGvXan8eLaj8WoFGAHQPnh5wcSA",
    masterKey: "eTyHAREOtqGSElIYRkvBrar0jPePWb4k9LDMnOp3"
  },


*/
