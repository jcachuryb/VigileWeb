function Board() {
  this.id = "";
  this.nombre;
  this.activo;
  this.company;
  this.fecha;
}
Board.prototype.cargarDatos = function (obj) {
  this.id = obj.id;
  this.nombre = obj.get("nombre");
  this.activo = obj.get("activo");
  this.company = obj.get("company");
  this.fecha = obj.get("fecha");

};
module.exports = Board;

