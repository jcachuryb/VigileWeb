function Usuario() {
  this.id = "";
  this.nombre;
  this.email;
  this.emailVerified;
  this.createdAt;
  this.datos = [];

  this.companyId;
  this.company;
  this.roles = [];

  this.cuenta = {}
}
Usuario.prototype.cargarDatos = function (obj) {
  this.id = obj.id;
  this.nombre = obj.get("nombre");
  this.email = obj.get("username");
  this.emailVerified = obj.get("activo");
  this.createdAt = obj.createdAt;

  this.datos = obj.get("datos");
  if (obj.get("company")) {
    this.companyId = obj.get("company").id;
    this.company = obj.get("company");
  }

};
module.exports = Usuario;

