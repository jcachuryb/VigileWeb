function MSession(sessionid) {
    this.id = sessionid;
    this.data = {
        user: {},
        roles: [],
        company: {},
        persona: {}
    };
    this.token = "";

}
// class methods
MSession.prototype.fooBar = function () {

};
// export the class
module.exports = MSession;