module.exports = function(mongoose) {
    var Schema = mongoose.Schema;
    var ObjectId = Schema.ObjectId;
    var Proceso = new Schema({
        processId           :    {type: String, index: true},
        id                  :    ObjectId,
        ciudad              :    String,
        estado              :    String,
        colors              :    {
            colorName       :    String,
            colorId         :    String,
            surcharge       :    Number
        }
    });

    var Persona = new Schema({
        //name                :    {type: String, index: true},
        id                  :    ObjectId,
        documento           :    String,
        nombres              :    String,
        apellidos            :    String,
        telefono            :    String,
        direccion           :    String,
        tipo_documento      :    String,
        genero              :    String,
        colors              :    {
            colorName       :    String,
            colorId         :    String,
            surcharge       :    Number
        }
    });


    var models = {
      proceso : mongoose.model('Process', Proceso),
      persona : mongoose.model('Persona', Persona),
      user : mongoose.model('User', User)
    };
    return models;
}