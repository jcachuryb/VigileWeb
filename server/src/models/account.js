function Account() {
    this.id = null;
    this.estado = "";
    this.active = false;
    this.limit = 20;
    this.usuarios = 0;
    this.tipo = "";
    this.activation = null;
    this.ends = null;
    this.siguientes = [];
    this.historial = [];
}
Account.prototype.cargarDatos = function (obj) {
    this.id = obj.id;
    this.estado = obj.get("estado");
    this.active = obj.get("active");
    this.limit = obj.get("limit");
    this.usuarios = obj.get("usuarios");
    this.activation = obj.get("activation");
    this.ends = obj.get("ends");
    this.tipo = obj.get("tipo");
    this.siguientes = obj.get("siguientes");
    this.historial = obj.get("historial");
};
module.exports = Account;