var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var personSchema = mongoose.Schema({
    name: String,
    lastname: String, 
    email: String,
    phonenumber: String, 
    pictureUrl: String
});

personSchema.methods.speak = function () {
  var greeting = this.name
    ? "My name is " + this.name + " " + this.lastname
    : "I don't have a name";
  console.log(greeting);
}


var Person = mongoose.model('Person', personSchema);