function Plan() {
    this.id = null;
    this.nombre = "";
    this.procesos = 0;
    this.usuarios = 0;
    this.valorProceso = 0;
    this.valorUsuario = 0;
    this.duracion = 0;
}
Plan.prototype.cargarDatos = function (obj) {
    this.id = obj.id;
    this.nombre = obj.get("nombre");
    this.procesos = obj.get("procesos");
    this.usuarios = obj.get("usuarios");
    this.valorProceso = obj.get("valorProceso");
    this.valorUsuario = obj.get("valorUsuario");
    this.duracion = obj.get("duracion");
};
module.exports = Plan;