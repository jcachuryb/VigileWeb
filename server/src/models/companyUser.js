function CompanyUser() {
    this.id = null;
    this.estado = "";
    this.isOwner = false;
    this.activo = false;
    this.permisos = null;
    this.nombreFirma = "";

    this.userId = null;
    this.companyId = null;
}
CompanyUser.prototype.cargarDatos = function (obj) {
    this.id = obj.id;
    this.estado = obj.get("estado");
    this.isOwner = obj.get("isOwner");
    this.activo = obj.get("activo");
    this.permisos = obj.get("permisos");

    this.userId = obj.get("user").id;
    this.companyId = obj.get("company").id;
    var companyData = obj.get("company");
    try {
        this.nombreFirma = companyData.get("nombre");
    } catch (error) {

    }
};
module.exports = CompanyUser;