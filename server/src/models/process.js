var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var processSchema = mongoose.Schema({
    processId: String,
    lastname: String, 
    email: String,
    phonenumber: String, 
    pictureUrl: String
});


var Process = mongoose.model('Process', processSchema);