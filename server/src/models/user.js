'use-strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


const userSchema = new Schema({
  name: { type: String, default: '' },
  email: { type: String, default: '' },
  username: { type: String, default: '' },
  provider: { type: String, default: '' },
  hashed_password: { type: String, default: '' },
  salt: { type: String, default: '' },
  authToken: { type: String, default: '' },
  facebook: {},
  twitter: {},
  github: {},
  google: {},
  linkedin: {}
});


userSchema.statics = {

  /**
   * Load
   *
   * @param {Object} options
   * @param {Function} cb
   * @api private
   */

  load: function (options, cb) {
    options.select = options.select || 'name email';
    //console.log("LOADING " + options.select);
    return this.findOne(options.criteria)
      .select(options.select)
      .exec(cb);
  },

  
};


var User = mongoose.model('User', userSchema);