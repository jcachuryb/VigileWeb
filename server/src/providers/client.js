const db = require("parse/node");
const _ = require("lodash");
const TABLE_COMPANY = "Company";
const TABLE_ACCOUNT = "Account";
const TABLE_CATEGORY = "Category";
const TABLE_PROCESS = "Process";

Client = {

    getCompanyProcessSummary: function (company) {
        var total = getTotalQuery(company);
        var errors = getErrorsQuery(company);
        var active = getActiveQuery(company);
        var recentlyAdded = getNewsQuery(company);
        var limiteProcesos = getLimitQuery(company);

        var promises = [];
        promises.push(total.count());
        promises.push(active.count());
        promises.push(errors.count());
        promises.push(recentlyAdded.count());
        promises.push(limiteProcesos.first());

        return db.Promise.when(promises).then(function (result) {
            var orale = 2-1;
            var data = { total: result[0], activos: result[1], error: result[2], nuevos: result[3], limite: result[4].get("limit") };
            return data;

        }, function (error) {
            console.log(error);
            return error;
        });


    },

    getCompanyById: function (companyid) {
        var u = db.Object.extend(TABLE_COMPANY);
        var query = new db.Query(u);
        query.include("cuenta");
        return query.get(companyid, {
            success: function (result) {
                return result;
            },
            error: function (err) {
                console.log(err);
                return err;
            }
        })
    },

    canAddProcesses: function (companyid, numIn) {
        var a = db.Object.extend(TABLE_ACCOUNT);
        var query = new db.Query(a);
        query.equalTo("companyId", companyid);
        query.select(['active', 'companyId', 'limit']);

        var promises = [];
        promises.push(query.first());
        promises.push(getTotalQuery(companyid).count());

        return db.Promise.when(promises).then(function (result) {
            var account = result[0];
            var total = result[1];
            var msg = "", status = "", canAdd = false;
            if (account.attributes.active) {
                canAdd = account.attributes.limit >= total + numIn;
                msg = canAdd ? "" : "Excede el límite";
                status = canAdd ? "success" : "exceeds";
            } else {
                canAdd = false;
                msg = "No puede agregar procesos con una cuenta inactiva.";
                status = "inactive";
            }
            return { msg: msg, status: status, success:  canAdd};
        }, function (error) {
            console.log(error);
            return error;
        });

    },

    updateClientCompany: function (companyId, data){
        var a = db.Object.extend(TABLE_COMPANY);
        var query = new db.Query(a);
        query.equalTo("objectId", companyId);
        query.select([]);
        return query.first().then(result=>{
            result.set("nombre", data.nombre);
            result.set("telefonos", [data.telefono]);
            result.set("ciudad", data.ciudad);
            result.set("documento", data.documento);
            result.set("configurada", true);
            return result.save();
        }).catch(error=>{
            console.log(error);
        })
    },

    getRoles: function (userId) {
        // Get company data


        var r = pObject.relation(relation);
        return r.query().find({
            success: function (roles) {
                return roles;
            },
            error: function (err) {
                return [];
            }

        })
    }
}

module.exports = Client;

function getErrorsQuery(company) {
    var query = new db.Query(TABLE_PROCESS);
    query.equalTo("company", company);
    query.exists("problem.msg");
    return query;
}
function getTotalQuery(company) {
    var query = new db.Query(TABLE_PROCESS);
    query.equalTo("company", company);
    return query;
}

function getActiveQuery(company) {
    var query = new db.Query(TABLE_PROCESS);
    query.equalTo("company", company);
    query.equalTo("activo", true);
    query.exists("estadoActual.actuacion");
    return query;
}

function getNewsQuery(company) {
    var query = new db.Query(TABLE_PROCESS);
    query.equalTo("company", company);
    query.doesNotExist("estadoActual.actuacion");
    query.doesNotExist("problem.msg");
    return query;
}

function getLimitQuery(company) {
    var query = new db.Query(TABLE_ACCOUNT);
    query.equalTo("companyId", company);
    query.select(['limit']);
    return query;
}