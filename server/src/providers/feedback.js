const db = require("parse/node");
const _ = require("lodash");
const t_Company = "Company";
const t_Account = "Account";
const t_Category = "Category";
const t_User = "Usuario";
const t_Process = "Process";
const t_ProcessStates = "ProcessStates";
const TABLE_FEEDBACK = "Feedback";
const notifier = require('./notifiers/notifier');



Feedback = {
    addFeedback: function (correo, mensaje, tipo, data) {
        var objClass = db.Object.extend(TABLE_FEEDBACK);
        var p = new objClass();
        p.set("mensaje", mensaje);
        p.set("tipo", tipo);
        p.set("email", correo);
        p.set("url", data.url);
        return p.save();
    },
}


module.exports = Feedback;


function dd(variable, varName) {
    var varNameOutput;

    varName = varName || '';
    varNameOutput = varName ? varName + ':' : '';

    console.warn(varNameOutput, variable, ' (' + (typeof variable) + ')');
}