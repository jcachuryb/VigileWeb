const db = require("parse/node");
const _ = require("lodash");
const notifier = require("./notifiers/notifier");
const t_Company = "Company";
const t_Account = "Account";
const t_Category = "Category";
const t_User = "User";
const t_Process = "Process";
const t_UserCompany = "UserCompany";



Company = {

    loadActiveAccount: function (companyId) {
        var companies = [];
        var o = db.Object.extend(t_Company);
        var query = new db.Query(o);
        query.equalTo("company", companyId);
        query.include("account");
        return query.each(function (val) {
            var active = val.attributes.account.attributes.active;
            if (active) companies.push(val);
        }).then(res => {
            return companies;
        });
    },

    loadActiveAccounts: function () {
        var companies = [];
        var o = db.Object.extend(t_Account);
        var query = new db.Query(o);
        query.equalTo("active", true);
        query.greaterThanOrEqualTo("ends", new Date());
        query.select(['limit', 'companyId']);
        return query.find();
    },

    loadCompany(companyId) {
        return loadCompany(companyId);
    },

    canAddUser(companyId, userEmail) {
        var canAdd = true;
        var promises = [];
        promises.push(loadCompany(companyId));
        promises.push(loadUsers(companyId));

        return db.Promise.when(promises).then(result => {
            var sMsg = { msg: "", success: true };
            var usuarios = result[1];
            var company = result[0];
            if (usuarios && company) {
                var limite = company.get("cuenta").get("usuarios");
                var numUsers = usuarios.length;
                if (numUsers > limite) {
                    sMsg.msg = "No puede agregar más usuarios a esta cuenta";
                    sMsg.success = false;
                    return sMsg;
                }
                _.forEach(usuarios, function (usuario) {
                    var email = usuario.attributes.user.attributes.username;
                    if (email === userEmail) {
                        sMsg.msg = "El usuario ya está asociado a esta cuenta";
                        sMsg.success = false;
                        return;
                    }
                });
                return sMsg;
            } else {
                return { msg: "Ocurrió un error", success: false };
            }
        }).catch(err => {
            return { msg: "Ocurrió un error", success: false };
        })
    },

    loadCompanyUsers(companyId) {
        return loadUsers(companyId).then(values => {
            var usersArray = [];
            _.forEach(values, function (usuarioFirma) {
                var finalUser = cargarDatosUsuarioFirmaDto(usuarioFirma);
                usersArray.push(finalUser);
            });
            return usersArray;
        }).catch(err => {
            console.log("Error: " + err);
            return [];
        });
    },

    loadSelectedUserCompany(userId, companyId) {
        var promises = [];
        promises.push(queryFetchUser(userId));
        promises.push(queryFetchCompany(companyId));

        return db.Promise.when(promises).then(result => {
            var user = result[0];
            var company = result[1];

            var obj = db.Object.extend(t_UserCompany);
            var query = new db.Query(obj);

            query.equalTo("company", company);
            query.equalTo("user", user);
            query.include("company");
            // query.equalTo("activo", true);
            return query.first();
        }).catch(err => {
            console.log("Error cargando el objeto user Company: " + err);
            return null;
        })
    },

    loadSelectedUserCompanyById(objectId) {
        return loadSelectedUserCompanyById(objectId);
    },

    loadUserCompanies(userId) {
        return queryFetchUser(userId).then(user => {
            var u = db.Object.extend(t_UserCompany);
            var query = new db.Query(u);
            query.equalTo("user", user);
            query.include("company");
            // query.equalTo("activo", true);
            return query.find().then(result => {
                var c = [];
                var invitations = false;
                _.forEach(result, function (uc) {
                    var finalCompany = {};
                    if (uc.attributes.activo) {
                        finalCompany.company = {
                            id: uc.attributes.company.id,
                            nombre: uc.attributes.company.attributes.nombre,
                            telefonos: uc.attributes.company.attributes.telefonos
                        }

                        finalCompany["estado"] = uc.attributes.estado;
                        finalCompany["activo"] = uc.attributes.activo;
                        finalCompany["permisos"] = uc.attributes.permisos;
                        finalCompany["isOwner"] = uc.attributes.isOwner;
                        finalCompany["createdAt"] = uc.createdAt;
                        finalCompany["id"] = uc.id;
                        c.push(finalCompany);
                    }
                    if (uc.attributes.estado === 'Pendiente') {
                        invitations = true;
                    }
                });
                return { companies: c, pendientes: invitations };
            });
        })
    },

    addUserToCompany(companyId, userEmail, data, isOwner) {
        var estado = isOwner ? "Activo" : "Pendiente";
        var activo = isOwner;

        var query1 = new db.Query(db.Object.extend(t_User));
        var query2 = new db.Query(db.Object.extend(t_Company));
        query1.equalTo("username", userEmail);
        query2.equalTo("objectId", companyId);

        var promises = [];
        promises.push(query1.first());
        promises.push(query2.first());

        return db.Promise.when(promises).then(result => {
            var usercompClass = db.Object.extend(t_UserCompany);
            var newUser = new usercompClass();
            newUser.set("company", result[1]);
            newUser.set("user", result[0]);
            newUser.set("permisos", data.permisos);
            newUser.set("estado", estado);
            newUser.set("activo", activo);
            newUser.set("isOwner", isOwner);

            return newUser.save();
        }).then(user => {
            var user1 = user;
            return loadSelectedUserCompanyById(user1.id);
        }).then(usuarioFirma => {
            var company = usuarioFirma.get("company");
            notifier.notificarInvitacion(usuarioFirma, company, userEmail);
            return cargarDatosUsuarioFirmaDto(usuarioFirma);
        }).catch(error => {
            console.log(error);
            return null;
        });
    },

    updateUserCompany(companyId, userId, data) {
        console.log(userId, companyId);
        return this.loadSelectedUserCompany(userId, companyId).then(res => {
            res.set("permisos", data.permisos);
            return res.save();
        }).then(usuarioFirma => {
            return cargarDatosUsuarioFirmaDto(usuarioFirma);
        }).catch(error => {
            console.log(error);
            return null;
        });
    },

    respondCompanyInvitation(userId, userCompanyId, response) {
        return queryFetchUser(userId).then(user => {
            var u = db.Object.extend(t_UserCompany);
            var query = new db.Query(u);
            query.equalTo("user", user);
            query.equalTo("objectId", userCompanyId);
            return query.first().then(result => {
                console.log("Respondiendo")
                var estado = response ? "Activo" : "Rechazado";
                result.set("estado", estado);
                result.set("activo", response);
                return result.save();
            }).catch(err => {
                console.log(err);
                return false;
            });
        });
    },

    cancelUserInvitation(selectedCompany, userId) {
        return loadSelectedUserCompanyById(userId).then(usuario => {
            var estado = usuario.get("estado");
            if (selectedCompany != usuario.get("company").id) {
                return false;
            }

            if (estado === "Pendiente") {
                return usuario.destroy()
            }
            return false;
        });
    },

    removeFromCompany(companyId, userId) {
        return loadUserCompany(userId, companyId).then(usuario => {
            if (!usuario) {
                return false;
            }
            if (usuario.get("isOwner")) {
                return false;
            }
            return usuario.destroy();
        });
    },

    loadCompanyInvitations(userId) {
        return queryFetchUser(userId).then(user => {
            var u = db.Object.extend(t_UserCompany);
            var query = new db.Query(u);
            query.equalTo("user", user);
            query.include("company");
            query.equalTo("estado", "Pendiente");
            return query.find().then(result => {
                var c = [];
                _.forEach(result, function (uc) {
                    var finalCompany = {};
                    finalCompany.company = {
                        id: uc.attributes.company.id,
                        nombre: uc.attributes.company.attributes.nombre,
                        telefonos: uc.attributes.company.attributes.telefonos
                    }

                    finalCompany["estado"] = uc.attributes.estado;
                    finalCompany["activo"] = uc.attributes.activo;
                    finalCompany["permisos"] = uc.attributes.permisos;
                    finalCompany["isOwner"] = uc.attributes.isOwner;
                    finalCompany["createdAt"] = uc.createdAt;
                    finalCompany["id"] = uc.id;
                    c.push(finalCompany);

                });
                return c;
            });
        })
    },

    getUserPermissions(userId, companyId) {
        return this.loadSelectedUserCompany(userId, companyId).then(result => {
            return result.attributes.permisos;
        });
    },

    removeUserCompany(companyId, userId) {
        return true;
    },

    removeUserSuscriptionsFromCompany(userId, companyId) {
        var obj = db.Object.extend(t_Process);
        var query = new db.Query(obj);
        query.equalTo("suscriptores.userId", userId);
        query.select("suscriptores");
        query.equalTo("company", companyId)
        return query.find().then(procesos => {
            _.each(procesos, x => {
                var s = x.get("suscriptores");
                s = _.pullAllBy(s, [{ 'userId': userId}], 'userId');
                x.set("suscriptores", s);
            })
        return db.Object.saveAll(procesos);
        });
    }

}


module.exports = Company;

function loadCompany(companyId) {
    var obj = db.Object.extend(t_Company);
    var query = new db.Query(obj);
    query.equalTo("objectId", companyId);
    query.include("cuenta");
    return query.first();
}
function loadUsers(companyId) {
    var promise = loadCompany(companyId);
    return promise.then(result => {
        var obj = db.Object.extend(t_UserCompany);
        var query = new db.Query(obj);
        query.equalTo("company", result);
        query.include("user");
        query.include("company");
        return query.find();
    });
}

function loadSelectedUserCompanyById(objectId) {
    var obj = db.Object.extend(t_UserCompany);
    var query = new db.Query(obj);

    query.equalTo("objectId", objectId);
    query.include("company");
    query.include("user");
    return query.first();
}

function loadUserCompany(userId, companyId) {
    var promises = [];
    promises.push(queryFetchUser(userId));
    promises.push(queryFetchCompany(companyId));

    return db.Promise.when(promises).then(result => {
        var user = result[0];
        var company = result[1];

        var obj = db.Object.extend(t_UserCompany);
        var query = new db.Query(obj);

        query.equalTo("company", company);
        query.equalTo("user", user);
        query.include("company");
        return query.first();
    }).catch(err => {
        console.log("Error cargando el objeto user Company: " + err);
        return null;
    })
}

function cargarDatosUsuarioFirmaDto(usuarioFirma) {
    var finalUser = {};
    var user = usuarioFirma.get("user");
    var company = usuarioFirma.get("company");
    finalUser.id = usuarioFirma.id;
    finalUser.usuarioId = user.id;
    finalUser.firmaId = company.id;
    finalUser.correo = user.get("username");
    finalUser.nombre = user.get("nombre");
    finalUser.activo = usuarioFirma.get("activo");
    finalUser.permisos = usuarioFirma.get("permisos");
    finalUser.isOwner = usuarioFirma.get("isOwner");
    finalUser.agregado = usuarioFirma.get("createdAt");
    finalUser.estado = usuarioFirma.get("estado");
    return finalUser;
}


function queryFetchUser(userId) {
    var u = db.Object.extend(t_User);
    var query = new db.Query(u);
    query.equalTo("objectId", userId);
    return query.first();
}

function queryFetchCompany(companyId) {
    var u = db.Object.extend(t_Company);
    var query = new db.Query(u);
    query.equalTo("objectId", companyId);
    return query.first();
}