const db = require("parse/node");
const _ = require("lodash");
const userProvider = require("./user");
const accountNotifier = require("./notifiers/accountNotifier");
const TABLE_USER = "User";
const TABLE_ACCOUNT = "Account";
const TABLE_CATEGORY = "Category";
const TABLE_COMPANY = "Company";
const TABLE_PROCESS = "Process";
const TABLE_PERSON = "Person";
const TABLE_PROCESS_STATES = "ProcessStates";
const TABLE_USER_COMPANY = "UserCompany";
const TABLE_USER_ROLE = "Role";

AccountProvider = {

    loadAccountInfo: function (companyId) {
        return loadAccount(companyId);
    },

    isAccountActive: function (companyId) {
        return loadAccount(companyId).then(cuenta => {
            if (cuenta) {
                return cuenta.get("active");
            }
            return null;
        })
    },

    loadSelCompanyAccountInfo: function (companyId) {
        return loadAccount(companyId).then(value => {
            if (!value) {
                return null;
            }
            var cuentaDto = {};
            cuentaDto.id = value.id;
            cuentaDto.starts = value.get("starts");
            cuentaDto.ends = value.get("ends");
            cuentaDto.active = value.get("active");
            cuentaDto.limit = value.get("limit");
            cuentaDto.usuarios = value.get("usuarios");
            return cuentaDto
        });
    },

    activarSuscripcion: function (plan, userId) {
        return fetchUserById(userId).then(user => {
            if (user) {
                if (user.get("company").id) {
                    return loadAccount(user.get("company").id).then(acc => {

                        if (acc) {
                            var date = new Date();
                            var newDate = new Date(date.getTime() + plan.duracion * 86400000);
                            acc.set("active", true);
                            acc.set("tipo", plan.nombre);
                            acc.set("activation", date);
                            acc.set("ends", newDate);
                            acc.set("limit", plan.procesos);
                            acc.set("usuarios", plan.usuarios);
                            // acc.set("siguientes", []);
                            // acc.set("historial", []);
                            return acc.save().then(res => {
                                if (res) {
                                    var data = {};
                                    data.datos = {};
                                    data.nombre = user.get("nombre");
                                    data.datos.email = user.get("username");
                                    data.datos.plan = plan.nombre;
                                    data.datos.procesos = plan.procesos;
                                    data.datos.usuarios = plan.usuarios;
                                    data.datos.finaliza = formatDate(newDate);

                                    const formatter = new Intl.NumberFormat('en-US', {
                                        style: 'currency',
                                        currency: 'USD',
                                        minimumFractionDigits: 0
                                    })

                                    data.datos.precio = formatter.format(plan.precio);
                                    return data;
                                }
                            });

                        }
                    });
                }
            }
        })
    },

    deactivateAccounts: function (page, num) {

        var clase = db.Object.extend(TABLE_ACCOUNT);
        var query = new db.Query(clase);
        query.equalTo("active", true);
        query.lessThan("ends", new Date());
        query.limit(num);
        query.skip(page * num);
        return query.find().then(list => {
            if (list) {
                _.forEach(list, c => {
                    console.log("Desactivar cuenta " + c.id);
                    console.log("Porque acabó el " + c.get("ends"));
                    console.log(" ------------------------------  ");
                    // c.set("active", false);
                    enviarCorreoFinSuscripcion(c.get("companyId"));
                    // c.save();
                })

                return list.length;
            } else {
                return 0;
            }
        })
    }

}

function loadAccount(companyId) {
    var clase = db.Object.extend(TABLE_COMPANY);
    var query = new db.Query(clase);
    query.equalTo("objectId", companyId);
    query.include("cuenta");
    query.select("cuenta");
    return query.first().then(company => {
        if (company) {
            return company.get("cuenta");
        }
        return null;
    }).catch(error => {
        console.log(error);
        return null;
    });
}

function fetchUserById(userId) {
    var u = db.Object.extend(TABLE_USER);
    var query = new db.Query(u);
    query.equalTo("objectId", userId);
    return query.first();
}

function fetchContactDetailsByCompanyId(companyId) {
    var u = db.Object.extend(TABLE_COMPANY);
    var query = new db.Query(u);
    query.equalTo("objectId", companyId);
    return query.first().then(company => {
        if (company) {
            var u = db.Object.extend(TABLE_USER);
            var query = new db.Query(u);
            query.equalTo("company", company);
            query.select(["username",]);
            return query.first().then(user => {
                var datos = {};
                if (user) {
                    datos.email = user.get("username");
                }
                return datos;
            });
        }
    });
}

function enviarCorreoFinSuscripcion(companyId) {
    fetchContactDetailsByCompanyId(companyId).then(contacto => {
        if (contacto.email) {
            // accountNotifier.finSuscripcion(contacto.email, {});
            console.log("Enviar correo a " + contacto.email);
        }
    })
}

function fetchAccountByCompanyId(companyId) {
    var u = db.Object.extend(TABLE_ACCOUNT);
    var query = new db.Query(u);
    query.equalTo("companyId", companyId);
    return query.first();
}

function formatDate(date) {
    var monthNames = [
        "Enero", "Febrero", "Marzo",
        "Abril", "Mayo", "Junio", "Julio",
        "Agosto", "Septiembre", "Octubre",
        "Noviembre", "Diciembre"
    ];

    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();

    return day + ' de ' + monthNames[monthIndex] + ' de ' + year;
}


module.exports = AccountProvider;