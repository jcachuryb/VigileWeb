const db = require("parse/node");
const _ = require("lodash");
const userProvider = require("./user");
const accountNotifier = require("./notifiers/accountNotifier");
const TABLE_USER = "User";
const TABLE_TASK = "Task";


TaskProvider = {

    initTask: function (tipo) {
        var clase = db.Object.extend(TABLE_TASK);
        var task = new clase();
        task.set("tipo", tipo);
        task.set("inicio", new Date());
        return task.save().then(x => {
            if (x) {
                return x.id;
            } else {
                return null;
            }
        })
    },

    endTask: function (taskId, exito, comentarios, descartar) {
        var clase = db.Object.extend(TABLE_TASK);
        var query = new db.Query(clase);
        query.equalTo("objectId", taskId);
        return query.first().then(task => {
            if (task) {
                if (descartar) {
                    return task.destroy();
                } else {
                    task.set("fin", new Date());
                    task.set("exito", exito);
                    task.set("comentarios", comentarios);
                    return task.save();
                }
            }
        });
    },

    loadTasks: function (page, num) {
        var o = db.Object.extend(TABLE_TASK);
        var query = new db.Query(o);
        query.limit(num);
        query.skip(page * num);
        query.descending("createdAt");
        return query.find();
    },

    removeRecord: function (taskId) {
        var o = db.Object.extend(TABLE_TASK);
        var query = new db.Query(o);
        query.equalTo("objectId", taskId);
        return query.first().then(task => {
            if (task) {
                return task.destroy();
            }
        });
    },



}



module.exports = TaskProvider;