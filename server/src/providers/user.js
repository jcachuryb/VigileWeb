const db = require("parse/node");
const notifier = require('./notifiers/notifier');
const MENU = require('./menuProvider');
const TABLE_COMPANY = "Company";
const TABLE_ACCOUNT = "Account";
const TABLE_PERSON = "Person";
const TABLE_USER_COMPANY = "UserCompany";
const t_Category = "Category";
const t_User = "Usuario";
const ROLE_TABLE = "Role";
const USER_TABLE = "User";


User = {
    userExists: function (email) {
        return fetchUserByEmail(email).then(user => {
            if (user) {
                return true;
            }
            return false;
        }).catch(error=>{
            return false;
        })
    },

    loginCredentials: function (username, password, options) {
        return db.User.logIn(username, password);
    },
    signUp: function (email, password, initData) {
        var user = new db.User();
        user.set("username", email);
        user.set("password", password);
        user.set("email", email);
        // user.set("emailVerified", false);
        user.set("nombre", initData.nombre);
        user.set("datos", {});
        user.set("verificationKey", initData.key);

        return user.signUp(null, {
            success: function (user) {
                var role = db.Object.extend(ROLE_TABLE);
                var query = new db.Query(role);
                query.equalTo("name", "client");
                return query.first().then(result => {
                    var relation = user.relation("roles");
                    relation.add(result);
                    return user.save(null, { useMasterKey: true }).then(result => {
                        return true;
                    }).catch(err => {
                        console.log(err);
                        return false;
                    });
                })
            },
            error: function (user, error) {
                console.log(error);
            }
        });
    },
    cambiarClave: function (userId, nuevaClave) {
        return fetchUserById(userId).then(user => {
            if (user) {
                var datos = user.get("datos");
                delete datos.cambioClave;
                user.set("datos", datos);
                user.set("password", nuevaClave);
                return user.save(null, { useMasterKey: true }).then(result => {
                    return true;
                }).catch(err => {
                    console.log(err);
                    return false;
                });
            }
        })
    },
    solicitarRecuperarClave: function (email) {
        return fetchUserByEmail(email).then(user => {
            if (user) {
                var inicio = new Date().getTime();
                codigo = getRandomString(30) + inicio;
                var datos = user.get("datos") || {};
                datos.cambioClave = {};
                datos.cambioClave.codigo = codigo;
                datos.cambioClave.validoHasta = new Date(inicio + 2 * 86400000).getTime();
                user.set("datos", datos);
                return user.save(null, { useMasterKey: true }).then(u => {
                    return { userId: u.id, codigo };
                })
            }
            return false;
        }).catch(err => {
            return false;
        })
    },
    getInfoCambioClave: function (userId) {
        var cambioClave;
        return fetchUserById(userId).then(user => {
            if (user) {
                cambioClave = user.get("datos").cambioClave;
            }
            return cambioClave;
        }).catch(err => {
            return null;
        })
    },
    completarRegistro: function (key) {
        var u = db.Object.extend(USER_TABLE);
        var query = new db.Query(u);
        query.equalTo("emailVerified", undefined);
        query.equalTo("verificationKey", key);
        query.select(['username', 'email', 'nombre', 'verificationKey']);
        return query.first().then(usuario => {
            if (usuario) {
                usuario.set('emailVerified', true);
                return usuario.save(null, { useMasterKey: true }).then(result => {
                    return usuario;
                }).catch(err => {
                    console.log(err);
                    return null;
                });
            } else {
                return false;
            }
        }).catch(error => {
            console.log(error);
            return false;
        })
    },
    cargarMenu: function(userId, roles, permisos){
        var menu = new MENU(userId, roles, permisos);
        return menu.obtenerMenu();
    },
    getRoles: function (pObject, relation) {
        var r = pObject.relation(relation);
        return r.query().find({
            success: function (roles) {
                return roles;
            },
            error: function (err) {
                return [];
            }

        })
    },
    crearCompany: function (email) {
        try {
            var promises = [];
            var u = db.Object.extend(USER_TABLE);
            var queryUser = new db.Query(u);
            queryUser.equalTo("username", email);
            queryUser.doesNotExist("company");
            queryUser.select(['username', 'nombre', 'email']);

            var CompanyClass = db.Object.extend(TABLE_COMPANY);
            var company = new CompanyClass();
            company.set("nombre", "Sin nombre");
            company.set("documento", "");
            company.set("telefono", []);
            company.set("configurada", false);

            var PersonClass = db.Object.extend(TABLE_PERSON);
            var person = new PersonClass();
            person.set("nombrecompleto", "desconocido");

            promises.push(queryUser.first());
            promises.push(company.save());
            promises.push(person.save());

            return db.Promise.when(promises).then(results => {
                var user = results[0];
                var newCompany = results[1];
                var newPerson = results[2];
                if (!user) {
                    newCompany.destroy();
                    newPerson.destroy();
                    return false;
                }
                return makeNewAccount(newCompany.id).then(newAccount => {
                    var promises = [];
                    user.set("company", newCompany);
                    user.set("notifications", { "email": [email] })
                    newCompany.set("cuenta", newAccount);
                    newCompany.set("nombre", user.get('nombre'));
                    newPerson.set("nombrecompleto", user.get('nombre'));
                    newPerson.set("userId", user.id);
                    promises.push(user.save(null, { useMasterKey: true }));
                    promises.push(newCompany.save());
                    promises.push(newPerson.save());

                    return db.Promise.when(promises).then(result => {
                        var data = { nombre: user.get('nombre') };
                        var to = email;
                        notifier.notifNuevaCuenta(to, data);
                        var userCompanyClass = db.Object.extend(TABLE_USER_COMPANY);
                        var ucObject = new userCompanyClass();
                        ucObject.set("estado", "Activo");
                        ucObject.set("isOwner", true);
                        ucObject.set("activo", true);
                        ucObject.set("user", user);
                        ucObject.set("company", newCompany);
                        ucObject.set("permisos", ["admin"]);

                        return ucObject.save();
                    }).catch(error => {
                        console.log(err);
                        return false;
                    });
                })
            }).catch(err => {
                console.log(err);
                return false;
            })
        } catch (error) {
            return error;
        }
    },
    createPerson: function (person) {
        if (person) {
            var personClass = db.Object.extend("Person");
            var p = new personClass();
            p.set("name", person.name);
            p.set("lastname", person.lastname);
            p.set("tel", person.tel);
            p.set("document", person.document);
            p.set("email", person.email);
            p.save(null, {
                success: function (persona) {
                    console.log("Persona guardada ");
                },
                error: function (error) {
                    console.log('parse saving PERSON error');
                }
            });

        }
    },
    getUserNotificationChannels: function (userId) {
        var u = db.Object.extend(USER_TABLE);
        var queryUser = new db.Query(u);
        queryUser.equalTo("objectId", userId);
        queryUser.select(['notifications']);
        return queryUser.first().then(data => {
            return data.attributes.notifications;
        }).catch(err => {
            return null;
        });
    },
    getRandomString: function (length) {
        return getRandomString(length);
    }
}

module.exports = User;

function makeNewAccount(companyId) {
    var days = 15;
    var AccountClass = db.Object.extend(TABLE_ACCOUNT);
    var acc = new AccountClass();
    var date = new Date();
    var newDate = new Date(date.getTime() + days * 86400000);
    acc.set("companyId", companyId);
    acc.set("active", true);
    acc.set("tipo", "Gratuita");
    acc.set("activation", date);
    acc.set("ends", newDate);
    acc.set("limit", 15);
    acc.set("usuarios", 2);
    acc.set("siguientes", []);
    acc.set("historial", []);
    return acc.save();
}

function fetchUserByEmail(email) {
    var u = db.Object.extend(USER_TABLE);
    var query = new db.Query(u);
    query.equalTo("email", email);
    return query.first();
}

function fetchUserById(userId) {
    var u = db.Object.extend(USER_TABLE);
    var query = new db.Query(u);
    query.equalTo("objectId", userId);
    return query.first();
}

function getRandomString(length) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < length; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}