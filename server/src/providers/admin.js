const db = require("parse/node");
const _ = require("lodash");
const notifier = require("./notifiers/notifier");
const t_Company = "Company";
const t_Account = "Account";
const t_Category = "Category";
const t_User = "User";
const t_Roles = "Role";
const t_UserCompany = "UserCompany";

const UsuarioDto = require('../models/usuario');
const CuentaDto = require('../models/account');


Admin = {

    fetchUsers: function (filter) {
        var users = [];
        var o = db.Object.extend(t_User);

        var c = db.Object.extend(t_Roles);

        var queryUsers = new db.Query(o);
        queryUsers.startsWith("email", filter.email);
        queryUsers.select(["nombre", "username", "emailVerified"]);
        queryUsers.equalTo("emailVerified", true);
        queryUsers.exists("company");
        queryUsers.limit(filter.num);
        queryUsers.skip(filter.num * filter.page);
        return queryUsers.find().then(res => {
            if (res) {
                _.each(res, u=>{
                    var obj= {};
                    obj.id = u.id;
                    obj.nombre = u.get("nombre");
                    obj.email = u.get("username");
                    obj.creado = u.createdAt
                    users.push(obj);
                })
            }
            return users;
        });
    },

    fetchUserData: function (userId) {
        var companyId = "";

        var obj = {};
        var o = db.Object.extend(t_User);
        var c = db.Object.extend(t_Account);

        var queryUser = new db.Query(o);
        var queryAccount = new db.Query(c);

        queryUser.equalTo("objectId", userId);
        queryUser.select(["username", "nombre", "company"]);
        queryUser.include("company");
        
        queryAccount.select(["activation", "ends", "active", "tipo", "limit", "usuarios"]);
        
        return queryUser.first().then(res => {
            if (res) {
                var usuarioDto = new UsuarioDto();
                usuarioDto.cargarDatos(res);
                obj.usuario = usuarioDto;
                if (!usuarioDto.company) {
                    return obj;
                }
                queryAccount.equalTo("companyId", usuarioDto.companyId);
                return queryAccount.first().then(cuenta=>{
                    var cuentaDto = new CuentaDto();
                    cuentaDto.cargarDatos(cuenta);
                    obj.cuenta = cuentaDto;
                    return obj;
                });
            }
        });
    },

 

}


module.exports = Admin;

function loadCompany(companyId) {
    var obj = db.Object.extend(t_Company);
    var query = new db.Query(obj);
    query.equalTo("objectId", companyId);
    query.include("cuenta");
    return query.first();
}
function loadUsers(companyId) {
    var promise = loadCompany(companyId);
    return promise.then(result => {
        var obj = db.Object.extend(t_UserCompany);
        var query = new db.Query(obj);
        query.equalTo("company", result);
        query.include("user");
        query.include("company");
        return query.find();
    });
}

function loadSelectedUserCompanyById(objectId) {
    var obj = db.Object.extend(t_UserCompany);
    var query = new db.Query(obj);

    query.equalTo("objectId", objectId);
    query.include("company");
    query.include("user");
    return query.first();
}

function loadUserCompany(userId, companyId) {
    var promises = [];
    promises.push(queryFetchUser(userId));
    promises.push(queryFetchCompany(companyId));

    return db.Promise.when(promises).then(result => {
        var user = result[0];
        var company = result[1];

        var obj = db.Object.extend(t_UserCompany);
        var query = new db.Query(obj);

        query.equalTo("company", company);
        query.equalTo("user", user);
        query.include("company");
        return query.first();
    }).catch(err => {
        console.log("Error cargando el objeto user Company: " + err);
        return null;
    })
}

function cargarDatosUsuarioFirmaDto(usuarioFirma) {
    var finalUser = {};
    var user = usuarioFirma.get("user");
    var company = usuarioFirma.get("company");
    finalUser.id = usuarioFirma.id;
    finalUser.usuarioId = user.id;
    finalUser.firmaId = company.id;
    finalUser.correo = user.get("username");
    finalUser.nombre = user.get("nombre");
    finalUser.activo = usuarioFirma.get("activo");
    finalUser.permisos = usuarioFirma.get("permisos");
    finalUser.isOwner = usuarioFirma.get("isOwner");
    finalUser.agregado = usuarioFirma.get("createdAt");
    finalUser.estado = usuarioFirma.get("estado");
    return finalUser;
}


function queryFetchUser(userId) {
    var u = db.Object.extend(t_User);
    var query = new db.Query(u);
    query.equalTo("objectId", userId);
    return query.first();
}

function queryFetchCompany(companyId) {
    var u = db.Object.extend(t_Company);
    var query = new db.Query(u);
    query.equalTo("objectId", companyId);
    return query.first();
}


function queryRoleByName(){
    var u = db.Object.extend(t_Company);
    var query = new db.Query(u);
    query.equalTo("objectId", companyId);
    return query.first();
}