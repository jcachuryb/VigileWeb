const mailService = require('./mailservice');
const _ = require("lodash");

const env = process.env.NODE_ENV || 'development';

var config;
if (env === 'production') {
    config = require('../../../configprod')
} else {
    config = require('../../../config')
}


Notifier = {

    finSuscripcion: function (correo, datos) {
        datos.url = "http://" + config.pages.root + config.pages.tarifas;
        mailService.sendMailBCC('Lamentamos que no continúe con nosotros', correo, 'fin-suscripcion', datos, messageSent);
    },
};

module.exports = Notifier;


function messageSent(error, info) {
    if (error) {
        console.log(new Date(), ": Error sending the mail : ", error)
    }
    if (info) {
        console.log('Notif %s sent: %s', info.messageId, info.response);
    }
}