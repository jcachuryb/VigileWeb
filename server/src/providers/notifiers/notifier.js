const mailService = require('./mailservice');
const _ = require("lodash");

const env = process.env.NODE_ENV || 'development';

var config;
if (env === 'production') {
    config = require('../../../configprod')
} else {
    config = require('../../../config')
}


Notifier = {

    registroUsuario: function (nombre, correo, key) {
        var datos = {};
        datos.url = "http://" + config.pages.root + config.pages.confirmarCuenta + key;
        datos.nombre = nombre;
        mailService.sendMailBCC('Complete su registro', correo, 'registro', datos, messageSent);
    },

    notifNuevaActuación: function (inputObj, suscriptores) {
        var bcc = [];
        _.forEach(suscriptores, function (suscriptor) {
            if (suscriptor.email) {
                bcc = bcc.concat(suscriptor.email);
            }
        });
        mailService.sendNewStatusMail(bcc, inputObj, messageSent);

        //Enviar SMS 
    },
    notifNuevaCuenta: function (to, data) {
        mailService.sendNewAccountMail(to, data, messageSent);

        //Enviar SMS 
    },
    test: function (to, data) {
        mailService.sendMail(to, data, "<h1>TEST</h1>", messageSent);

        //Enviar SMS 
    },
    notificarAlerta: function (data, suscriptores) {
        var bcc = [];
        _.forEach(suscriptores, function (suscriptor) {
            if (suscriptor.email) {
                bcc = bcc.concat(suscriptor.email);
            }
        });
        mailService.sendWarningProcess(bcc, data, messageSent);

        //Enviar SMS 
    },

    notificarInvitacion: function (userCompany, company, to) {
        var datos = {};
        datos.nombreFirma = company.get("nombre");
        datos.permisos = userCompany.get("permisos");
        datos.urlAdminFirmas = config.pages.root + config.pages.adminFirmas;
        mailService.sendMailBCC('Invitación para ingresar a cuenta', to, 'invitar-cuenta', datos, messageSent)
    },

    solicitarRecuperarClave: function (userId, correo, codigo) {
        var datos = {};
        datos.url = config.pages.root + config.pages.cambiarClave + userId + "/" + codigo;
        datos.correo = correo;
        mailService.sendMailBCC('Recuperación de Clave', correo, 'recuperar-clave', datos, messageSent);
    },

    avisoCambioClave: function (correo) {
        var datos = {};
        datos.correo = correo;
        mailService.sendMailBCC('Cambio de clave exitoso', to, 'aviso-cambio-clave', datos, messageSent);
    },

    nuevaSuscripcion: function (correo, datos) {
        datos.url = "http://" + config.pages.root;
        mailService.sendMailBCC('¡Adquirió un plan en Vigile!', correo, 'nuevaSuscripcion', datos, messageSent);
    },
};

module.exports = Notifier;


function messageSent(error, info) {
    if (error) {
        console.log(new Date(), ": Error sending the mail : ", error)
    }
    if (info) {
        console.log('Notif %s sent: %s', info.messageId, info.response);
    }
}