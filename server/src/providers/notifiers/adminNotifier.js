const mailService = require('./mailservice');
const _ = require("lodash");
var momentjs = require('moment');

const env = process.env.NODE_ENV || 'development';

var config;
if (env === 'production') {
    config = require('../../../configprod')
} else {
    config = require('../../../config')
}

Notifier = {
    feedbackRecibido: function (feedback) {
        var bcc = config.emailAdmins;
        var datos = {};
        
        datos.email = feedback.get("email");
        datos.mensaje = feedback.get("mensaje");
        datos.url = feedback.get("url");
        datos.tipo = feedback.get("tipo");
        mailService.sendMailBCC('Nuevo Comentario de los usuarios', bcc, 'nuevo-feedback', datos, messageSent)
    },

    contactoRecibido: function (contacto) {
        var bcc = config.emailAdmins;
        var datos = {};
        
        datos.nombres = contacto.nombres;
        datos.correo = contacto.correo;
        datos.telefono = contacto.telefono;
        datos.pregunta = contacto.pregunta;

        mailService.sendMailBCC('Nuevo Contacto recibido', bcc, 'nuevo-contacto', datos, messageSent)
    },

};

module.exports = Notifier;


function messageSent(error, info) {
    if (error) {
        console.log(new Date(), ": Error sending the mail : ", error)
    }
    if (info) {
        console.log('Notif %s sent: %s', info.messageId, info.response);
    }
}