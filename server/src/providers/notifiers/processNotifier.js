const mailService = require('./mailservice');
const _ = require("lodash");
var momentjs = require('moment');

Notifier = {
    procesoEliminado: function (proceso, emailUsuario) {
        var suscriptores = proceso.get('suscriptores');
        var bcc = [];
        _.forEach(suscriptores, function (suscriptor) {
            if (suscriptor.email) {
                bcc = bcc.concat(suscriptor.email);
            }
        });
        if (bcc.length) {
            var datos = {};
            datos.codProceso = proceso.get('codProceso');
            datos.ciudad = proceso.get('ciudad');
            datos.entidad = proceso.get('entidad');
            datos.creado = proceso.get('createdAt');
            datos.info = proceso.get("info");
            datos.estadoActual = proceso.get("estadoActual");
            datos.emailUsuario = emailUsuario;
            datos.ultimaActuacion = proceso.get("ultimaActuacion");

            mailService.sendMailBCC('Proceso eliminado', bcc, 'proceso-eliminado', datos, messageSent)
        }

        //Enviar SMS 
    },
    test: function (to, data) {
        mailService.sendMail(to, data,"<h1>TEST</h1>",  messageSent);

        //Enviar SMS 
    },
};

module.exports = Notifier;


function messageSent(error, info) {
    if (error) {
        console.log(new Date(), ": Error sending the mail : ", error)
    }
    if(info){
        console.log('Notif %s sent: %s', info.messageId, info.response);
    }
}