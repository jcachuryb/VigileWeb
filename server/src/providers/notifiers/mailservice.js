const nodemailer = require('nodemailer');
const hbs = require('nodemailer-express-handlebars');
var path = require('path');
const env = process.env.NODE_ENV || 'development';
let transporter = null;
let options = null;

var config;
if (env === 'production') {
    config = require('../../../configprod')
} else {
    config = require('../../../config')
}

MailService = {
    initialize: function (serviceOptions) {
        var hbsOptions = {
            viewEngine: {
                extname: '.hbs',
                layoutsDir: 'src/email-templates/layouts/',
                defaultLayout: 'template',
                partialsDir: 'src/email-templates/partials/'
            },
            viewPath: 'src/email-templates/templates/',
            extName: '.hbs'
        };
        options = serviceOptions;

        transporter = nodemailer.createTransport({
            host: 'in-v3.mailjet.com',
            port: 465,
            secure: true, // secure:true for port 465, secure:false for port 587
            auth: {
                user: 'dcd3d81a86220bcc007626df7bf3ffa7',
                pass: '438f8dedee70a0738d36f56783993524'
            },
            tls: {
                rejectUnauthorized: false
            }
        });
        transporter.use('compile', hbs(hbsOptions));
    },

    sendMail: function (to, subject, html, callback) {
        try {
            mailOptions = {
                to: to.toString(), // list of receivers
                subject: subject, // Subject line
                html: html // html body
            };
            mailOptions.from = options.from;
            return transporter.sendMail(mailOptions, callback);
        } catch (error) {
            return false;
        }
    },
    sendRegMail: function (to, data, callback) {
        try {
            var url =  config.pages.root + config.pages.confirmarCuenta + data.key;
            data.key = url;
            return transporter.sendMail({
                from: options.from,
                to: to,
                subject: 'Complete su registro',
                template: 'registro',
                context: data
            }, callback);
        } catch (error) {
            return false
        }
    },
    sendNewStatusMail: function (to, datosProceso, callback) {
        try {
            datosProceso.url = options.processDetailPage + datosProceso.objectId
            return transporter.sendMail({
                from: options.from,
                bcc: to,
                subject: 'Nueva actuación registrada',
                template: 'nuevaActuacion',
                context: datosProceso
            }, callback);
        } catch (error) {
            console.log(error);
            return false;
        }
    },
    sendNewAccountMail: function (to, data, callback) {
        try {
            data.address = options.server;
            return transporter.sendMail({
                from: options.from,
                to: to,
                subject: 'Bienvenido a Vigile',
                template: 'bienvenida',
                context: data
            }, callback);
        } catch (error) {
            console.log(error);
            return false;
        }
    },
    sendWarningProcess: function (to, datosProceso, callback) {
        try {
            datosProceso.url = options.processDetailPage + datosProceso.objectId;
            return transporter.sendMail({
                from: options.from,
                bcc: to,
                subject: 'Alerta registrada',
                template: 'alerta',
                context: datosProceso
            }, callback);
        } catch (error) {
            console.log(error);
            return false;
        }
    },

    sendMailBCC: function (subject, to, template, datos, callback) {
        try {
            return transporter.sendMail({
                from: options.from,
                bcc: to,
                subject: subject,
                template: template,
                context: datos
            }, callback);
        } catch (error) {
            return false;
        }
    }

};

module.exports = MailService;