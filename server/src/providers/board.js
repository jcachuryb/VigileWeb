const db = require("parse/node");
const _ = require("lodash");

const t_Process = "Process";
const TABLE_BOARD = "Board";
const TABLE_LIST = "List";
const TABLE_PROCESS = "Process";

const notifier = require('./notifiers/notifier');



Board = {
    addBoard: function (nombre, company) {

        // Check account permissions and limits
        var objClass = db.Object.extend(TABLE_BOARD);
        var p = new objClass();
        p.set("nombre", nombre);
        p.set("company", company);
        p.set("activo", true);
        return p.save();
    },
    editBoard: function (boardId, nombre, activo) {
        var objClass = db.Object.extend(TABLE_BOARD);
        var query = new db.Query(objClass);
        query.equalTo("objectId", boardId);
        return query.first().then(obj => {
            obj.set("nombre", nombre);
            p.set("activo", activo);
            return obj.save();
        });
    },
    fetchBoards: function (company) {
        var objClass = db.Object.extend(TABLE_BOARD);
        var query = new db.Query(objClass);
        query.equalTo("company", company);
        return query.find();
    },

    // ----------------------------------------------------------------------------------

    addList: function (boardId, nombre, orden) {

        // Check account permissions and limits
        var objClass = db.Object.extend(TABLE_BOARD);
        var query = new db.Query(objClass);
        query.equalTo("objectId", boardId);
        return query.first().then(board => {
            var objClass = db.Object.extend(TABLE_LIST);
            var p = new objClass();
            p.set("nombre", nombre);
            p.set("tablero", board);
            p.set("orden", orden);
            p.set("activa", true);
            return p.save();
        });

    },
    editList: function (listId, nombre, activa, orden) {
        var objClass = db.Object.extend(TABLE_LIST);
        var query = new db.Query(objClass);
        query.equalTo("objectId", listId);
        return query.first().then(obj => {
            obj.set("nombre", nombre);
            p.set("orden", orden);
            obj.set("activa", activa);
            return obj.save();
        });
    },

    saveListProcesses: function (listId, procesos) {
        var objClass = db.Object.extend(TABLE_LIST);
        var query = new db.Query(objClass);
        query.equalTo("objectId", listId);
        return query.first().then(obj => {
            obj.set("procesos", procesos);
            return obj.save();
        });
    },

    fetchLists: function (boardId) {
        var objClass = db.Object.extend(TABLE_BOARD);
        var query = new db.Query(objClass);
        query.equalTo("objectId", boardId);
        return query.first().then(board => {
            var query = new db.Query(db.Object.extend(TABLE_LIST));
            query.equalTo("tablero", board);
            query.equalTo("activa", true);
            query.orde
            return query.find();
        });
    },

    fetchListProcesses: function (company, boardId, listId) {
        var objClass = db.Object.extend(TABLE_BOARD);
        var query = new db.Query(objClass);
        query.equalTo("objectId", boardId);
        query.equalTo("company", company);
        return query.first().then(board => {
            var query = new db.Query(db.Object.extend(TABLE_LIST));
            query.equalTo("tablero", board);
            query.equalTo("objectId", listId);
            query.select(["procesos"]);
            return query.find().then(lista => {
                var array = lista.get("procesos");

                if (array.length > 0) {
                    var queryProcesses = new db.Query(db.Object.extend(TABLE_PROCESS));
                    queryProcesses.containedIn("objectId", array);
                    return queryProcesses.find();
                }
            });
        });
    },

    // ----------------------------------------------------------------------------------

    addProcessToList: function (correo, mensaje, tipo, data) {
        return null;
    },
    removeProcessFromList: function (correo, mensaje, tipo, data) {
        return null;

    },
    moveProcess: function (correo, mensaje, tipo, data) {

        return null;
    },




}


module.exports = Board;