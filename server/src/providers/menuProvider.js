const _ = require("lodash");
const PAGES_MENU = require("./menu");


function Menu(userId, roles, permisos) {
    this.userId = userId;
    this.roles = roles;
    this.permisos = permisos;
}

Menu.prototype.obtenerMenu = function () {
    var routes = _.cloneDeep(PAGES_MENU);
    var items = this._convertArrayToItems(routes);
    return this._skipEmpty(items);
}

module.exports = Menu;

Menu.prototype._skipEmpty = function (items) {
    var menu = [];
    items.forEach((item) => {
        var menuItem;
        if (item.skip) {
            if (item.children && item.children.length > 0) {
                menuItem = item.children;
            }
        } else {
            menuItem = item;
        }

        if (menuItem) {
            menu.push(menuItem);
        }
    });

    return [].concat.apply([], menu);
}

Menu.prototype._convertArrayToItems = function (routes, parent) {
    var items = [];
    routes.forEach((route) => {
        var item = this._convertObjectToItem(route, parent);
        if (item && !item.hidden) {
            items.push(item);
        }

    });
    return items;
}

Menu.prototype._convertObjectToItem = function (object, parent) {
    var item = {};
    var diferencias = [];
    if (object.data && object.data.menu) {
        // this is a menu object
        item = object.data.menu;
        item.route = object;
        if (item.roles) {
            diferencias = _.difference(this.roles, item.roles);
            item.hidden = diferencias.length >= this.roles.length;
        }

        if (item.permisos) {
            diferencias = _.difference(this.permisos, item.permisos);
            item.hidden = diferencias.length >= this.permisos.length;
        }
        delete item.route.data.menu;
    } else {
        item.route = object;
        item.skip = true;
    }

    // we have to collect all paths to correctly build the url then
    if (Array.isArray(item.route.path)) {
        item.route.paths = item.route.path;
    } else {
        item.route.paths = parent && parent.route && parent.route.paths ? parent.route.paths.slice(0) : ['/'];
        if (!!item.route.path) item.route.paths.push(item.route.path);
    }

    if (object.children && object.children.length > 0) {
        item.children = this._convertArrayToItems(object.children, item);
    }

    let prepared = this._prepareItem(item);

    // if current item is selected or expanded - then parent is expanded too
    if ((prepared.selected || prepared.expanded) && parent) {
        parent.expanded = true;
    }

    return prepared;
}

Menu.prototype._prepareItem = function (object) {
    if (!object.skip) {
        object.target = object.target || '';
        object.pathMatch = object.pathMatch || 'full';
        return object;
    }

    return object;
}

Menu.prototype._selectItem = function (object) {
    object.selected = _router.isActive(_router.createUrlTree(object.route.paths), object.pathMatch === 'full');
    return object;
}