const db = require("parse/node");
const _ = require("lodash");
const momentjs = require('moment');
const t_Company = "Company";
const t_Account = "Account";
const t_Category = "Category";
const t_User = "Usuario";
const t_Process = "Process";
const t_ProcessStates = "ProcessStates";
const notifier = require('./notifiers/notifier');
const userProvider = require('./user');



Process = {
    addProcess: function (process) {
        var processClass = db.Object.extend(t_Process);
        var p = new processClass();
        p.set("activo", true);
        p.set("estado", "inicial");
        p.set("tipo", process.tipo);
        p.set("info", process.data);
        p.set("categorias", []);
        p.set("problem", null);
        p.set("filtros", process.filtros);
        return p.save().then(res => {
            console.log(res);
            return true;
        }).catch(res => {
            console.log(res);
            return false;
        });
    },

    checkExistences: function (company, procesos, tipo) {
        try {
            var codProcesos = [];
            var o = db.Object.extend(t_Process);
            var query = new db.Query(o);
            query.equalTo("company", company);
            _.forEach(procesos, function (val) {
                codProcesos.push(val.data.codProceso);
            });

            query.containedIn("codProceso", codProcesos);
            query.select(["codProceso", "data"]);
            return query.find().then(res => {
                var repetidos = [];
                _.each(res, function (val) {
                    if (val.attributes.data) {
                        _.each(procesos, function (input) {
                            var igualCiudad = val.attributes.data.ciudad === input.values.ciudad;
                            var igualEntidad = val.attributes.data.entidad === input.values.entidad;
                            if (igualCiudad && igualEntidad) {
                                repetidos.push(val.attributes.data.codProceso);
                                return false;
                            }
                        });
                    }
                })
                return repetidos;
            }).catch(res => {
                console.log(res);
                return false;
            });
        } catch (error) {
            console.log(error)
        }
    },

    addMultipleProcesses: function (company, processes, suscriptor) {
        var processClass = db.Object.extend(t_Process);
        var objProces = [];
        _.forEach(processes, function (val) {
            var p = new processClass();
            p.set("codProceso", val.data.codProceso);
            p.set("ciudad", val.data.ciudad);
            p.set("entidad", val.data.entidad);
            p.set("data", val.values);
            p.set("company", company);
            p.set("activo", true);
            p.set("estadoActual", {});
            p.set("info", {});
            p.set("tags", []);
            p.set("problem", {});
            p.set("ultimaAct", null);
            p.set("indexActuacion", 0);
            p.set("suscriptores", [suscriptor]);
            objProces.push(p);
        });
        return db.Object.saveAll(objProces, {
            success: function (list) {
                return true;
            },
            error: function (error) {
                console.log(error);
                return error;
            },
        });

    },

    saveProcessStates: function (data) {
        var statesClass = db.Object.extend(t_ProcessStates);
        var estados = new statesClass();
        if (data.id) {
            var query = new Parse.Query(statesClass);
            return query.get(data.id).then(res => {
                res.set("html", data.html);
                res.set("total", data.total);
                return res.save().then(obj => {
                    return obj.id;
                }).catch(err => {
                    console.log(err);
                    return "";
                });

            }).catch(err => {
                console.log(err);
                return "";
            });
        }

        estados.set("html", data.html);
        estados.set("total", data.total);
        return estados.save().then(res => {
            return obj.id;
        }).catch(err => {
            console.log(err);
            return "";
        });


    },

    reactivateProcess: function (company, objectId) {
        var objClass = db.Object.extend(t_Process);
        var query = new db.Query(objClass);
        query.equalTo("objectId", objectId);
        query.equalTo("company", company);
        return query.first().then(obj => {
            obj.set("activo", true);
            obj.set("problem", {});
            // obj.set("ultimaAct", new Date());
            return obj.save();
        });

    },

    reProcess: function (companyId, objectId, reactivate) {
        console.log(companyId, objectId, reactivate);

        var objClass = db.Object.extend(t_Process);
        var query = new db.Query(objClass);
        query.equalTo("objectId", objectId);
        query.equalTo("company", companyId);
        return query.first().then(obj => {
            obj.set("reconsultar", reactivate);
            return obj.save();
        }).catch(err => {
            console.log(err);
            return null;
        });

    },

    botToreProcess: function (objectId, codProceso) {
        console.log("Resetear proceso ", objectId, codProceso);
        var Process = db.Object.extend(t_Process);
        var ProcessStates = db.Object.extend(t_ProcessStates);
        var query = new db.Query(Process);
        query.equalTo("objectId", objectId);

        var query2 = new db.Query(ProcessStates);
        query2.equalTo("codProceso", codProceso);
        query2.limit(1000);

        var promises = [];
        promises.push(query2.find());
        promises.push(query.first());

        return db.Promise.when(promises).then(result => {
            var actuaciones = result[0];
            var proceso = result[1];
            console.log("# Actuaciones del proceso:" + actuaciones.length, proceso);
            proceso.set("ultimaActuacion", "La concha");
            proceso.set("problem", {});
            proceso.set("estadoActual", {});
            proceso.set("info", {});
            proceso.set("activo", true);
            proceso.set("reconsultar", null);

            promises = [];
            promises.push(db.Object.destroyAll(actuaciones));
            promises.push(proceso.save());
            return db.Promise.when(promises).then(result => {
                console.log("Listo, proceso restaurado")
                return result[1];
            }).catch(err => {
                console.log(err);
                return null;
            });
        }).catch(err => {
            console.log(err);
            return null;
        });
    },

    deleteProcess: function (company, objectId) {
        var objClass = db.Object.extend(t_Process);
        var query = new db.Query(objClass);
        query.equalTo("objectId", objectId);
        query.equalTo("company", company);
        return query.first().then(obj => {
            if (obj) {
                return obj.destroy();
            } else {
                return false;
            }
        });
    },

    updateBadProcess: function (company, objectId, process) {
        var objClass = db.Object.extend(t_Process);
        var query = new db.Query(objClass);
        query.equalTo("objectId", objectId);
        query.equalTo("company", company);
        return query.first().then(obj => {
            obj.set("codProceso", process.data.codProceso);
            obj.set("ciudad", process.data.ciudad);
            obj.set("entidad", process.data.entidad);
            obj.set("data", process.values);
            obj.set("activo", true);
            obj.set("problem", {});
            // obj.set("ultimaAct", new Date());
            return obj.save();
        });

    },

    saveProcesses: function (data) {
        var objClass = db.Object.extend(t_Process);
        var ids = [];
        var processes = [];
        _.forEach(data, function (p) {
            ids.push(p.objectId);
        });
        var query = new db.Query(objClass);
        query.containedIn("objectId", ids);
        return query.find().then(res => {
            _.forEach(res, function (p) {
                var currentID = p.id;
                var obj = _.find(data, { 'objectId': currentID });
                if (obj) {
                    actualizarObj(p, obj);
                    processes.push(p);
                }
            });
            return db.Object.saveAll(processes);
        });
    },

    getProcessStates: function (processid, full) {
        var statesClass = db.Object.extend(t_ProcessStates);

        var o = db.Object.extend(t_ProcessStates);
        var query = new db.Query(o);
        query.equalTo("process", processid);
        params = ["total"];
        if (full) params.push("html");
        query.select(params);
        return query.find().then(res => {
            return res;
        }).catch(res => {
            console.log(res);
            return false;
        });
    },

    getInfoProcessStatus: function (codProceso, full) {
        var statesClass = db.Object.extend(t_ProcessStates);

        var o = db.Object.extend(t_ProcessStates);
        var query = new db.Query(o);
        query.equalTo("codProceso", codProceso);
        return query.count();
    },

    saveMultipleStatus: function (estados) {
        var statesClass = db.Object.extend(t_ProcessStates);
        var arrayObjects = [];
        _.forEach(estados, function (obj) {
            var p = new statesClass();
            p.set("actuacion", obj.actuacion);
            p.set("fechaActuacion", obj.fechaActuacion);
            p.set("anotacion", obj.anotacion);
            p.set("fechaInicioTermino", obj.fechaInicioTermino);
            p.set("fechaFinTermino", obj.fechaFinTermino);
            p.set("fechaRegistro", obj.fechaRegistro);
            p.set("num", obj.num);
            p.set("codProceso", obj.codProceso);
            arrayObjects.push(p);
        });
        return db.Object.saveAll(arrayObjects, {
            success: function (list) {
                return true;
            },
            error: function (error) {
                console.log(error);
                return error;
            },
        });
    },

    saveSingleState: function (obj) {

        try {
            var statesClass = db.Object.extend(t_ProcessStates);
            var p = new statesClass();
            p.set("actuacion", obj.actuacion);
            p.set("fechaActuacion", obj.fechaActuacion);
            p.set("anotacion", obj.anotacion);
            p.set("fechaInicioTermino", obj.fechaInicioTermino);
            p.set("fechaFinTermino", obj.fechaFinTermino);
            p.set("fechaRegistro", obj.fechaRegistro);
            p.set("num", obj.num);
            p.set("codProceso", obj.codProceso);
            return p.save();
        } catch (error) {
            return error;
        }
    },


    loadToApp: function (companyid, select) {
        var o = db.Object.extend(t_Process);
        var query = new db.Query(o);
        query.equalTo("company", companyid);
        query.descending("ultimaAct");
        query.select(["codProceso", "ciudad", "entidad", "estadoActual", "tags", "problem", "reconsultar"]);
        return query.find().then(res => {
            return res;
        }).catch(res => {
            console.log(res);
            return false;
        });
    },


    listProcessesToApp: function (companyid, num, page, filtros) {
        var o = db.Object.extend(t_Process);
        var query = new db.Query(o);
        query.equalTo("company", companyid);
        query.descending("ultimaAct");
        query.addDescending("createdAt");
        query.limit(num);
        query.skip(page * num);
        query.select(["codProceso", "ciudad", "entidad",
            "estadoActual", "suscriptores", "problem", "ultimaActuacion", "info", "reconsultar", "ultimaAct"]);
        addFiltros(query, filtros);

        return query.find().then(res => {
            var listProcesos = [];
            if (res) {
                _.each(res, p => {
                    var itemProceso = {};
                    itemProceso.id = p.id;
                    itemProceso.codProceso = p.get("codProceso");
                    itemProceso.ciudad = p.get("ciudad");
                    itemProceso.entidad = p.get("entidad");
                    itemProceso.estadoActual = p.get("estadoActual");
                    itemProceso.suscriptores = p.get("suscriptores");
                    itemProceso.problem = p.get("problem");
                    itemProceso.ultimaActuacion = p.get("ultimaActuacion");
                    itemProceso.info = p.get("info");
                    itemProceso.reconsultar = p.get("reconsultar");
                    itemProceso.ultimaAct = p.get("ultimaAct");
                    listProcesos.push(itemProceso);
                })
            }
            return listProcesos;
        }).catch(res => {
            console.log(res);
            return [];
        });
    },

    loadProcessDetail: function (companyid, objectId) {
        var o = db.Object.extend(t_Process);
        var query = new db.Query(o);
        query.equalTo("company", companyid);
        query.equalTo("objectId", objectId);
        //query.select(["codProceso", "ciudad", "entidad", "entidad", "estadoActual", "tags", "problem"]);
        //query.select(select);
        return query.first().then(res => {
            return res;
        }).catch(res => {
            console.log(res);
            return false;
        });
    },

    loadActuacionesProceso: function (codProceso, index, page, num) {
        var o = db.Object.extend(t_ProcessStates);
        var query = new db.Query(o);
        query.equalTo("codProceso", codProceso);
        query.lessThan("num", index + 1);
        query.limit(num);
        query.skip(page * num);
        query.descending("num");
        return query.find();
    },

    loadToBot: function (companyid, page, num) {

        var selectFields = ["codProceso", "data", "estadoActual", "estados", "info", "reconsultar"];
        var o = db.Object.extend(t_Process);
        var query = new db.Query(o);
        query.equalTo("company", companyid);
        query.limit(num);
        query.skip(page * num);
        // query.equalTo("activo", true);
        query.ascending("createdAt");
        query.select(selectFields);
        var query2 = new db.Query(o);
        query2.equalTo("company", companyid);
        query2.limit(num);
        query2.skip(page * num);
        query2.equalTo("reconsultar", true);
        query2.select(selectFields);
        var mainQuery = db.Query.or(query, query);
        mainQuery.limit(num);
        mainQuery.skip(page * num);

        mainQuery.select(selectFields);


        return mainQuery.find().then(res => {
            return res;
        }).catch(res => {
            console.log(res);
            return false;
        });
    },

    filterTest: function (companyid, num, page, filtros) {
        var o = db.Object.extend(t_Process);
        var query = new db.Query(o);
        query.equalTo("company", companyid);
        query.descending("updatedAt");
        query.limit(num);
        query.skip(page * num);
        query.select(["codProceso", "ciudad", "entidad",
            "estadoActual", "tags", "problem", "ultimaActuacion", "info"]);
        addFiltros(query, filtros);

        return query.find();
    },

    getProcesosByUser: function (userId) {
        var o = db.Object.extend(t_Process);
        var query = new db.Query(o);
        query.equalTo("suscriptores.userId", userId);
        return query.find();
    },

    subscribeToProcess(processId, userId, channels) {
        var process;

        var o = db.Object.extend(t_Process);
        var query = new db.Query(o);
        query.equalTo("objectId", processId);
        query.select(["suscriptores"]);
        return query.first().then(p => {
            process = p;
            if (process) {
                var suscriptores = process.get("suscriptores");
                var existe = false;
                _.forEach(suscriptores, function (sus) {
                    if (sus.userId === userId) {
                        existe = true;
                        return;
                    }
                });
                if (existe) {
                    return true;
                }
                return userProvider.getUserNotificationChannels(userId).then(userChannels => {
                    var nuevoSuscriptor = {};
                    nuevoSuscriptor["userId"] = userId;
                    _.forEach(channels, function (name) {
                        nuevoSuscriptor[name] = userChannels[name];
                    });
                    var suscriptores = process.get("suscriptores");
                    if (!suscriptores) {
                        suscriptores = [];
                    }
                    suscriptores.push(nuevoSuscriptor);
                    process.set("suscriptores", suscriptores);
                    return process.save();
                })
            } else {
                return false;
            }
        })
    },

    unSubscribeFromProcess(processId, userId) {
        var process;

        var o = db.Object.extend(t_Process);
        var query = new db.Query(o);
        query.equalTo("objectId", processId);
        query.select(["suscriptores"]);
        return query.first().then(p => {
            process = p;
            if (process) {
                var suscriptores = process.get("suscriptores");
                _.remove(suscriptores, sus => {
                    return sus.userId === userId;
                });
                process.set("suscriptores", suscriptores);
                return process.save();
            } else {
                return false;
            }
        })
    }

}


module.exports = Process;


function actualizarObj(parseObj, inputObj) {
    try {

        var procesoultimaAct = null;
        try {
            procesoultimaAct = new Date(inputObj.ultimaAct);
        } catch (error) {

        }
        parseObj.set("ultimoEscan", new Date().toISOString());
        parseObj.set("indexActuacion", inputObj.indexActuacion);

        if (inputObj.infoActualizada) {
            parseObj.set("info", inputObj.info);
        }
        if (inputObj.estadoActualizado) {
            var notificarEstado = !parseObj.get("estadoActual");
            parseObj.set("estadoActual", inputObj.estadoActual);
            parseObj.set("ultimaActuacion", inputObj.estadoActual.fechaActuacion);
            parseObj.set("ultimaAct", procesoultimaAct);
            if (parseObj.attributes.estadoActual.actuacion) {
                inputObj.ciudad = parseObj.attributes.ciudad;
                inputObj.entidad = parseObj.attributes.entidad;
                if (notificarEstado) {
                    notifier.notifNuevaActuación(inputObj, parseObj.attributes.suscriptores);
                }
            }
        }
        if (inputObj.alerta) {
            try {
                notifier.notificarAlerta(inputObj, parseObj.attributes.suscriptores);
            } catch (error) {
                console.log("Error notificando alerta");
                console.log(error);
            }
            parseObj.set("alerta", inputObj.alerta);
        } else {
            parseObj.set("alerta", null);
        }
        if (inputObj.estadosActualizados) {
        }
        // if (inputObj.estadoActualizado
        //     || inputObj.problem) {
        //     parseObj.set("ultimaAct", new Date());
        // }


        if (inputObj.problem) {
            parseObj.set("problem", inputObj.problem);
            // parseObj.set("activo", false);
        }else{
            parseObj.set("problem", {});
            parseObj.set("activo", true);
        }
    } catch (error) {
        console.log(error)
    }

}

function addFiltros(query, filtros) {
    _.forEach(filtros, function (f) {
        // value, query, key
        if (f.query === "contains") {
            query.contains(f.key, f.value.toUpperCase());
        }
        if (f.query === "equalTo") {
            query.equalTo(f.key, f.value);
        }
        if (f.query === "exists") {
            query.exists(f.key);
        }
    });
}

function queryFetchCompany(companyId) {
    var u = db.Object.extend(t_Company);
    var query = new db.Query(u);
    query.equalTo("objectId", companyId);
    return query.first();
}

function dd(variable, varName) {
    var varNameOutput;

    varName = varName || '';
    varNameOutput = varName ? varName + ':' : '';

    console.warn(varNameOutput, variable, ' (' + (typeof variable) + ')');
}