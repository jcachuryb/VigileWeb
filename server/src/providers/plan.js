const db = require("parse/node");
const _ = require("lodash");
const notifier = require("./notifiers/notifier");
const t_Company = "Company";
const t_Account = "Account";
const t_Category = "Category";
const t_User = "User";
const t_Roles = "Role";
const t_UserCompany = "UserCompany";
const t_Plan = "Plan";

const UsuarioDto = require('../models/usuario');
const planDto = require('../models/plan');


Plan = {

    fetchPlans: function () {
        var planes = [];
        return fetchPlans().then(list => {
            if (list) {
                _.forEach(list, pl => {
                    var plan = new planDto();
                    plan.cargarDatos(pl);
                    planes.push(plan);
                })
            }
            return planes;
        });
    },

    fetchPlanById: function (id) {
        return loadPlan(id).then(pl => {
            var plan = new planDto();
            if (pl) {
                plan.cargarDatos(pl);
            }
            return plan;
        });
    },

    createPlan: function (data) {
        var planClass = db.Object.extend(t_Plan);
        var plan = new planClass();
        plan.set("nombre", data.nombre);
        plan.set("tipo", data.tipo);
        plan.set("procesos", data.procesos);
        plan.set("usuarios", data.usuarios);
        plan.set("duracion", data.duracion);
        plan.set("valorProceso", data.valorProceso);
        plan.set("valorUsuario", data.valorUsuario);
        return plan.save();
    },

    editPlan: function (data) {
        return loadPlan(data.id).then(plan => {
            plan.set("nombre", data.nombre);
            plan.set("tipo", data.tipo);
            plan.set("procesos", data.procesos);
            plan.set("usuarios", data.usuarios);
            plan.set("duracion", data.duracion);
            plan.set("valorProceso", data.valorProceso);
            plan.set("valorUsuario", data.valorUsuario);
            return plan.save();
        })
    },

    removePlan: function (id) {
        return loadPlan(id).then(plan => {
            return plan.destroy();
        })
    },

}


module.exports = Plan;

function loadPlan(planId) {
    var obj = db.Object.extend(t_Plan);
    var query = new db.Query(obj);
    query.equalTo("objectId", planId);
    return query.first();
}

function fetchPlans() {
    var obj = db.Object.extend(t_Plan);
    var query = new db.Query(obj);
    return query.find();
}