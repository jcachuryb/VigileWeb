const fs = require('fs');
const join = require('path').join;
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors = require('cors');
var mongoose = require('mongoose');
var passport = require('passport');
var MailService = require('./src/providers/notifiers/mailservice');

var Parse = require('parse/node');

const models = join(__dirname, 'src/models');

const env = process.env.NODE_ENV || 'development';

var config;
if (env === 'production') {
  config = require('./configprod.js')
} else {
  config = require('./config.js')
}

// DB RELATED
console.log("Initializing parse...");
Parse.initialize(config.parse.appID, config.parse.javascriptKey, config.parse.masterKey);
Parse.serverURL = config.parse.serverURL;
// END  DB RELATED
console.log("Initializing mailservice...");
MailService.initialize(config.mailservice);


//  - - - - - - - - - - ROUTES
var index = require('./routes/index');
var users = require('./routes/users');
var admin = require('./routes/admin');
var plan = require('./routes/admin/plan');
var task = require('./routes/tasks');
var suscripcion = require('./routes/admin/suscripcion');
var clientR = require('./routes/client');
var company = require('./routes/company');
var processes = require('./routes/processes');
var auth = require('./routes/auth');
var account = require('./routes/account');
var bot = require('./routes/bot');
var board = require('./routes/board');
//  - - - - - - - - END ROUTES


var app = express();

// Bootstrap models
fs.readdirSync(models)
  .filter(file => ~file.search(/^[^\.].*\.js$/))
  .forEach(file => require(join(models, file)));


// Bootstrap routes
require('./config/passport')(passport);

app.use(cors()) // <--- CORS  

// view engine setup

if (env === 'production') {
  app.use(express.static(path.join(__dirname, '/dist')));
} else {
  app.set('views', path.join(__dirname, 'views'));
}
app.set('view engine', 'jade');


function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) { return next(null); }
  res.redirect('/error')
}


// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(require('express-session')({
  secret: 'keyboard cat',
  resave: true,
  saveUninitialized: true
}));



app.use(passport.initialize());
app.use(passport.session());

app.use('/api/index', index);
app.use('/api/users', users);
app.use('/api/processes', processes);
app.use('/api/admin', admin);
app.use('/api/plan', plan);
app.use('/api/task', task);
app.use('/api/suscripcion', suscripcion);
app.use('/api/auth', auth);
app.use('/api/account', account);
app.use('/api/bot', bot);
app.use('/api/client', clientR);
app.use('/api/company', company);
app.use('/api/board', board);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

if (env === 'production') {

  // production error handler
  // no stacktraces leaked to user
  app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: {}
    });
  });
}

app.use(errorHandler);
function errorHandler(err, req, res, next) {
  console.log(err.stack);
  res.status(401);
  res.json(false);
}

module.exports = app;
function objTest(variable, varName) {
  var varNameOutput;

  varName = varName || '';
  varNameOutput = varName ? varName + ':' : '';

  console.warn(varNameOutput, variable, ' (' + (typeof variable) + ')');
}