module.exports = {
    getConfigOptions: function () {
        const env = process.env.NODE_ENV || 'development';

        var config;
        if (env === 'production') {
            config = require('./configprod.js')
        } else {
            config = require('./config.js')
        }
        return config;
    }
};