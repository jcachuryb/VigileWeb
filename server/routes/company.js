var express = require('express');
const _ = require("lodash");
var router = express.Router();
const clientProvider = require('../src/providers/client');
const userProvider = require('../src/providers/user');
const companyProvider = require('../src/providers/company');


var CompanyUserDto = require('../src/models/companyUser');

const ACCION_EDITAR = "editar";
const ACCION_ASIGNAR = "asignar";

router.post('/**', function (req, res, next) {
    /*
        req.body = {
            email: "jcachuryb@outlook.com",
            permisos: ['leer'], 
            userCompany: "3XNIdrXfMD",
            response: false
        };
        req.user = {
            company: "aDR20urd0q",
            objectId: "mxyH32PFqK"
        }
        //dd(req.user);
    */
    if (!req.user) {
        res.json({ status: "fail", success: false });
    } else {
        next();
    }
});

router.post('/selectCompany', function (req, res, next) {
    var companyId = req.body.company;
    var userId = req.user.usuario.id;
    companyProvider.loadSelectedUserCompany(userId, companyId)
        .then(value => {
            req.user.selectedCompanyId = value.get("company").id;
            req.user.usuario.selectedCompany = new CompanyUserDto();
            req.user.usuario.selectedCompany.cargarDatos(value);
            res.json({ status: "success", data: value.attributes });
        }).catch(error => {
            console.log(error);
            res.json({ status: "fail", msg: "Usted no tiene permisos para hacer eso", reload: true })
        });
});

router.post('/getCompanyUsers', function (req, res, next) {
    var companyId = req.user.selectedCompanyId;
    companyProvider.loadCompanyUsers(companyId).then(data => {
        res.json(data);
    })
});

router.post('/respondCompanyInvitation', function (req, res, next) {
    var userId = req.user.usuario.id;
    var userCompanyId = req.body.id;
    var response = req.body.response;
    companyProvider.respondCompanyInvitation(userId, userCompanyId, response).then(data => {
        if (!data) {
            res.json({ status: "fail", msg: "La operación no se encuentra disponible en estos momentos.", reload: true });
        }
        res.json({ status: "success", msg: "Los datos fueron guardados con éxito." });
    })
});

router.post('/loadCompanyInvitations', function (req, res, next) {
    var userId = req.user.usuario.id;
    companyProvider.loadCompanyInvitations(userId).then(data => {
        res.json({ status: "success", data: data });
    })
});

router.post('/getUserCompanies', function (req, res, next) {
    var userId = req.user.usuario.id;
    companyProvider.loadUserCompanies(userId).then(data => {
        res.json({ status: "success", data: data });
    }).catch(err => {
        console.log(err);
        res.json({ status: "fail", msg: "Ocurrió un error en la consulta" });
    })
});

router.post('/addUserToCompany', userExists, canAdduserToCompany, validarPermisosGestionarUsuarios, function (req, res, next) {
    var companyId = req.user.selectedCompanyId;
    var userEmail = req.body.email;
    var data = req.body.data;
    var isOwner = false;

    companyProvider.addUserToCompany(companyId, userEmail, data, isOwner).then(data => {
        if (data) {
            res.json({ status: "success", data: data });
        } else {
            res.json({ status: "fail", msg: "Ocurrió un error en la consulta" });
        }
    }).catch(err => {
        console.log(err);
        res.json({ status: "fail", msg: "Ocurrió un error en la consulta" });
    })
});


// Cambia los permisos del usuario en la firma
router.post('/updateUserCompany', validarPermisosGestionarUsuarios, function (req, res, next) {
    var selectedCompany = req.user.selectedCompanyId;
    var userId = req.body.userId;
    var data = req.body.data;
    companyProvider.updateUserCompany(selectedCompany, userId, data).then(result => {
        res.json({ status: "success", data: result });
    }).catch(error => {
        console.log(error)
        res.json({ status: "fail", msg: "No se pudieron guardar los datos" });
    })
});

// Cancela la invitación
router.post('/cancelUserInvitation', validarPermisosGestionarUsuarios, function (req, res, next) {
    var selectedCompany = req.user.selectedCompanyId;
    var userId = req.body.userId;
    companyProvider.cancelUserInvitation(selectedCompany, userId).then(result => {
        res.json({ status: "success", data: result });
    }).catch(error => {
        console.log(error)
        res.json({ status: "fail", msg: "No se pudieron guardar los datos" });
    })
});

// Se sale voluntariamente de la firma
router.post('/abandonCompany', function (req, res, next) {
    req.body.userId = req.user.usuario.id;
    if (req.body.userId == null || req.body.companyId === req.user.usuario.company.id) {
        res.json({ status: "fail", msg: "No se pudieron guardar los datos" });
    } else {
        next();
    }
},
    removeUserSuscriptions, function (req, res, next) {
        var companyId = req.body.companyId;
        var userId = req.user.usuario.id;
        companyProvider.removeFromCompany(companyId, userId).then(result => {
            if (result) {
                res.json({ status: "success", data: result });
            } else {
                res.json({ status: "fail", data: "No se pudieron guardar los datos" });
            }
        }).catch(error => {
            console.log(error)
            res.json({ status: "fail", msg: "No se pudieron guardar los datos" });
        })
    });

// Es eliminado de la firma
router.post('/removeUserFromCompany', function (req, res, next) {
    req.body.companyId = req.user.selectedCompanyId;
    if (req.body.userId == null || req.body.userId === req.user.usuario.id) {
        res.json({ status: "fail", msg: "No se pudieron guardar los datos" });
    } else {
        next();
    }
}, removeUserSuscriptions, function (req, res, next) {
    var companyId = req.body.companyId;
    var userId = req.body.userId;
    companyProvider.removeFromCompany(companyId, userId).then(result => {
        if (result) {
            res.json({ status: "success", data: result });
        } else {
            res.json({ status: "fail", data: "No se pudieron guardar los datos" });
        }
    }).catch(error => {
        console.log(error)
        res.json({ status: "fail", msg: "No se pudieron guardar los datos" });
    })
});

function canAdduserToCompany(req, res, next) {
    var companyId = req.user.selectedCompanyId;
    var userEmail = req.body.email;
    var promises = [];

    companyProvider.canAddUser(companyId, userEmail).then(result => {
        if (result.success) {
            next();
        } else {
            res.json({ success: false, status: "fail", msg: result.msg })
        }
    })
}

function userExists(req, res, next) {
    var email = req.body.email;
    userProvider.userExists(email).then(exists => {
        if (exists) {
            next();
        } else {
            res.json({ status: "fail", msg: "No puede añadir a este usuario o revise la escritura del nombre de usuario ingresado." });
        }
    }).catch(err => {
        console.log(err);
        res.json({ status: "fail", msg: "Ocurrió un error en la consulta" });
    })

}

function validarPermisosGestionarUsuarios(req, res, next) {
    try {
        var selectedCompany = req.user.selectedCompanyId;
        var userId = req.user.usuario.id;
        companyProvider.loadSelectedUserCompany(userId, selectedCompany)
            .then(value => {
                var usuarioActual = new CompanyUserDto();
                usuarioActual.cargarDatos(value);
                let pp = _.filter(usuarioActual.permisos, function (o) { return o == "gestionar" || o == "admin"; });
                if (pp.length) {
                    next();
                }
            }).catch(error => {
                console.log(error);
                res.json({ status: "fail", msg: "Usted no tiene permisos para hacer eso", reload: true })
            });
    } catch (error) {
        console.log(error)
        res.json({ status: "fail", msg: "Ocurrió un error", reload: true })
    }
}

function removeUserSuscriptions(req, res, next) {
    var companyId = req.body.companyId;
    var userId = req.body.userId;
    companyProvider.removeUserSuscriptionsFromCompany(userId, companyId).then(val => {
        next();
    }).catch(error => {
        console.log(error)
        res.json({ status: "fail", msg: "Ocurrió un error", reload: true });
    })
}

module.exports = router;