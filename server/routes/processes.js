var express = require('express');
var router = express.Router();
const processProvider = require('../src/providers/process');
const clientProvider = require('../src/providers/client');
const userProvider = require('../src/providers/user');
const companyProvider = require('../src/providers/company');
const processNotifier = require('../src/providers/notifiers/processNotifier');
const _ = require("lodash");

router.post('/**', function (req, res, next) {
    //if (!req.user) {
    if (!req) {
        res.redirect("/");
    }
    next();
});

router.post('/listProcesses', function (req, res, next) {

    var company = req.user.selectedCompanyId;
    var page = req.body.page;
    var num = req.body.num;
    var filtros = req.body.filtros;
    try {
        processProvider.listProcessesToApp(company, num, page, filtros).then(result => {
            res.json({ data: result });
        }).catch(result => {
            res.json(503, { result });
        })

    } catch (error) {
        console.log(error);
        res.json({});
    }

});

router.post('/loadProcessDetail', function (req, res, next) {
    var company = req.user.selectedCompanyId;
    var processId = req.body.processId;

    try {
        processProvider.loadProcessDetail(company, processId).then(result => {
            res.json({ data: result });
        }).catch(result => {
            res.json(503, { result });
        })

    } catch (error) {
        console.log(error);
        res.json({});
    }

});

router.post('/loadActuacionesProceso', function (req, res, next) {
    var codProceso = req.body.codProceso;
    var page = req.body.page;
    var num = req.body.num;
    var index = req.body.index;

    try {
        processProvider.loadActuacionesProceso(codProceso, index, page, num).then(result => {
            res.json({ data: result });
        }).catch(result => {
            res.json(503, { result });
        })

    } catch (error) {
        console.log(error);
        res.json({});
    }

});

router.post('/reactivate', function (req, res, next) {

    try {
        var company = req.user.selectedCompanyId;
        var processId = req.body.processId;
        processProvider.reactivateProcess(company, processId).then(result => {
            res.json({ data: "Proceso reactivado con éxito.", error: null });
        }).catch(result => {
            console.log(result)
            res.json(503, { error: "Ocurrió un error al intentar reactivar el proceso." });
        })
    } catch (error) {
        console.log(error);
        res.json({ error: "Ocurrió un error al reactivar el proceso." });
    }

});

router.post('/reconsultar', function (req, res, next) {

    try {
        var company = req.user.selectedCompanyId;
        var processId = req.body.processId;
        var reconsultar = req.body.reconsultar;
        processProvider.reProcess(company, processId, reconsultar).then(result => {
            res.json({ status: "success", data: "" });
        }).catch(result => {
            console.log(result)
            res.json(503, { status: "error", msg: "Ocurrió un error al guardar la información." });
        })
    } catch (error) {
        console.log(error);
        res.json({ status: "error", error: "Ocurrió un error al guardar la información.", reload: true });
    }

});

router.post('/deleteProcess',
    getUserPermissions,
    function (req, res, next) {
        var tienePermiso = _.filter(req.body.permisos, p => {
            return p === 'editar' || p === 'admin';
        })
        if (!tienePermiso.length) {
            res.json({ success: false, msg: "El usuario no puede ejecutar la operación." });
            return;
        }
        var company = req.user.selectedCompanyId;
        var processId = req.body.processId;
        var emailUsuario = req.user.usuario.email;
        processProvider.deleteProcess(company, processId).then(result => {
            if (result) {
                processNotifier.procesoEliminado(result, emailUsuario);
                res.json({ success: true, msg: '' });
            } else {
                res.json({ success: false, msg: 'El proceso no pudo ser borrado' });

            }
        }).catch(result => {
            res.json({ success: false, msg: "Ocurrió un error al borrar el proceso." });
        })
    });

router.post('/updateBadProcess',
    function (req, res, next) {
        req.body.procesos = [req.body.proceso];
        next();
    }, checkExistences,
    function (req, res, next) {

        try {
            var company = req.user.selectedCompanyId;
            var processId = req.body.processId;
            var proceso = req.body.proceso;
            processProvider.updateBadProcess(company, processId, proceso).then(result => {
                res.json({ success: true, status: "success" });
            }).catch(result => {
                console.log(result);
                res.json({ success: false, status: "fail" });
            })

        } catch (error) {
            console.log(error);
            res.json(503, { error: "Ocurrió un error al actualizar el proceso." });
        }

    });


router.post('/addMultiple', checkProcessLimit, checkExistences, getCurrentSuscriptor,
    function (req, res, next) {
        try {
            var procesos = req.body.procesos;
            var company = req.user.selectedCompanyId;
            var suscriptor = req.body.suscriptor;
            processProvider.addMultipleProcesses(company, procesos, suscriptor).then(result => {
                res.json({ success: true, status: "success" });
            })
        } catch (error) {
            console.log(error);
            res.json({ error, success: false, status: "error" });
        }
    });

router.post('/subscribeToProcess', function (req, res, next) {
    var processId = req.body.processId;
    var userId = req.user.usuario.id;
    var channels = ["email", "sms", "push"];
    processProvider.subscribeToProcess(processId, userId, channels).then(result => {
        if (result) {
            res.json({ success: true, status: "success" });
        } else {
            res.json({ success: false, status: "fail" });
        }
    })
});

router.post('/unSubscribeFromProcess', function (req, res, next) {
    var processId = req.body.processId;
    var userId = req.user.usuario.id;
    processProvider.unSubscribeFromProcess(processId, userId).then(result => {
        if (result) {
            res.json({ success: true, status: "success" });
        } else {
            res.json({ success: false, status: "fail" });
        }
    })
});

module.exports = router;



function checkExistences(req, res, next) {

    var procesos = req.body.procesos;
    var company = req.user.selectedCompanyId;

    processProvider.checkExistences(company, procesos).then(list => {
        if (list.length > 0) {
            res.json({ success: false, repetidos: list, status: "fail" });
        } else {
            next();
        }
    }).catch(error => {
        res.json({ error });
    })
}

function checkProcessLimit(req, res, next) {
    // Check if user can add more processes
    var numIn = req.body.procesos.length;
    clientProvider.canAddProcesses(req.user.selectedCompanyId, numIn)
        .then(data => {
            if (data.success) {
                next();
            } else {
                res.json(data);
            }
        }).catch(err => {
            res.json(err);
        });
}

function getCurrentSuscriptor(req, res, next) {
    var suscriptor;
    var userId = req.user.usuario.id;
    suscriptor = userProvider.getUserNotificationChannels(userId)
        .then(data => {
            req.body.suscriptor = data;
            req.body.suscriptor.userId = userId;
            next();
        }).catch(err => {
            res.json({ success: false, status: "fail" });
        });
}

function getUserPermissions(req, res, next) {
    var userId = req.user.usuario.id;
    var companyId = req.user.selectedCompanyId;
    companyProvider.getUserPermissions(userId, companyId).then(val => {
        req.body.permisos = val ? val : [];
        next();
    }).catch(error => {
        res.json({ success: false, msg: "El usuario no tiene permisos para ejecutar la acción." });
    })
}