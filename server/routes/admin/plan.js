var express = require('express');
const _ = require("lodash");
var router = express.Router();
const clientProvider = require('../../src/providers/client');
const userProvider = require('../../src/providers/user');
const companyProvider = require('../../src/providers/company');
const adminProvider = require('../../src/providers/admin');
const planProvider = require('../../src/providers/plan');


const ACCION_EDITAR = "editar";
const ACCION_ASIGNAR = "asignar";

router.post('/**', function (req, res, next) {
  if (!req.user) {
    // res.json(503, {});
    next();
  } else {
    next();

  }
});

router.post('/fetchPlans', function (req, res, next) {
  planProvider.fetchPlans().then(data => {
    res.json(data);
  })
});

router.post('/fetchPlanById', function (req, res, next) {
  var planId = req.body.planId;
  planProvider.fetchPlanById(planId).then(data => {
    res.json(data);
  })
});

router.post('/savePlan', function (req, res, next) {
  var planData = req.body.plan;
  var promise;
  if (planData.id) {
    promise = planProvider.editPlan(planData);
  } else {
    promise = planProvider.createPlan(planData);
  }
  promise.then(data => {
    var status = data ? "success" : "fail";
    res.json({ status, data });
  })
});

router.post('/removePlan', function (req, res, next) {
  var planId = req.body.planId;
  planProvider.removePlan(planId).then(data => {
    res.json(data);
  });
});

module.exports = router;
