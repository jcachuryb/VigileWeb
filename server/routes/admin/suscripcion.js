var express = require('express');
const _ = require("lodash");
var router = express.Router();

const notifier = require('../../src/providers/notifiers/notifier');
const accountProvider = require('../../src/providers/accountProvider');

router.post('/**', function (req, res, next) {
  if (!req.user) {
    // res.json(503, {});
    next();
  } else {
    next();

  }
});

router.post('/activarPlan', function (req, res, next) {

  var userId = req.body.userId;
  var planInfo = req.body.plan;
  accountProvider.activarSuscripcion(planInfo, userId).then(data => {
    var msg = "";
    if (data) {
      notifier.nuevaSuscripcion(data.datos.email, data);
    } else {
      msg = "No se pudo actualizar la cuenta para el usuario " + userId;
    }
    res.json({ status: data ? "success" : "fail", msg });
  });

});



module.exports = router;
