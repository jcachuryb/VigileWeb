var express = require('express');
var router = express.Router();
const userProvider = require('../src/providers/user');
const processProvider = require('../src/providers/process');
const companyProvider = require('../src/providers/company');
const accountProvider = require('../src/providers/accountProvider');
const taskProvider = require('../src/providers/taskProvider');
const mailService = require('../src/providers/notifiers/mailservice');



router.post('/updateProcesses', function (req, res, next) {
    try {
        var params = JSON.parse(req.body.procesos);
        processProvider.saveProcesses(params).then(result => {
            res.json(result ? "Éxito al guardar." : "No se pudieron guardar los procesos");
        }).catch(result => {
            console.log(result)
            res.json("Error al guardar los procesos.");
        })
    } catch (error) {
        console.log("Error guardando los procesos ", error);
        res.json("No se pudieron guardar los procesos")
    }
});

router.post('/loadCompanies', function (req, res, next) {
    try {
        companyProvider.loadActiveAccounts().then(result => {
            res.json(result);
        }).catch(result => {
            res.json(503, { result });
        })

    } catch (error) {
        console.log(error);
        res.json({});
    }

});

router.post('/loadCompanyProcesses', function (req, res, next) {
    try {
        var company = req.body.company;
        var page = req.body.page;
        var num = req.body.num;
        processProvider.loadToBot(company, page, num).then(result => {
            res.json(result);
        }).catch(result => {
            res.json(503, { result });
        })

    } catch (error) {
        console.log(error);
        res.json({});
    }

});

router.post('/getInfoProcessStatus', function (req, res, next) {
    var codProceso = req.body.codProceso;
    try {
        processProvider.getInfoProcessStatus(codProceso).then(result => {
            res.json({ data: result });
        }).catch(result => {
            res.json(503, { result });
        })

    } catch (error) {
        console.log(error);
        res.json({});
    }
});
router.post('/saveMultipleStatus', function (req, res, next) {
    var data = JSON.parse(req.body.estados);
    try {
        processProvider.saveMultipleStatus(data).then(result => {
            res.json(true);
        }).catch(result => {
            console.log(result)
            res.json(false);
        })

    } catch (error) {
        console.log(error);
        res.json({});
    }
});

router.post('/initTask', function (req, res, next) {

    var tipo = req.body.tipo;
    taskProvider.initTask(tipo).then(result => {
        res.json(result);
    }).catch(result => {
        console.log(result)
        res.json(null);
    })
});
router.post('/endTask', function (req, res, next) {

    var id = req.body.id;
    var exito = req.body.exito;
    var comentarios = req.body.comentarios;
    var descartar = req.body.descartar;

    taskProvider.endTask(id, exito, comentarios, descartar).then(result => {
        res.json(result);
    }).catch(result => {
        console.log(result)
        res.json(null);
    })
});


router.post('/restartProcess', function (req, res, next) {
    var processId = JSON.parse(req.body.processId);
    var codProceso = JSON.parse(req.body.codProceso);
    console.log("Resetear proceso ", processId, codProceso);
    try {
        processProvider.botToreProcess(processId, codProceso).then(result => {
            res.json(result);
        }).catch(result => {
            console.log(result)
            res.json(null);
        })

    } catch (error) {
        console.log(error);
        res.json({});
    }
});

router.post('/deactivateAccounts', function (req, res, next) {

    var page = 0;
    var num = 10;
    deactivateAccounts(page, num, null).then(result => {
        var status = result === "Acabó" ? "success" : "fail";
        var msg = "";
        res.json({ status, msg });
    })
});


module.exports = router;


function deactivateAccounts(page, num, resultados) {

    if (resultados && resultados < num) {
        return "Acabó";
    }

    return accountProvider.deactivateAccounts(page, num).then(result => {
        return deactivateAccounts(++page, num, result);
    }).catch(result => {
        return "Error";
    })
}