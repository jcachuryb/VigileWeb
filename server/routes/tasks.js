var express = require('express');
var router = express.Router();
const userProvider = require('../src/providers/user');
const taskProvider = require('../src/providers/taskProvider');
const adminNotifier = require('../src/providers/notifiers/adminNotifier');
const _ = require("lodash");

router.post('/fetchTasks', function (req, res, next) {
    var page = req.body.pagina;
    var num = req.body.num;

    taskProvider.loadTasks(page, num).then(val => {
        var status = "FAIL";
        if (val) {
            status = "SUCCESS";
        }
        res.json({ status, list: val });
    });
});

router.post('/removeRecord', function (req, res, next) {
    var idTask = req.body.idTask;
    taskProvider.removeRecord(idTask).then(val => {
        var status = "FAIL";
        if (val) {
            status = "SUCCESS";
        }
        res.json({ status });
    });

});

module.exports = router;