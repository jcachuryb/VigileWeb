var express = require('express');
const _ = require("lodash");
var router = express.Router();
const boardProvider = require('../src/providers/board');

var BoardDto = require('../src/models/board');
var ListDto = require('../src/models/list');

router.post('/**', function (req, res, next) {
  if (!req.user) {
    res.json(503, {});
  } else {
    next();
  }
});


router.post('/addBoard', function (req, res, next) {
  var nombre = req.body.nombre;
  var company = req.user.selectedCompanyId;
  boardProvider.addBoard(nombre, company).then(tableroNuevo => {
    var board = new BoardDto();
    try {
      board.cargarDatos(tableroNuevo);
      res.json({ valid: true, board });
    } catch (error) {
      res.json({ valid: false, msg: error });
    }
  })
});


router.post('/editBoard', function (req, res, next) {
  var nombre = req.body.nombre;
  var activo = req.body.activo;
  var boardId = req.body.boardId;
  boardProvider.editBoard(boardId, nombre, activo).then(result => {
    var valid = result ? true : false;
    res.json({ valid });
  })
});


router.post('/fetchBoards', function (req, res, next) {
  var company = req.user.selectedCompanyId;
  boardProvider.fetchBoards(company).then(boards => {
    res.json(boards);
  })
});

// ----------------------------------------------------------------------------------


router.post('/addList', function (req, res, next) {
  var nombre = req.body.nombre;
  var orden = req.body.orden;
  var boardId = req.body.boardId;

  boardProvider.addList(boardId, nombre, orden).then(listaNueva => {
    var list = new ListDto();
    try {
      list.cargarDatos(listaNueva);
      res.json({ valid: true, list });
    } catch (error) {
      res.json({ valid: false, msg: error });
    }
  })
});


router.post('/editList', function (req, res, next) {
  var nombre = req.body.nombre;
  var activa = req.body.activa;
  var orden = req.body.orden;
  var listId = req.body.listId;
  boardProvider.editList(listId, nombre, activa, orden).then(result => {
    var valid = result ? true : false;
    res.json({ valid });
  })
});

router.post('/saveListProcesses', function (req, res, next) {
  var procesos = req.body.procesos;
  var listId = req.body.listId;
  boardProvider.saveListProcesses(listId, procesos).then(result => {
    var valid = result ? true : false;
    res.json({ valid });
  })
});

router.post('/fetchLists', function (req, res, next) {
  var boardId = req.user.boardId;
  boardProvider.fetchLists(boardId).then(lists => {
    res.json(lists);
  })
});

router.post('/fetchListProcesses', function (req, res, next) {
  var company = req.user.selectedCompanyId;
  var boardId = req.user.boardId;
  var listId = req.user.listId;
  boardProvider.fetchListProcesses(company, boardId, listId).then(procesos => {
    res.json(procesos);
  })
});

module.exports = router;
