var express = require('express');
var momentjs = require('moment');
var router = express.Router();
const clientProvider = require('../src/providers/client');
const processProvider = require('../src/providers/process');
const userProvider = require('../src/providers/user');
const notifier = require('../src/providers/notifiers/notifier');
const processNotifier = require('../src/providers/notifiers/processNotifier');
const companyProvider = require('../src/providers/company');
const accountProvider = require('../src/providers/accountProvider');
const menuProvider = require('../src/providers/menuProvider');
var shortUrl = require('node-url-shortener');


/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});
router.post('/**', function (req, res, next) {
  //if (!req.user) {
  if (!req) {
    res.redirect("/");
  }
  req.user = {};
  req.user.company = "aDR20urd0q";
  next();
});

router.post('/testFilter', function (req, res, next) {
  momentjs.locale('es');

  var fechas = ['05 sep 2017', '29 ago 2017', '06 mar 2017', '10 oct 2016',
    '07 jul 2016', '09 abr 2018', '27 Abr 2018', '08 Ago 2017', '28 may 2014'];
  for (let i = 0; i < fechas.length; i++) {
    try {
      var a = new Date(fechas[i]);
      var f = momentjs(a).format();
      console.log(f);
    } catch (error) {
      console.error(error);
    }

  }
  res.json();
});

router.post('/testFetch', function (req, res, next) {

  var channels = ["email", "sms", "push"];
  processProvider.unSubscribeFromProcess("iR65Mioq9a", "mxyH32PFqK", channels).then(valor => {
    var a = "test"
    console.log(a)
    res.json(valor);
  }).catch(error => {
    res.json({ status: "fail", msg: "No se pudieron guardar los datos" });
  })
})

router.post('/testReg', function (req, res, next) {
  var processId = "zRDpXzCqdv"
  var codProceso = "11001310500920100044101"
  try {
    processProvider.botToreProcess(processId, codProceso).then(result => {
      res.json(result);
    }).catch(result => {
      console.log(result)
      res.json(false);
    })

  } catch (error) {
    console.log(error);
    res.json({});
  }
});

router.post('/updateProcesses', function (req, res, next) {
  var procesos = JSON.parse(req.body.procesos);
  res.json("Recibidos " + procesos.length);
});

router.post('/testGeneric', function (req, res, next) {
  var userId = req.body.userId;
  var companyId = req.body.companyId;
  companyProvider.removeUserSuscriptionsFromCompany(userId, companyId).then(val => {
    res.json(val);
  })

});

router.post('/testMail', function (req, res, next) {
  var val = "";
  shortUrl.short('http://vigileapp.com', function (err, url) {
    console.log(url)
    res.json({ url });
  });

});


router.post('/testooo', function (req, res, next) {

  var userId = "";
  var roles = ["client"];
  var permisos = [
    "consultar"
  ];
  var items = userProvider.cargarMenu(userId, roles, permisos);
  res.json(items);
});

module.exports = router;

function dd(variable, varName) {
  var varNameOutput;

  varName = varName || '';
  varNameOutput = varName ? varName + ':' : '';

  console.warn(varNameOutput, variable, ' (' + (typeof variable) + ')');
}

function loadMockStatus() {
  try {
    var any = { "estados": "[{\"codProceso\":\"11001400303420170078000\",\"actuacion\":\"RADICACIÓN DE PROCESO\",\"fechaActuacion\":\"2017-06-09 12:00\",\"anotacion\":\"ACTUACIÓN DE RADICACIÓN DE PROCESO REALIZADA EL 09/06/2017 A LAS 11:54:21\",\"fechaInicioTermino\":\"2017-06-09 12:00\",\"fechaFinTermino\":\"2017-06-09 12:00\",\"fechaRegistro\":\"2017-06-09 12:00\",\"num\":12},{\"codProceso\":\"11001400303420170078000\",\"actuacion\":\"AL DESPACHO\",\"fechaActuacion\":\"2017-06-09 12:00\",\"anotacion\":\"\",\"fechaInicioTermino\":\"\",\"fechaFinTermino\":\"\",\"fechaRegistro\":\"2017-06-09 12:00\",\"num\":11},{\"codProceso\":\"11001400303420170078000\",\"actuacion\":\"AUTO ADMITE TUTELA\",\"fechaActuacion\":\"2017-06-09 12:00\",\"anotacion\":\"\",\"fechaInicioTermino\":\"\",\"fechaFinTermino\":\"\",\"fechaRegistro\":\"2017-06-09 12:00\",\"num\":10},{\"codProceso\":\"11001400303420170078000\",\"actuacion\":\"OFICIO ELABORADO\",\"fechaActuacion\":\"2017-06-09 12:00\",\"anotacion\":\"\",\"fechaInicioTermino\":\"\",\"fechaFinTermino\":\"\",\"fechaRegistro\":\"2017-06-09 12:00\",\"num\":9},{\"codProceso\":\"11001400303420170078000\",\"actuacion\":\"ENVÍO COMUNICACIONES\",\"fechaActuacion\":\"2017-06-12 12:00\",\"anotacion\":\"NOTIFICACION ACCIONADOS. VIA CORREO ELECTRONICO.\",\"fechaInicioTermino\":\"\",\"fechaFinTermino\":\"\",\"fechaRegistro\":\"2017-06-12 12:00\",\"num\":8},{\"codProceso\":\"11001400303420170078000\",\"actuacion\":\"ENVÍO COMUNICACIONES\",\"fechaActuacion\":\"2017-06-13 12:00\",\"anotacion\":\"TELEX\",\"fechaInicioTermino\":\"\",\"fechaFinTermino\":\"\",\"fechaRegistro\":\"2017-06-13 12:00\",\"num\":7},{\"codProceso\":\"11001400303420170078000\",\"actuacion\":\"RECEPCIÓN MEMORIAL\",\"fechaActuacion\":\"2017-06-14 12:00\",\"anotacion\":\"CONTESTACION PORVENIR. COREO ELECTRONICO.\",\"fechaInicioTermino\":\"\",\"fechaFinTermino\":\"\",\"fechaRegistro\":\"2017-06-14 12:00\",\"num\":6},{\"codProceso\":\"11001400303420170078000\",\"actuacion\":\"AL DESPACHO\",\"fechaActuacion\":\"2017-06-14 12:00\",\"anotacion\":\"\",\"fechaInicioTermino\":\"\",\"fechaFinTermino\":\"\",\"fechaRegistro\":\"2017-06-14 12:00\",\"num\":5},{\"codProceso\":\"11001400303420170078000\",\"actuacion\":\"RECEPCIÓN MEMORIAL\",\"fechaActuacion\":\"2017-06-14 12:00\",\"anotacion\":\"RESPUESTA PORVENIR.\",\"fechaInicioTermino\":\"\",\"fechaFinTermino\":\"\",\"fechaRegistro\":\"2017-06-14 12:00\",\"num\":4},{\"codProceso\":\"11001400303420170078000\",\"actuacion\":\"SENTENCIA TUTELA PRIMERA INSTANCIA\",\"fechaActuacion\":\"2017-06-23 12:00\",\"anotacion\":\"\",\"fechaInicioTermino\":\"\",\"fechaFinTermino\":\"\",\"fechaRegistro\":\"2017-06-23 12:00\",\"num\":3},{\"codProceso\":\"11001400303420170078000\",\"actuacion\":\"OFICIO ELABORADO\",\"fechaActuacion\":\"2017-06-23 12:00\",\"anotacion\":\"\",\"fechaInicioTermino\":\"\",\"fechaFinTermino\":\"\",\"fechaRegistro\":\"2017-06-23 12:00\",\"num\":2},{\"codProceso\":\"11001400303420170078000\",\"actuacion\":\"ENVÍO COMUNICACIONES\",\"fechaActuacion\":\"2017-06-23 12:00\",\"anotacion\":\"NOTIFICACIÓN ACCIONADOS VIA CORREO ELECTRONICO- Y ACCIONANTE VIA TELEGRAMA.\",\"fechaInicioTermino\":\"\",\"fechaFinTermino\":\"\",\"fechaRegistro\":\"2017-06-23 12:00\",\"num\":1},{\"codProceso\":\"11001400303420170078000\",\"actuacion\":\"RECEPCIÓN MEMORIAL\",\"fechaActuacion\":\"2017-06-27 12:00\",\"anotacion\":\"CONTESTACION COLPENSIONES. 2 FOLIOS\",\"fechaInicioTermino\":\"\",\"fechaFinTermino\":\"\",\"fechaRegistro\":\"2017-06-27 12:00\",\"num\":0}]" };
    var a = JSON.parse(any.estados);
    return any.estados;
  } catch (error) {
    console.log(error);
    return null;
  }
}