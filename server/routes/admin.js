var express = require('express');
const _ = require("lodash");
var router = express.Router();
const clientProvider = require('../src/providers/client');
const userProvider = require('../src/providers/user');
const companyProvider = require('../src/providers/company');
const adminProvider = require('../src/providers/admin');


var CompanyUserDto = require('../src/models/companyUser');

const ACCION_EDITAR = "editar";
const ACCION_ASIGNAR = "asignar";

router.post('/**', function (req, res, next) {
  if (!req.user) {
      res.json(503, {});
  } else {
      next();
  }
});


router.post('/fetchUserList', function(req, res, next) {
  var filter = req.body.filter;
  adminProvider.fetchUsers(filter).then(users=>{
    res.json(users);
  })
});

router.post('/fetchUserData', function(req, res, next) {
  var userId = req.body.userId;
  adminProvider.fetchUserData(userId).then(data=>{
    res.json(data);
  })
});

module.exports = router;
