var express = require('express');
var router = express.Router();
const userProvider = require('../src/providers/user');
const feedbackProvider = require('../src/providers/feedback');
const adminNotifier = require('../src/providers/notifiers/adminNotifier');
const _ = require("lodash");

router.post('/logged', function (req, res, next) {
  var resp = req.user != null ? true : false;
  res.json({ loggedIn: resp });
});

router.post('/logout', function (req, res, next) {
  req.logout();
  res.redirect('/');
});

router.post('/feedback', function (req, res, next) {
  var email = req.body.email;
  var mensaje = req.body.mensaje;
  var data = req.body.data;
  var tipo = req.body.tipo;
  feedbackProvider.addFeedback(email, mensaje, tipo, data)
    .then(obj => {
      adminNotifier.feedbackRecibido(obj);
      res.json({ msg: "Su comentario ha sido guardado", status: "success" });
    }).catch(error => {
      console.log(error)
      res.json({ msg: "Ha ocurrido un error.", status: "fail" });
    });
});

router.post('/loadUserMenu', function (req, res, next) {
  var userId = req.user.usuario.id;
  var roles = []
  _.forEach(req.user.usuario.roles, function(rol) {
    roles.push(rol.nombre);
  });
  var permisos = req.user.usuario.selectedCompany.permisos;
  var items = userProvider.cargarMenu(userId, roles, permisos);
  res.json(items);
});

router.post('/contact', function (req, res, next) {
  var captcha = req.body.captcha;

  if (req.body.contacto) {
    adminNotifier.contactoRecibido(req.body.contacto);
  }

  res.json({ msg: "Su comentario ha sido guardado", status: "success" });

});

router.post('/loadUserData', function (req, res, next) {
  var user = req.user;
  res.json(user);
});
module.exports = router;