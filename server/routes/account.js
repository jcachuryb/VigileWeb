var express = require('express');
var router = express.Router();
const clientProvider = require('../src/providers/client');
const companyProvider = require('../src/providers/company');
const accountProvider = require('../src/providers/accountProvider');
const userCompanyDto = require('../src/models/companyUser');


router.post('/**', function (req, res, next) {
    if (!req.user) {
        res.json(503, {});
    } else {
        next();
    }
});

router.post('/loadSelCompanyAccountInfo', function (req, res) {
    var companyId = req.user.selectedCompanyId;
    accountProvider.loadSelCompanyAccountInfo(companyId).then(data => {
        res.json({ cuenta: data });
    }).catch(err => {
        res.json({ error: err });
    });
});

router.post('/loadOwningCompanyAccountInfo', function (req, res) {
    var companyId = req.user.usuario.company.id;
    accountProvider.loadAccountInfo(companyId).then(data => {
        res.json({ cuenta: data });
    }).catch(err => {
        res.json({ error: err });
    });
});

router.post('/isOwningAccountActive', function (req, res) {
    var companyId = req.user.usuario.company.id;
    accountProvider.isAccountActive(companyId).then(data => {
        res.json({ activa: data });
    }).catch(err => {
        res.json({ error: err });
    });
});

router.post('/isSelCompanyAccountActive', function (req, res) {
    var companyId = req.user.selectedCompanyId;
    accountProvider.isAccountActive(companyId).then(data => {
        res.json({ activa: data });
    }).catch(err => {
        res.json({ error: err });
    });
});

module.exports = router;