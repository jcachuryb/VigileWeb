var express = require('express');
var router = express.Router();
var passport = require('passport');
const userProvider = require('../src/providers/user');
var mailService = require('../src/providers/notifiers/mailservice');
var notifier = require('../src/providers/notifiers/notifier');
const companyProvider = require('../src/providers/company');
const accountProvider = require('../src/providers/accountProvider');
var _ = require('underscore');

var Usuario = require('../src/models/usuario');
var Rol = require('../src/models/rol');
var Company = require('../src/models/company');
var AccountDto = require('../src/models/account');
var CompanyUserDto = require('../src/models/companyUser');

router.get('/token',
    passport.authenticate('oauth2'));


router.post('/authorization',
    passport.authenticate('oauth2', { failureRedirect: '/login' }),
    function (req, res) {
        console.log("Callback");
        // Successful authentication, redirect home.
        res.redirect('/');
    });


router.post('/registro', validarExistencia, function (req, res, next) {
    if (req.body.existeUsuario) {
        res.json({ msg: "El correo ya está registrado", status: "fail" });
        return;
    }
    var nombre = req.body.nombre;
    var email = req.body.email;
    var password = req.body.password;
    req.body.key = userProvider.getRandomString(20);
    var initData = { nombre: nombre, key: req.body.key };
    userProvider.signUp(email, password, initData).then(result => {
        next();
    }).catch(err => {
        console.log(err)
        res.json({ err: err, msg: "Registro fallido", status: "error" });
    });
}, function (req, res, next) {
    notifier.registroUsuario(req.body.nombre, req.body.email, req.body.key);
    res.json({ msg: "Registro exitoso, mensaje enviado", status: "success" });
});

router.post('/completarRegistro', function (req, res, next) {
    next();
}, function (req, res, next) {
    var key = req.body.key;
    userProvider.completarRegistro(key).then(result => {
        var status = result ? "success" : "fail";
        if (result) {
            req.body.email = result.get('username');
            req.body.nombre = result.get('nombre');
            next();
        } else {
            res.json({ msg: "Cuenta verificada.", status: status });
        }
    }).catch(error => {
        console.log(error)
        res.json({ msg: "No pudo confirmarse el correo.", status: "error" });
    })

}, function (req, res, next) {
    // Crearle una Compañía y una cuenta
    var email = req.body.email;
    var nombre = req.body.nombre;
    var datos = { email: email, nombre: nombre };
    userProvider.crearCompany(email).then(result => {
        var status = result ? "success" : "fail";

        res.json({ status: status, datos });
    }).catch(error => {
        console.log(error);
        res.json({ error });
    })

});

router.get('/google',
    passport.authenticate('google', {
        scope: [
            'https://www.googleapis.com/auth/plus.login',
            'https://www.googleapis.com/auth/plus.profile.emails.read'
        ]
    }
    ));
router.get('/google/callback',
    passport.authenticate('google', { failureRedirect: '/' }),
    function (req, res, next) {
        console.log("//Load Company data");
        next();
    },
    function (req, res) {
        res.redirect("/#/pages/dashboard");
    });

// Login con user y password
router.post('/loginCredentials',
    passport.authenticate('local', {
        failureRedirect: '/login',
        failureFlash: 'Invalid email or password.'
    }
    ), function (req, res, next) {
        var status = req.user ? "success" : "error";
        var msg = req.user ? "" : "Usuario o contraseña inválidos";
        if (req.user) {
            next();
        } else {
            res.json({ msg: msg, status: status });
        }
    }, function (req, res, next) {
        console.log("//Load Company data");
        var userData = req.user.usuario;

        var loadCompany = companyProvider.loadCompany(userData.company.id);
        var loadAccount = accountProvider.loadAccountInfo(userData.company.id);
        var loadUserCompany = companyProvider.loadSelectedUserCompany(userData.id, userData.company.id);

        Promise.all([loadCompany, loadAccount, loadUserCompany]).then(values => {
            // company
            var valCompany = values[0];
            userData.company.nombre = valCompany.get("nombre");
            userData.company.configurada = valCompany.get("configurada");
            req.user.selectedCompanyId = userData.company.id;
            // account
            var valAccount = values[1];
            userData.cuenta = new AccountDto();
            userData.cuenta.cargarDatos(valAccount);

            // userCompany
            var valUserCompany = values[2];
            userData.selectedCompany = new CompanyUserDto();
            userData.selectedCompany.cargarDatos(valUserCompany);
            next();
        }).catch(errors => {
            console.log(errors);
            res.json({ msg: "Shit", status: "fail" });
        });

    }, function (req, res) {
        var currentUser = req.user;
        res.json({ msg: "", status: "success", data: currentUser });
    });

router.post('/logout', logout);

router.post('/solicitarCambioClave', function (req, res, next) {
    var email = req.body.email;
    userProvider.solicitarRecuperarClave(email).then(respuesta => {
        if (respuesta) {
            
            notifier.solicitarRecuperarClave(respuesta.userId, email, respuesta.codigo);
            res.json({ msg: "", status: "success" });
            return;
        } else {
            res.json({ msg: "No existe una cuenta asociada a ese correo electrónico", status: "fail" });
        }
    })
})

router.post('/recuperarClaveConfirmacion', validarClaveToken, cambiarClaveUsuario);

module.exports = router;

function validarExistencia(req, res, next) {
    var email = req.body.email;
    if (!email) {
        res.json({ msg: "", status: "fail" });
        return;
    }
    userProvider.userExists(email).then(result => {
        if (result) {
            req.body.existeUsuario = true;
        } else {
            req.body.existeUsuario = false;
        }
        next();
    }).catch(err => {
        res.json({ error: err });
    });
}
function logout(req, res) {
    console.log("Log out");
    req.logout();
    res.json({ msg: "", status: "success" });
}

function validarClaveToken(req, res, next) {
    var token = req.body.token;
    var userId = req.body.usuario;
    userProvider.getInfoCambioClave(userId).then(result => {
        if (result) {
            
            var valido = true;
            var ahora = new Date().getTime();
            if (ahora > result.validoHasta) {
                res.json({ status: "fail", msg: "Esta clave ha caducado. Por favor, realice el proceso nuevamente." });
                return;
            }

            valido = valido && (token === result.codigo);
            if (valido) {
                next();
                return;
            }
        }
        res.json({ status: "fail", msg: "" });
    }).catch(err => {
        res.json({ error: err });
    });
}

function cambiarClaveUsuario(req, res, next) {
    var nuevaClave = req.body.nuevaClave;
    var userId = req.body.usuario;
    userProvider.cambiarClave(userId, nuevaClave).then(result => {
        var status = result ? "success" : "fail";
        var msg = result ? "" : "Ocurrió un error con los datos";
        res.json({ status: status, msg: msg });
    });
}