var express = require('express');
var router = express.Router();
const clientProvider = require('../src/providers/client');
const companyProvider = require('../src/providers/company');
const userCompanyDto = require('../src/models/companyUser');


router.post('/**', function (req, res, next) {
    if (!req.user) {
        res.json(503, {});
    } else {
        next();

    }
    //req.user = {};
    //req.user.company = "aDR20urd0q";
});

router.post('/getUserCompany', function (req, res) {
    // Se obtienen los datos de la compañía y se retornan
    clientProvider.getCompanyById(req.user.usuario.company.id).then(data => {
        res.json({ company: data });
    }).catch(err => {
        res.json({ error: err });
    });
});

router.post('/getCurrentUserCompany', function (req, res, next) {
    //Validamos que el usuario pueda manipular o ver esta compañía 
    next();
}, function (req, res) {
    var userCompanyId = req.user.usuario.selectedCompany.id;
    companyProvider.loadSelectedUserCompanyById(userCompanyId).then(data => {
        var userCompany = new userCompanyDto();
        userCompany.cargarDatos(data);

        res.json({ userCompany });
    }).catch(err => {
        res.json({ error: err });
    });
});

router.post('/getOwnCompanyProcessSummary', function (req, res, next) {
    //Validamos que el usuario pueda manipular o ver esta compañía 

    next();
}, function (req, res) {
    var stats = null;
    clientProvider.getCompanyProcessSummary(req.user.usuario.company.id).then(data => {
        stats = data;
        res.json({ stats: stats });
    }).catch(err => {
        res.json({ error: err });
    });

});

router.post('/getSelCompanyProcessSummary', function (req, res, next) {
    //Validamos que el usuario pueda manipular o ver esta compañía 
    companyProvider.getUserPermissions(req.user.usuario.id, req.user.selectedCompanyId).then(val=>{
        if (val) {
            next();
        }else{
            res.json({ status: 'fail', message: "Access denied" });
        }
    }).catch(err => {
        res.json({ error: err });
    });

}, function (req, res) {
    var stats = null;
    clientProvider.getCompanyProcessSummary(req.user.selectedCompanyId).then(data => {
        stats = data;
        res.json({ stats: stats });
    }).catch(err => {
        res.json({ error: err });
    });

});

router.post('/updateClientCompany', function (req, res) {
    var data = req.body.company;
    clientProvider.updateClientCompany(req.user.usuario.company.id, data).then(result => {
        var usuario = req.user.usuario;
        companyProvider.loadCompany(usuario.company.id).then(value => {
            req.user.usuario.company.nombre = value.get("nombre");
            req.user.usuario.company.configurada = value.get("configurada");
            res.json({ status: "success" });
        });

    }).catch(err => {
        res.json({ error: err });
    });
});

module.exports = router;