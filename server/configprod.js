module.exports = {
  parse: {
    serverURL: "https://parseapi.back4app.com",
    appID: "ijaqncMxWK4cTuzTIUgs1UiZuUh1hsVS4uchSVsx",
    javascriptKey: "1acEEidNSeyUEJXuYvADx8ErozFEgBAhG06yvFj2",
    masterKey: "YZ70HIdkyEGzCjNVO9efdm0p4rvGZES6NsM6prRD"
  },

  mailservice: {
    from: 'Vigile - Consulta de procesos judiciales <lebufesolution@gmail.com>',
    server: 'https://vigileapp.com/',
    confirmPage: 'https://vigileapp.com/#/reg/confirmPage;key=',
    processDetailPage: 'https://vigileapp.com/#/pages/processes/lista;objectId=',
  },
  facebook: {
    clientID: 'get_your_own',
    clientSecret: 'get_your_own',
    callbackURL: 'http://127.0.0.1:1337/auth/facebook/callback'
  },
  twitter: {
    consumerKey: 'get_your_own',
    consumerSecret: 'get_your_own',
    callbackURL: "http://127.0.0.1:1337/auth/twitter/callback"
  },
  github: {
    clientID: 'get_your_own',
    clientSecret: 'get_your_own',
    callbackURL: "http://127.0.0.1:1337/auth/github/callback"
  },
  google: {
    clientID: "291387265126-tmvjr3in4iau52cr00b4u3cqrfjtl7vb.apps.googleusercontent.com",
    clientSecret: "a_yqKvL46YQumSaoh-O7f0G2",
    callbackURL: "https://vigileapp.com/api/auth/google/callback"
  },
  instagram: {
    clientID: 'get_your_own',
    clientSecret: 'get_your_own',
    callbackURL: 'http://127.0.0.1:1337/auth/instagram/callback'
  },

  db: {
    url: "mongodb://localhost:27017/test"
  },

  emailAdmins : ["jcachuryb@outlook.com", "jcachury@htsoft.co"],

  pages : {
    root : "vigileapp.com/#/",
    confirmarCuenta: 'registro/confirmar-correo/',
    processDetailPage: 'pages/processes/lista;objectId=',
    adminFirmas : "pages/firmas",
    cambiarClave: "recuperar-clave/confirmacion/",
    tarifas: "inicio/planes",
  }


};