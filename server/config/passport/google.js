'use strict';

const env = process.env.NODE_ENV || 'development';

var controller = require('../../controller');
const config = controller.getConfigOptions();

const mongoose = require('mongoose');
const GoogleStrategy = require('passport-google-oauth2').Strategy;
const User = mongoose.model('User');
const userProvider = require('../../src/providers/user');

/**
 * Expose
 */

module.exports = new GoogleStrategy({
  clientID: config.google.clientID,
  clientSecret: config.google.clientSecret,
  callbackURL: config.google.callbackURL
},
  function (accessToken, refreshToken, profile, done) {
    //dd(accessToken, "accessToken");
    //console.log("User email:" + profile.email);
    var login = userProvider.login(profile.email);
    login.then(function (user) {
      if (user) {
        console.log("El usuario SÍ existe");
        // Obtener los roles 
        user.roles = [];
        var query = user.relation("roles").query();
        query.find({
          success: function (result) {
            var roles = ["default"];
            for (var i = 0; i < result.length; i++) {
              roles.push(result[i].attributes.name)
            }
            done(null, { user, roles });
          },
          error: function (err) {
            done(err, null);
          }

        })
      } else {
        console.log("El usuario NO existe o está inhabilitado");
        done(null, null);
      }
    });
    login.catch(function (err) {
      console.log("Ocurrió un error en la autenticación");
      done(err, profile);
    });
  }
);

function dd(variable, varName) {
  var varNameOutput;

  varName = varName || '';
  varNameOutput = varName ? varName + ':' : '';

  console.warn(varNameOutput, variable, ' (' + (typeof variable) + ')');
}