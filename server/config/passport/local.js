'use strict';
/**
 * Module dependencies.
 */
var Usuario = require('../../src/models/usuario');
var Rol = require('../../src/models/rol');
var Company = require('../../src/models/company');
var AccountDto = require('../../src/models/account');

const LocalStrategy = require('passport-local').Strategy;
const userProvider = require('../../src/providers/user');

/**
 * Expose
 */

module.exports = new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password'
},
  function (email, password, done) {
    var usr = email;
    var pwd = password;
    userProvider.loginCredentials(usr, pwd)
      .then(user => {
        if (user) {
          console.log("El usuario existe");
          // Obtener los roles 
          user.roles = [];
          var usuario = new Usuario();
          usuario.id = user.id;
          usuario.email = user.get("email");
          usuario.username = user.get("username");
          usuario.company = new Company(user.get("company").id); 
          usuario.cuenta = new AccountDto();
          var query = user.relation("roles").query();
          query.find({
            success: function (result) {
              var roles = [];
              for (var i = 0; i < result.length; i++) {
                roles.push(result[i].attributes.name)
                usuario.roles.push(new Rol(result[i].id, result[i].get("name")));
              }
              done(null, { usuario });
            },
            error: function (err) {
              done(err, null);
            }

          })
        } else {
          console.log("El usuario NO existe o está inhabilitado");
          done(null, null);
        }
      }).catch(error => {
        done(error);
      })
  }
);

function dd(variable, varName) {
  var varNameOutput;

  varName = varName || '';
  varNameOutput = varName ? varName + ':' : '';

  console.warn(varNameOutput, variable, ' (' + (typeof variable) + ')');
}
