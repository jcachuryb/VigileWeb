import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

import { Router } from '@angular/router';

@Injectable()
export class BaAppUtils {

    constructor(private _router: Router) {

    }

    extractData(res: any) {
        var body;
        if (res.length == undefined) {
            body = res.json();
        } else {
            body = [];
            for (var i = 0; i < res.length; i++) {
                body.push(res[i].json());
            }
        }
        return body || {};
    }
    handleErrorObservable(error: Response | any) {
        try {
            if (error.status === 500) {
                this._router.navigate(['/']);
            }
            console.error(error.statusText || error);
            return Observable.throw(error.statusText || error);
        } catch (error) {

        }
    }


}
