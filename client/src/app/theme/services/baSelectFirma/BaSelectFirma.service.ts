import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

import { Firma } from "./Firma";
@Injectable()
export class BaSelectFirma {


  private subject;
  private keepAfterRouteChange = false;

  constructor() {
    this.subject = new Subject<any>();
  }

  getAlert(): Observable<any> {
    return this.subject.asObservable();
  }

  elegirFirma(firma: Firma){
    // this.subject.next(<Alert>{ type: t, message: message });
  }

  cargarFirmas(firmas: any[], tieneInvitaciones: boolean){
    this.subject.next(<any>{ firmas: firmas, invitaciones: tieneInvitaciones });
  }

}
