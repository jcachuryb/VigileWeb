import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import { UserProvider, AccountProvider, ClientProvider, UserCompanyProvider } from "../../../providers";
import { GlobalState } from '../../../global.state';
import { EventosFirmas, EventosUsuario, EventosCuenta } from "../../../model/CambioEventos";

@Injectable()
export class BaAppData {



    private keepAfterRouteChange = false;

    constructor(private _state: GlobalState,
        private clientProvider: ClientProvider,
        private userCompanyProvider: UserCompanyProvider,
        private userProvider: UserProvider,
        private accountProvider: AccountProvider) {

    }


    loadData(): Promise<boolean> {
        return new Promise((resolve, reject) => {
            return this.getUserData().then(ok => {
                resolve(true);
            }).catch(error => {
                console.log(error);
                reject(false);
            })
        });
    }

    updateFirmasInfo() {
        return Observable.combineLatest(this.clientProvider.loadSelectedCompany(), this.clientProvider.loadUserCompany());
    }

    updateAccountInfo() {
        return Observable.combineLatest(this.accountProvider.isSelCompanyAccountActive());
    }

    private getUserData() {
        return new Promise((resolve, reject) => {
            this.userProvider.loadUserData().subscribe(value => {
                this.updateFirmasInfo().subscribe(values => {
                    this._state.notifyDataChanged(EventosFirmas.INFO_CAMBIO_USUARIO_FIRMA, values[0].userCompany, true);
                    this._state.notifyDataChanged(EventosFirmas.CAMBIO_INFO, values[1]);
                    this.updateAccountInfo().subscribe(value=>{
                        this._state.notifyDataChanged(EventosCuenta.CAMBIO_ACTUAL_ACTIVA, value[0]);
                        resolve(true);
                    })
                })
            }, error => {
                reject(false);
            });
        });
    }

}