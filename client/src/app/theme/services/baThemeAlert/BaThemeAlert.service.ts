import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

import { Alert } from "../../components/baAlerts/Alert";

@Injectable()
export class BaThemeAlert {


  private subject;
  private keepAfterRouteChange = false;

  constructor() {
    this.subject = new Subject<Alert>();
  }

  getAlert(): Observable<any> {
    return this.subject.asObservable();
  }

  success(message: string, keepAfterRouteChange = false) {
    this.alert("success", message, keepAfterRouteChange);
  }

  error(message: string, keepAfterRouteChange = false) {
    this.alert("error", message, keepAfterRouteChange);
  }

  info(message: string, keepAfterRouteChange = false) {
    this.alert("info", message, keepAfterRouteChange);
  }

  warn(message: string, keepAfterRouteChange = false) {
    this.alert("warning", message, keepAfterRouteChange);
  }

  private alert(t, message: string, keepAfterRouteChange = false) {
    this.keepAfterRouteChange = keepAfterRouteChange;
    this.subject.next(<Alert>{ type: t, message: message });
  }
}
