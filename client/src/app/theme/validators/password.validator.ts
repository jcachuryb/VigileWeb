import {AbstractControl} from '@angular/forms';

export class PasswordValidator {

  public static validate(c:AbstractControl) {
    let PWD_REGEXP = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).{6,20}$/;
    return PWD_REGEXP.test(c.value) ? null : {
      validateEmail: {
        valid: false
      }
    };
  }
}
