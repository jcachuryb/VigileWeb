import {AbstractControl} from '@angular/forms';

export class PhoneValidator {

  public static validate(c:AbstractControl) {
    let PHONE_REGEXP = /^\s*(?:\+?\d{1,3})?[- (]*\d{3}(?:[- )]*\d{3})?[- ]*\d{4}(?: *[x/#]\d+)?\s*$/;
    return PHONE_REGEXP.test(c.value) || !c.value ? null : {
      validateEmail: {
        valid: false
      }
    };
  }
}
