import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

import { Overlay } from 'angular2-modal';
import { Modal } from 'angular2-modal/plugins/bootstrap';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { BaThemeConfigProvider, colorHelper, BaThemeAlert } from '../../';
import { ClientProvider, UserCompanyProvider } from '../../../providers';
import { BaAppData, BaThemeSpinner } from '../../../theme/services';


import * as _ from 'lodash';
import * as Chart from 'chart.js';
import * as moment from 'moment';
moment.locale('es');


@Component({
  selector: 'select-firma',
  templateUrl: './selectFirma.html',
  styleUrls: ['./selectFirma.scss']
})
export class SelectFirma {


  private selected = { nombre: "Avocados y Aguacates" }
  private firmas = [];

  constructor(public router: Router, private _spinner: BaThemeSpinner, public clientProvider: ClientProvider, public userCompanyProvider: UserCompanyProvider,
    private _baConfig: BaThemeConfigProvider, private _alert: BaThemeAlert, private _appData: BaAppData) {
    // this._appData.getFirmasNotifier().subscribe(val => {
    //   this.firmas = val.firmas;
    //   this.selected = this.firmas[0];
    // }, error => {
    //   this._alert.error("Error cargando los datos de las firmas")
    // })
  }


  selectFirma(firma) {

    this._spinner.show();
    this.userCompanyProvider.selectUserCompany(firma.company.id).subscribe(value => {
      this._spinner.hide();
      if (value.status == "fail") {
        this._alert.error(value.msg);
      } else {
        this.selected = firma;
        
      }
    }, error => {
      this._spinner.hide();
      this._alert.error("Ocurrió un error ")
    });
    // Actualizar la selección en el back
    // Actualizar menú

    // Ocultar al Loader
  }

}