import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, Params, NavigationEnd } from '@angular/router';
import { Location } from '@angular/common';
import { Subscription } from 'rxjs/Rx';

import { AccountProvider } from '../../../providers';

import { GlobalState } from '../../../global.state';

import { EventosCuenta } from "../../../model/CambioEventos";

import * as _ from 'lodash';
import * as moment from 'moment';
moment.locale('es');


@Component({
    selector: 'bloque-suscripcion',
    templateUrl: './bloque-suscripcion.html',
    styleUrls: ['./bloque-suscripcion.scss']
})
export class BloqueSuscripcionComponent {

    protected _onRouteChange: Subscription;

    private cuentaActiva = true;
    private mensaje = "";

    constructor(public _router: Router, private route: ActivatedRoute,
        private _location: Location, private _state: GlobalState,
        private accountProvider: AccountProvider) {
        this.cuentaActiva = this.accountProvider.getSelectedAccountActiva();
        this._state.subscribe(EventosCuenta.CAMBIO_ACTUAL_ACTIVA, val => {
            this.cuentaActiva = val;
        });
    }

    ngOnInit() {
        this.mensaje = this._router.url;
        // console.log(this._router.url);
        this._router.events.subscribe((event) => {

            if (event instanceof NavigationEnd) {
                let url = event.url;
                let array = url.split("/");
                if (url.length) {
                    let pagina = array[array.length - 1];
                    this.updateMensaje(pagina)
                }
            }

        });
    }

    updateMensaje(pagina) {
        switch (pagina) {
            case 'agregar':
                this.mensaje = 'Sin una suscripción activa no podrá automatizar las consultas de sus procesos.';
                break;
            case 'lista':
                this.mensaje = 'Sin una suscripción activa no podremos hacerle seguimiento a sus procesos.';

                break;
            case 'gestionar-usuarios':
                this.mensaje = 'Sin una suscripción activa no podrá gestionar a sus usuarios.';

                break;

            default:
                this.mensaje = "";
                break;
        }
    }



}