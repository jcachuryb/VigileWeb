import { Injectable, Component, ViewContainerRef } from '@angular/core';
import { Router, Routes } from '@angular/router';
import * as _ from 'lodash';

import { NotificationsService } from "angular2-notifications";
import { BehaviorSubject } from 'rxjs/BehaviorSubject';


import { BaThemeAlert } from "../../services/baThemeAlert";
import { Alert } from "./Alert";
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
    selector: 'alert-service',
    template: '',
})
export class BaAlertComponent {

    private types = ['success', 'error', 'info', 'warning']

    private options = {
        overlay: true,
        overlayClickToClose: false,
        showCloseButton: true,
        duration: 10000
    }

    constructor(private _router: Router,
        private _notifications: NotificationsService, private _as: BaThemeAlert,
        public toastr: ToastsManager, vcr: ViewContainerRef) {
            
        let a = this._as.getAlert();
        this._as.getAlert().subscribe((_alert: Alert) => {
            switch (_alert.type) {
                case 'success':
                    this.showSuccess(_alert.message);
                    break;
                case 'error':
                    this.showError(_alert.message);
                    break;
                case 'warning':
                    this.showWarning(_alert.message);
                    break;
                case 'info':
                    this.showInfo(_alert.message);
                    break;
                default:
                    alert(_alert.message);
                    break;
            }
        });
    }

    setViewContainerRef(vcr: ViewContainerRef) {
        this.toastr.setRootViewContainerRef(vcr);
    }

    showSuccess(message) {
        this.toastr.success(message);
    }

    showError(message) {
        this.toastr.error(message, 'Ocurrió un error');
    }

    showWarning(message) {
        this.toastr.warning(message, '¡Atención!');
    }

    showInfo(message) {
        this.toastr.info(message);
    }

    showCustom(message) {
        this.toastr.custom('<span style="color: red">Message in red.</span>', null, { enableHTML: true });
    }

}
