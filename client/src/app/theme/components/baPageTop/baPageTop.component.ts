import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { GlobalState } from '../../../global.state';
import { BaThemeSpinner } from '../../services';
import { AuthenticationProvider, ClientProvider } from '../../../providers';

import { Firma } from "../../../model/Dtos";
import { EventosFirmas, EventosUsuario, EventosCuenta } from "../../../model/CambioEventos";


@Component({
  selector: 'ba-page-top',
  templateUrl: './baPageTop.html',
  styleUrls: ['./baPageTop.scss']
})
export class BaPageTop {

  public isScrolled: boolean = false;
  public isMenuCollapsed: boolean = false;
  private cuentaActiva: boolean = true;

  private nombreFirma: string = "";

  constructor(private _state: GlobalState, private router: Router, 
    private clientProvider: ClientProvider,
    private route: ActivatedRoute, private _spinner: BaThemeSpinner) {
    this._state.subscribe('menu.isCollapsed', (isCollapsed) => {
      this.isMenuCollapsed = isCollapsed;
    });

    this._state.subscribe(EventosFirmas.INFO_CAMBIO_USUARIO_FIRMA, (infoFirma: Firma) => {
      this.nombreFirma = infoFirma.nombreFirma;
    });

    this._state.subscribe(EventosCuenta.CAMBIO_ACTUAL_ACTIVA, val => {
      this.cuentaActiva = val;
    });
  }

  public ngAfterViewInit(): void {
    let currentCompany = this.clientProvider.getCurrentUserCompany();
    if (!this.nombreFirma && currentCompany) {
      this.nombreFirma = currentCompany.nombreFirma;
    }
  }

  public toggleMenu() {
    this.isMenuCollapsed = !this.isMenuCollapsed;
    this._state.notifyDataChanged('menu.isCollapsed', this.isMenuCollapsed);
    return false;
  }

  public scrolledChanged(isScrolled) {
    this.isScrolled = isScrolled;
  }

  public cerrarSesion() {
    this._state._onEvent({event: EventosUsuario.LOGOUT, data: { accion: "salir"}});
  }

  public verPerfil() {
    return this.router.navigate(['/pages/perfil/miperfil']);
  }

  public verFirmas() {
    return this.router.navigate(['/pages/firmas']);
  }
}
