import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { GlobalState } from '../../../global.state';
import { BaThemeSpinner } from '../../services';
import { FeebackProvider } from '../../../providers/user-provider/feedback-provider';

@Component({
  selector: 'ba-feedback',
  templateUrl: './feedback.html',
  styleUrls: ['./feedback.scss']
})
export class BaFeedback {

  public form: FormGroup;
  public message: AbstractControl;
  public type: AbstractControl;

  public isShowing: boolean;
  public data = { url: "", time: null };
  public email;
  public sent: boolean = false;

  public types = [
    { name: "error", value: "Error" },
    { name: "sugerencia", value: "Sugerencia" },
    { name: "duda", value: "Duda" },
  ];


  constructor(fb: FormBuilder, private feebackProvider: FeebackProvider, private router: Router,
    private route: ActivatedRoute, private _spinner: BaThemeSpinner) {
    this.form = fb.group({
      'message': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
      'type': ['', Validators.compose([Validators.required, Validators.minLength(2)])]
    });
    this.message = this.form.controls['message'];
    this.type = this.form.controls['type'];
  }

  public postMessage() {
    this.data.url = this.router.url;
    this.feebackProvider.postFeedback(this.type.value, this.message.value, this.data)
      .subscribe(value => {
        this.sent = true;
        this.message.reset();
        this.type.setValue("");
      }, error => {
        console.log(error)
      });
  }

  toggleView() {
    this.isShowing = !this.isShowing;
    this.sent = false;
  }

  borrar() {

  }

}
