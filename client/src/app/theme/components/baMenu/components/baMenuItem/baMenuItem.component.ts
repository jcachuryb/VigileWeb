import {Component, Input, Output, EventEmitter} from '@angular/core';
import { GlobalState } from '../../../../../global.state';
@Component({
  selector: 'ba-menu-item',
  templateUrl: './baMenuItem.html',
  styleUrls: ['./baMenuItem.scss']
})
export class BaMenuItem {
  constructor(private _state: GlobalState){

  }

  @Input() menuItem:any;
  @Input() child:boolean = false;

  @Output() itemHover = new EventEmitter<any>();
  @Output() toggleSubMenu = new EventEmitter<any>();

   public isMenuCollapsed: boolean = false;
   public clickAppeared: boolean = false;

  public onHoverItem($event):void {
    this.itemHover.emit($event);
  }

  public onToggleSubMenu($event, item):boolean {
    $event.item = item;
    this.toggleSubMenu.emit($event);
    return false;
  }

  public clickItem($event){
    this.clickAppeared = !this.clickAppeared;
    this._state.notifyDataChanged('menu.clickappeared', this.clickAppeared);
  }
}
