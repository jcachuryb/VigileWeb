import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationProvider } from '../../../../providers/auth-provider/auth-provider';
import { BaThemeSpinner } from '../../../../theme/services';
import { BaThemeAlert } from '../../../../theme';
import { EventosFirmas, EventosUsuario } from '../../../../model/CambioEventos';
import { EmailValidator, EqualPasswordsValidator } from '../../../../theme/validators';

import { GlobalState } from '../../../../global.state';

@Component({
  selector: 'solicitud-clave',
  templateUrl: './solicitud.html',
  styleUrls: ['./solicitud.scss']
})
export class SolicitudClaveComponente {

  public busy: Boolean;
  public form: FormGroup;
  public email: AbstractControl;
  public captcha: AbstractControl;
  public captchaResp: String;
  public submitted: boolean = false;
  public solicitudEnviada: boolean = false;

  constructor(fb: FormBuilder, public authProvider: AuthenticationProvider, private router: Router,
    private route: ActivatedRoute, private _spinner: BaThemeSpinner, private _alert: BaThemeAlert,
    private _state: GlobalState) {
    this.form = fb.group({
      'email': ['', Validators.compose([Validators.required, EmailValidator.validate])],
      'recaptcha': ['', Validators.compose([Validators.required])]
    });

    this.email = this.form.controls['email'];
    this.captcha = this.form.controls['recaptcha'];
    this.busy = false;
  }
  ngOnInit() {
    this._spinner.hide();
    this.route.params.subscribe(params => {
      if (params.email) {
        this.email.setValue(params.email);
      }

    });
    jQuery('#email').focus();
  }

  resolved(captchaResponse: string) {
    this.captchaResp = captchaResponse;
  }

  public onSubmit(values: Object): void {
    this.submitted = true;
    if (this.form.valid) {
      this.busy = true;

      this.authProvider.solicitudRecuperarClave(this.email.value, this.captchaResp).subscribe(value => {
        this.busy = false;
        this.captcha.reset();
        if (value.status === "success") {
          this.solicitudEnviada = true;
          return true;
        } else {
          this._alert.error(value.msg);
        }

      },
        error => {
          this._spinner.hide();
          this._alert.error("Nombre de usuario o contraseña inválidos.")
          console.log(error);
        }
      );
    }
  }
}
