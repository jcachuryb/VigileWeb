import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationProvider } from '../../../../providers/auth-provider/auth-provider';
import { BaThemeSpinner } from '../../../../theme/services';
import { BaThemeAlert } from '../../../../theme';
import { PasswordValidator, EqualPasswordsValidator } from '../../../../theme/validators';
import { EventosFirmas, EventosUsuario } from '../../../../model/CambioEventos';

import { GlobalState } from '../../../../global.state';

@Component({
  selector: 'confirmacion-solicitud-clave',
  templateUrl: './confirmacion-solicitud.html',
  styleUrls: ['./confirmacion-solicitud.scss']
})
export class ConfirmacionComponente {
  public token: String;
  public userId: String;

  public busy: Boolean;
  public form: FormGroup;
  public clave: AbstractControl;
  public repetir: AbstractControl;
  public submitted: boolean = false;

  public claveCambiada: boolean = false;

  pwdPattern = "^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).{6,20}$";
  

  constructor(fb: FormBuilder, public authProvider: AuthenticationProvider, private router: Router,
    private route: ActivatedRoute, private _spinner: BaThemeSpinner, private _alert: BaThemeAlert,
    private _state: GlobalState) {
    this.form = fb.group({
      'clave': ['', Validators.compose([Validators.required, PasswordValidator.validate])],
      'repetir': ['', Validators.compose([Validators.required])]
    }, { validator: EqualPasswordsValidator.validate('clave', 'repetir') });

    this.clave = this.form.controls['clave'];
    this.repetir = this.form.controls['repetir'];
    this.busy = false;
  }
  ngOnInit() {
    this._spinner.hide();
    this.route.params.subscribe(params => {
      if (params.token && params.userId) {
        this.token = params.token;
        this.userId = params.userId;
      }else{
        this._alert.error("You shouldn't be here");
      }
    });
    jQuery('#clave').focus();
  }

  passwordMatch(control: AbstractControl){
    let paswd = control.root.get('clave');
    if(paswd && control.value != paswd.value){
     return {
         passwordMatch: false
     };   
    }
    return null;
}


  public onSubmit(values: Object): void {
    if (this.form.valid) {
      this.submitted = true;
      this.busy = true;
      this._spinner.show();
      this.authProvider.recuperarClaveConfirmacion(this.userId, this.token, this.clave.value)
        .subscribe(value => {
          this.busy = false;
          this._spinner.hide();
          this.claveCambiada = value.status === "success";
          if (value.status === "success") {
            this._alert.success("Usted ha cambiado la clave con éxito")
            return true;
          } else {
            this._alert.error(value.msg);
          }

        },
        error => {
          this._spinner.hide();
          this._alert.error("Nombre de usuario o contraseña inválidos.")
          console.log(error);
        }
        );
    }
  }
}
