import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { AppTranslationModule } from '../../app.translation.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';

import { RecuperarClaveComponent } from './recuperar-clave.component';
import { routing }       from './recuperar-clave.routing';
import { SolicitudClaveComponente, ConfirmacionComponente } from './components';


@NgModule({
  imports: [
    CommonModule,
    AppTranslationModule,
    ReactiveFormsModule,
    FormsModule,
    NgaModule,
    routing
  ],
  declarations: [
    RecuperarClaveComponent,
    SolicitudClaveComponente,
    ConfirmacionComponente
  ]
})
export class RecuperarClaveModule {}
