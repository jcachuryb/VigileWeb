import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationProvider } from '../../providers/auth-provider/auth-provider';
import { BaThemeSpinner } from '../../theme/services';
import { BaThemeAlert } from '../../theme';
import { EventosFirmas, EventosUsuario } from '../../model/CambioEventos';

import { GlobalState } from '../../global.state';

@Component({
  selector: 'recuperar-clave',
  template: '<router-outlet></router-outlet>'
})
export class RecuperarClaveComponent {

  public busy: Boolean;
  public form: FormGroup;
  public email: AbstractControl;
  public password: AbstractControl;
  public submitted: boolean = false;

  constructor(fb: FormBuilder, public authProvider: AuthenticationProvider, private router: Router,
    private route: ActivatedRoute, private _spinner: BaThemeSpinner, private _alert: BaThemeAlert,
    private _state: GlobalState) {
    this.form = fb.group({
      'email': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
      'password': ['', Validators.compose([Validators.required, Validators.minLength(4)])]
    });

    this.email = this.form.controls['email'];
    this.password = this.form.controls['password'];
    this.busy = false;
  }
  ngOnInit() {
    this._spinner.hide();
    this.route.params.subscribe(params => {
      if (params.email) {
        this.email.setValue(params.email);
      }
      
    });
    jQuery('#email').focus();
  }

  public onSubmit(values: Object): void {
    this.submitted = true;
    if (this.form.valid) {
      this.busy = true;
      this._spinner.show();
      this.authProvider.loginCredentials(this.email.value, this.password.value)
        .subscribe(value => {
          this.password.reset();
          this.busy = false;
          if (value.status === "success") {
            this._state._onEvent({event: EventosUsuario.LOGIN, data: {}})
            return true;
          } else {
            this._alert.error(value.msg);
          }

        },
        error => {
          this._spinner.hide();
          this._alert.error("Nombre de usuario o contraseña inválidos.")
          console.log(error);
        }
        );
    }
  }
}
