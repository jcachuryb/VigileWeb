import { Routes, RouterModule }  from '@angular/router';

import { RecuperarClaveComponent } from './recuperar-clave.component';
import { ModuleWithProviders } from '@angular/core';
import { SolicitudClaveComponente, ConfirmacionComponente } from './components';

// noinspection TypeScriptValidateTypes
export const routes: Routes = [
  {
    path: '',
    component: RecuperarClaveComponent,
    children: [
      { path: '', redirectTo: 'solicitud', pathMatch: 'full' },
      {path: 'solicitud', component: SolicitudClaveComponente},
      {path: 'confirmacion/:userId/:token', component: ConfirmacionComponente}

    ]
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
