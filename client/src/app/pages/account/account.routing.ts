import { Routes, RouterModule } from '@angular/router';

import { MiCuenta } from './components/micuenta/';
import { UsuariosComponent } from './components/usuarios/';
import { AccountComponent } from './account.component';

// noinspection TypeScriptValidateTypes
const routes: Routes = [
  {
    path: '',
    component: AccountComponent,
    children: [
      { path: '', redirectTo: 'micuenta', pathMatch: 'full' },
      {path: 'micuenta', component: MiCuenta},
      {path: 'gestionar-usuarios', component: UsuariosComponent}

    ]
  }
];

export const routing = RouterModule.forChild(routes);
