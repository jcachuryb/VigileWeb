import { Component, Output, EventEmitter, ViewChild } from '@angular/core';
import { ProcessProvider, ClientProvider, CiudadEntidadService, UserProvider } from '../../../../providers';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { BaThemeConfigProvider, colorHelper, BaThemeAlert } from '../../../../theme';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

import { Observable } from 'rxjs';
import { BaAppData, BaThemeSpinner } from '../../../../theme/services';

import * as _ from 'lodash';
import * as Chart from 'chart.js';
import * as moment from 'moment';
import { GlobalState } from '../../../../global.state';
import { EventosAplicacion, EventosFirmas, EventosUsuario } from "../../../../model/CambioEventos";
moment.locale('es');


@Component({
  selector: 'primera-configuracion',
  templateUrl: './primeraConfiguracion.html',
  styleUrls: ['./primeraConfiguracion.scss']
})
export class PrimeraConfiguracion {

  public formGeneral: FormGroup;
  public nombre: AbstractControl;
  public documento: AbstractControl;
  public telefono: AbstractControl;
  public ciudad: AbstractControl;

  busy: boolean = false;

  constructor(private _state: GlobalState, public processProvider: ProcessProvider,
    public router: Router, private route: ActivatedRoute,
    private _appData: BaAppData,  private _spinner: BaThemeSpinner,
    public formBuilder: FormBuilder, public clientProvider: ClientProvider,
    public userProvider: UserProvider, private _baConfig: BaThemeConfigProvider
    , private _alert: BaThemeAlert) {
    this.loadForm();

  }
  ngOnInit(){
    
    this._spinner.hide();
  }

  loadForm() {
    this.formGeneral = this.formBuilder.group({
      'nombre': ['', Validators.compose([Validators.required, Validators.minLength(2)])],
      'documento': ['', Validators.compose([Validators.minLength(5)])],
      'telefono': ['', Validators.compose([Validators.minLength(7)])],
      'ciudad': ['', Validators.compose([Validators.minLength(3)])]
    });
    this.nombre = this.formGeneral.controls['nombre'];
    this.documento = this.formGeneral.controls['documento'];
    this.telefono = this.formGeneral.controls['telefono'];
    this.ciudad = this.formGeneral.controls['ciudad'];
    // ---------------------------------------------
  }

  guardar() {
    var company = {
      nombre: this.nombre.value,
      documento: this.documento.value,
      telefono: this.telefono.value,
      ciudad: this.ciudad.value
    }
    this.actualizarFirma(company);
  }

  omitir() {
    var company = {
      nombre: "Sin nombre",
      documento: "",
      telefono: "",
      ciudad: ""
    }
    this.actualizarFirma(company);

  }

  actualizarFirma(firma) {
    this.clientProvider.updateCompany(firma).subscribe(value => {
      this.clientProvider.loadUserCompany().subscribe(done=>{
        this._state.notifyDataChanged(EventosFirmas.FIRMA_CONFIGURADA, done, true);
      })
    }, error => {
      this.busy = false;
      this._alert.error("No se pudieron actualizar los datos. Intente más tarde");
    });
  }

  cerrarSesion() {
    this._state.notifyDataChanged(EventosUsuario.LOGOUT, true);
  }


}