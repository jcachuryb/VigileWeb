import { Component } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { ProcessProvider, ClientProvider, UserCompanyProvider, CiudadEntidadService, AccountProvider } from '../../../../providers';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

import { GlobalState } from '../../../../global.state';
import { EventosFirmas, EventosUsuario } from "../../../../model/CambioEventos";
import { Overlay } from 'angular2-modal';
import { Modal } from 'angular2-modal/plugins/bootstrap';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BaThemeAlert } from '../../../../theme';

import * as _ from 'lodash';

import * as moment from 'moment';
moment.locale('es');

@Component({
  selector: 'users-management',
  templateUrl: './usuarios.html',
  styleUrls: ['./usuarios.scss']
})
export class UsuariosComponent {

  datosCuenta : any = {};
  accountDataReady = false;


  public users: any[] = [];
  public adding = false;
  public isBusy = false;
  public user: any;
  public mostrarUsuario = false;
  public form: FormGroup;
  public email: AbstractControl;
  private userName;
  private permisos = [
    { value: "editar", label: "Modificar procesos (Agregar, editar)", selected: false },
    { value: "asignar", label: "Asignar procesos a usuarios", selected: false },
  ]

  private listaPermisos = [];
  private listaPermisosNuevoUsuario = [];

  private esApoderado = false;

  private currentUserId = "";

  constructor(private _state: GlobalState, 
    public fb: FormBuilder, 
    public userCompanyProvider: UserCompanyProvider, public clientProvider: ClientProvider, public accountProvider: AccountProvider,
    public router: Router, private route: ActivatedRoute,
    private _location: Location, private _alert: BaThemeAlert) {

    this.loadCompanyUsers();
    this.form = fb.group({
      'email': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
    });

    this.email = this.form.controls['email'];

  }

  ngOnInit() {
    this._state.subscribe(EventosFirmas.INFO_CAMBIO_USUARIO_FIRMA, usuario => {
      this.currentUserId = usuario.id;
    });
    let currentUser = this.clientProvider.getCurrentUserCompany();
    if (currentUser) {
      this.currentUserId = currentUser.id;
    }

    this.cargarInfoCuenta();
  }

  getBasePermisos() {
    return [
      { title: "Consulta y revisión", label: "Revisar la lista de procesos añadidos.", id: "consultar", disabled: true, checked: true },
      { title: "Modificación de procesos", label: "Agregar, modificar, eliminar y archivar procesos.", id: "editar", disabled: false, checked: false },
      { title: "Asignación", label: "Determinar los apoderados y dependientes de los procesos. ", id: "asignar", disabled: false, checked: false },
      { title: "Gestión de usuarios", label: "Gestionar los usuarios asociados a la firma.", id: "gestionar", disabled: false, checked: false }];
  }

  displayAddUserFormat(value) {
    this.adding = value;
    this.email.setValue("");
    this.listaPermisosNuevoUsuario = this.getBasePermisos();
  }  
  
  cargarInfoCuenta() {
    this.accountProvider.loadSelCompanyAccountInfo().subscribe(value => {
      if (value) {
        this.datosCuenta = value.cuenta;
        if (this.datosCuenta.active) {
          
        }
        this.accountDataReady = true;
      } else {
        this._alert.error("Ocurrió un error al traer los datos");
        return false;
      }
    })
  }

  public onSubmit(values: Object): void {
    if (this.form.valid) {
      let email: string = this.email.value;
      email = email.toLowerCase();
      let pp = _.filter(this.listaPermisosNuevoUsuario, function (o: any) { return o.selected || o.checked; });
      let data = { permisos: _.map(pp, 'id') };
      this.userCompanyProvider.addUsertoCompany(email, data).
        subscribe(value => {
          if (value.status === "success") {
            this.users.push(value.data);
            this.displayAddUserFormat(false);
            this._alert.success("La invitación fue enviada al usuario");
          }
          if (value.status === "fail") {
            this._alert.error(value.msg);
          }
        });
    }
  }


  loadCompanyUsers() {
    this.userCompanyProvider.loadCompanyUsers().subscribe(value => {
      this.users = value;
    })
  }

  eliminarUsuario(usuario) {
    let val = confirm("¿Desea eliminar a " + usuario.nombre + " de la firma?")
    this.userCompanyProvider.removeUserFromCompany(usuario.usuarioId).subscribe(value => {
      if (value.status === "success") {
        _.remove(this.users, u=>{
          return u.id === usuario.id;
        })
        this._alert.info("Usuario eliminado de la firma");
      }
    })
  }
  cancelarInvitacion(usuario) {
    this.userCompanyProvider.cancelUserInvitation(usuario.id).subscribe(value => {
      _.remove(this.users, u=>{
        return u.id === usuario.id;
      })
      this._alert.info("Invitación cancelada");
    })
  }

  desactivarUsuario() {

  }

  selectPermisos($event, p) {
    p.selected = $event.target.checked;
    p.checked = $event.target.checked;
  }

  userSelected(seleccionado) {
    if (this.isBusy) {
      return;
    }
    this.user = {};
    this.listaPermisos = this.getBasePermisos();

    this.user.datos = seleccionado;
    this.user.permisos = (JSON.parse(JSON.stringify(this.listaPermisos)));
    _.forEach(this.user.permisos, function (value) {
      value.checked = _.includes(seleccionado.permisos, value.id);
    });
    this.mostrarUsuario = true;
  }

  cancelUserSelected() {
    this.user = null;
    this.mostrarUsuario = false;
  }

  updateUser(user) {
    this.isBusy = true;
    let userId = user.datos.usuarioId;
    let pp = _.filter(user.permisos, function (o: any) { return o.selected || o.checked; });
    let data = { permisos: _.map(pp, 'id') };
    this.userCompanyProvider.updateUserCompany(userId, data).subscribe(result => {
      if (result.status === "fail") {
        this._alert.error(result.msg);
      }
      if (result.status === "success") {
        // Mostrar los cambios
        this.aplicarCambios(result.data);
        this.cancelUserSelected();
        this._alert.success("Datos actualizados con éxito");
      }
      this.isBusy = false;
    },
      error => {
        this._alert.error("Ocurrió un error tratando de actualizar los datos");
        this.isBusy = false;
      });
  }

  private aplicarCambios(newData) {
    _.forEach(this.users, function (u) {
      if (newData.id === u.id) {
        u.permisos = newData.permisos;
      }
    });
  }

}