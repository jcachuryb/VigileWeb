import { Component, Input } from '@angular/core';

import { TrafficChartService } from './trafficChart.service';
import * as Chart from 'chart.js';

@Component({
  selector: 'processes-chart',
  templateUrl: './trafficChart.html',
  styleUrls: ['./trafficChart.scss']
})

// TODO: move chart.js to it's own component
export class TrafficChart {

  @Input() total;
  @Input() data: Array<Object>;
  private _init = false;

  public doughnutData: Array<Object>;

  constructor(private trafficChartService: TrafficChartService) {
    this.doughnutData = this.data;
    //this.doughnutData = trafficChartService.getData();
  }

  ngAfterViewInit() {
    if (!this._init) {
      this._loadDoughnutCharts();
      this._init = true;
    }
  }

  private _loadDoughnutCharts() {
    let el = jQuery('.chart-area').get(0) as HTMLCanvasElement;
    new Chart(el.getContext('2d')).Doughnut(this.doughnutData, {
      segmentShowStroke: false,
      percentageInnerCutout: 64,
      responsive: true
    });
  }
}
