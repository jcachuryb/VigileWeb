import { Component } from '@angular/core';
import { UserProvider, ProcessProvider, ClientProvider, CiudadEntidadService } from '../../../../providers';

import { GlobalState } from '../../../../global.state';
import { EventosFirmas, EventosUsuario } from "../../../../model/CambioEventos";
import { BaThemeConfigProvider, colorHelper } from '../../../../theme';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';


import { Overlay } from 'angular2-modal';
import { Modal } from 'angular2-modal/plugins/bootstrap';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BaThemeAlert } from '../../../../theme';

import * as _ from 'lodash';
import * as Chart from 'chart.js';
import * as moment from 'moment';
moment.locale('es');


@Component({
  selector: 'micuenta',
  templateUrl: './micuenta.html',
  styleUrls: ['./micuenta.scss', './trafficChart.scss', './pieChart.scss']
})
export class MiCuenta {
  company = null;
  cuenta = null;
  stats = null;
  pieChartsInfo = [];
  pChart = [];
  progress = null;
  ready = false;
  format1 = "LL";
  format2 = "";

  constructor(private _state: GlobalState, public processProvider: ProcessProvider,
    public router: Router, private route: ActivatedRoute,
    private _location: Location, public clientProvider: ClientProvider,
    public userProvider: UserProvider, private _baConfig: BaThemeConfigProvider,
    private _alert: BaThemeAlert) {

  }

  ngOnInit() {
    let usuario = this.userProvider.getCurrentUser();
    if (!usuario) {
      this.userProvider.loadUserData()
    } else {
      this.cargarDatosMiCuenta();
    }

  }

  cargarDatosMiCuenta() {
    this.clientProvider.loadCurrentUserCompanyAccountInfo().subscribe(value => {
      this.company = value[0].company;
      this.cuenta = this.company.cuenta;
      this.stats = value[1].stats;
      this.loadProcessChartInfo();
      this.loadProgressInfo();
      this.company.createdAt = new Date(this.company.createdAt).toDateString();
      this.ready = true;
    },
      error => {
        this._alert.error("Ocurrió un error con los datos.");
        this.router.navigate(['/', null]);
      }
    );
  }


  loadProgressInfo() {
    this.progress = {};
    let init = this.getParsedDate(this.cuenta.activation.iso);
    let end = this.getParsedDate(this.cuenta.ends.iso);
    let now = new Date();
    this.progress.init = this.formatDate(init.toISOString(), this.format1);
    this.progress.end = this.formatDate(end.toISOString(), this.format1);
    this.progress.total = this.getDaysDiff(init, end);
    this.progress.passed = this.getDaysDiff(now, init);
    this.progress.spare = this.progress.total - this.progress.passed;

    let p = this.progress.passed / this.progress.total * 100;
    this.progress.p = p + "%";
    this.progress.class = 'success';
    if (p > 70 && p < 85) {
      this.progress.class = 'warning';
    } else {
      if (p >= 85) {
        this.progress.class = 'danger';
      }
    }
    if (this.progress.spare > 0) {
      this.progress.msg = "Vence en " + this.progress.spare + " día";
      this.progress.msg += this.progress.spare === 1 ? '' : 's';
    } else if (this.progress.spare == 0) {
      this.progress.msg = "Hoy es el último día";
    } else {
      this.progress.msg = "Es momento de renovar la suscripción";
    }

  }

  getParsedDate(dateString: string) {
    return new Date(dateString);
  }

  getPrettyDate(date: Date) {
    let ar = date.toString().split(' ');
    return ar[2] + " " + ar[1] + " " + ar[3];
  }

  getDaysDiff(d1: Date, d2: Date) {
    try {
      var timeDiff = Math.abs(d2.getTime() - d1.getTime());
      return Math.ceil(timeDiff / (1000 * 3600 * 24));
    } catch (error) {
      console.log(error);
      return 0;
    }
  }

  loadProcessChartInfo() {
    let dashboardColors = this._baConfig.get().colors.dashboard;
    let colorScheme = this._baConfig.get().colors;
    this.pChart.push({
      value: this.stats.activos,
      color: colorScheme.primary,
      highlight: colorHelper.shade(colorScheme.primary, 15),
      label: 'Procesos Activos',
      percentage: this.stats.activos / this.cuenta.limit * 100,
      order: 1,
    });
    this.pChart.push({
      value: this.stats.error,
      color: colorScheme.danger,
      highlight: colorHelper.shade(colorScheme.danger, 15),
      label: 'Procesos con error',
      percentage: this.stats.error / this.cuenta.limit * 100,
      order: 1,
    });
    this.pChart.push({
      value: this.stats.nuevos,
      color: colorScheme.info,
      highlight: colorHelper.shade(colorScheme.info, 15),
      label: 'Procesos sin consultar',
      percentage: this.stats.nuevos / this.cuenta.limit * 100,
      order: 1,
    });
    let spare = this.cuenta.limit - this.stats.total;
    this.pChart.push({
      value: spare,
      color: colorScheme.default,
      highlight: colorHelper.shade(colorScheme.default, 15),
      label: 'Disponibilidad',
      percentage: spare / this.cuenta.limit * 100,
      order: 1,
    });
    this._loadDoughnutCharts();
  }

  _loadDoughnutCharts() {
    let el = jQuery('.chart-area').get(0) as HTMLCanvasElement;
    new Chart(el.getContext('2d')).Doughnut(this.pChart, {
      segmentShowStroke: false,
      percentageInnerCutout: 64,
      responsive: true
    });
  }

  formatDate(date: string, format: string) {
    return moment(date).format(format);
  }

}