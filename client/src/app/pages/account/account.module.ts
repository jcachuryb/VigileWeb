import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule as AngularFormsModule } from '@angular/forms';
import { AppTranslationModule } from '../../app.translation.module';
import { NgaModule } from '../../theme/nga.module';
import { NgbRatingModule } from '@ng-bootstrap/ng-bootstrap';
import { DataTableModule } from "angular2-datatable";
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { BsModalModule } from 'ng2-bs3-modal';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { routing } from './account.routing';

import { AccountComponent } from './account.component';

import { MiCuenta } from './components/micuenta/';
import { TrafficChart } from './components/micuenta/components/trafficChart';
import { UsuariosComponent } from './components/usuarios/';

import { TrafficChartService } from './components/micuenta/components/trafficChart/trafficChart.service';

@NgModule({
  imports: [
    CommonModule,
    AngularFormsModule,
    AppTranslationModule,
    NgaModule,
    NgbDropdownModule,
    NgbRatingModule,
    FormsModule, 
    ReactiveFormsModule,
    routing,
    DataTableModule
  ],
  declarations: [
    AccountComponent,
    MiCuenta,
    UsuariosComponent,
    TrafficChart
  ],
  providers: [
    TrafficChartService
  ]
})
export class AccountModule {
}
