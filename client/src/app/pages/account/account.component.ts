import {Component} from '@angular/core';

@Component({
  selector: 'cuenta',
  template: `<router-outlet></router-outlet>`
})
export class AccountComponent {

  constructor() {
  }
}
