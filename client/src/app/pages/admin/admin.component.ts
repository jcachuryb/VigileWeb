import { Component } from '@angular/core';
import { Routes, Router, ActivatedRoute, Params } from '@angular/router';

import { BaMenuService } from '../../theme';
import { GlobalState } from '../../global.state';
import { BaThemeSpinner } from '../../theme/services';
import { UserProvider } from '../../providers/user-provider/user-provider';
import { ClientProvider } from '../../providers/client-provider/client-provider';


import { EventosFirmas, EventosUsuario, EventosAplicacion } from '../../model/CambioEventos';
import { Firma } from '../../model/Dtos';

@Component({
  selector: 'admin',
  template: 
  '<router-outlet></router-outlet>'
})
export class MainAdminComponent {

  constructor(private _menuService: BaMenuService, private _spinner: BaThemeSpinner,
    private userProvider: UserProvider, private clientProvider: ClientProvider,
    private _state: GlobalState, private router: Router) {

  }

  ngOnInit() {

  }

  public ngAfterViewInit(): void {
    // this.updateMenu();

    this._spinner.hide();
  }

}
