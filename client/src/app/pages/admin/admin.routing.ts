import { Routes, RouterModule, ActivatedRouteSnapshot } from '@angular/router';

import { ModuleWithProviders } from '@angular/core';

import { AuthGuard, LoginGuard } from '../../_guards/';
import { MainAdminComponent } from './admin.component';
import { AdminCuentasComponent } from './components/cuentas';
import { AdminPlanesComponent } from './components/planes';
import { AdminVistaEjecucionesComponent } from './components/ejecuciones';

// noinspection TypeScriptValidateTypes

// export function loadChildren(path) { return System.import(path); };

const routes: Routes = [
  {
    path: '',
    component: MainAdminComponent,
    children: [
      {path: '', redirectTo: 'cuentas', pathMatch: 'full' },
      {path: 'cuentas', component: AdminCuentasComponent},
      {path: 'planes', component: AdminPlanesComponent},
      {path: 'ejecuciones', component: AdminVistaEjecucionesComponent},
    ]
  }
];

export const routing = RouterModule.forChild(routes);

