import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { routing } from './admin.routing';
import { NgaModule } from '../../theme/nga.module';
import { ModalModule } from 'angular2-modal';
import { BootstrapModalModule } from 'angular2-modal/plugins/bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminCuentasComponent } from './components/cuentas';
import { MainAdminComponent } from './admin.component';
import { AdminPlanesComponent } from './components/planes';
import { AdminVistaEjecucionesComponent } from './components/ejecuciones';


@NgModule({
  imports: [CommonModule,
    NgaModule,
    routing,
    FormsModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    BootstrapModalModule,
  ],


  declarations: [
    MainAdminComponent,
    AdminCuentasComponent,
    AdminPlanesComponent, 
    AdminVistaEjecucionesComponent
  ]
})
export class MainAdminModule {
}
