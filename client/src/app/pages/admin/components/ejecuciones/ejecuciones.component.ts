import { Component, HostListener } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { AdminProvider, PlanProvider, TaskProvider } from '../../../../providers';
import { BaThemeSpinner } from '../../../../theme/services';
import { BaThemeAlert } from '../../../../theme';
import { PasswordValidator, EqualPasswordsValidator } from '../../../../theme/validators';
import { EventosFirmas, EventosUsuario } from '../../../../model/CambioEventos';

import { GlobalState } from '../../../../global.state';
import { Plan } from '../../../../model/Dtos';

import * as _ from 'lodash';

@Component({
  selector: 'admin-ejecuciones',
  templateUrl: './ejecuciones.html',
  styleUrls: ['./ejecuciones.scss']
})
export class AdminVistaEjecucionesComponent {


  taskList = [];
  plan: Plan;
  onEdit: boolean;
  tipo: string;
  inicio = new Date();
  fin = new Date();

  page: number = 0;
  num: number = 10;
  moreResults: boolean = true;
  buscando: boolean = true;

  constructor(fb: FormBuilder, public adminProvider: AdminProvider, public taskProvider: TaskProvider,
    private router: Router, private route: ActivatedRoute, private _spinner: BaThemeSpinner, private _alert: BaThemeAlert,
    private _state: GlobalState) {
  }

  ngOnInit() {
    this.realizarBusqueda();
  }

  realizarBusqueda() {
    this.taskProvider.fetchTasks(this.tipo, this.inicio, this.fin, this.num, this.page).subscribe(data => {
      this.taskList = this.taskList.concat(data.list);
      if (data.list.length >= this.num) {
        this.moreResults = true;
        this.page++;
      } else {
        this.moreResults = false;
      }
      this.buscando = false;
    })
  }

  @HostListener('window:scroll')
  _onWindowScroll(): void {
    if ($(window).scrollTop() + $(window).height() == $(document).height()) {
      if (this.moreResults) {
        this.realizarBusqueda();
      }
    }
  }

  getTipoDesc(tipo: string) {
    switch (tipo) {
      case 'OP_CONSULTAR_COMPLETO':
        return 'Consulta completa';
      case 'OP_DESACTIVAR':
        return 'Desactivar cuentas';
      default:
        return tipo.replace('_', ' ');
    }
  }

  borrarRegistro(element) {
    this.taskProvider.removeRecord(element.objectId).subscribe(result => {
      _.pull(this.taskList, element);
      this._alert.info("Registro borrado");

    })
  }


}
