import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { AdminProvider, PlanProvider } from '../../../../providers';
import { BaThemeSpinner } from '../../../../theme/services';
import { BaThemeAlert } from '../../../../theme';
import { PasswordValidator, EqualPasswordsValidator } from '../../../../theme/validators';
import { EventosFirmas, EventosUsuario } from '../../../../model/CambioEventos';

import { GlobalState } from '../../../../global.state';
import { Plan } from '../../../../model/Dtos';

@Component({
  selector: 'admin-planes',
  templateUrl: './planes.html',
  styleUrls: ['./planes.scss']
})
export class AdminPlanesComponent {


  planList = [];
  plan: Plan;
  onEdit: boolean;

  constructor(fb: FormBuilder, public adminProvider: AdminProvider, public planProvider: PlanProvider,
    private router: Router, private route: ActivatedRoute, private _spinner: BaThemeSpinner, private _alert: BaThemeAlert,
    private _state: GlobalState) {
    this.plan = new Plan();
  }

  ngOnInit() {
    this.traerPlanes();
  }

  traerPlanes() {
    this.planProvider.fetchPlans().subscribe(list => {
      this.planList = list;
      console.log(list);
    })
  }

  guardarPlan() {
    if (!this.esValidoPlan(this.plan)) {
      this._alert.error("Revise los datos que quiere guardar");
      return;
    }
    this.planProvider.savePlan(this.plan).subscribe(res => {
      if (res.status == "success") {
        this._alert.success("Se han guardado los datos");
        this.limpiarFormulario();
        this.traerPlanes();
      }
    });
  }

  editarPLan(item: Plan) {
    this.plan = item;
    this.onEdit = true;
  }

  cancelarEdicion() {
    this.limpiarFormulario();
  }

  eliminarPlan() {
    var r = confirm("¿Desea eliminar el Plan?");
    if (r) {
      this.planProvider.removePlan(this.plan.id).subscribe(res => {
        if (res.status == "success") {
          this._alert.success("El plan fue eliminado");
          this.limpiarFormulario();
          this.traerPlanes();
        }
      });
    }
  }

  limpiarFormulario(){
    this.onEdit = false;
    this.plan = new Plan();
  }


  esValidoPlan(pl: Plan) {
    if (!pl.nombre || pl.procesos <= 0 || pl.usuarios < 0 || pl.duracion <= 0) {
      return false;
    }
    return true;
  }


}
