import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { AdminProvider, PlanProvider } from '../../../../providers';
import { BaThemeSpinner } from '../../../../theme/services';
import { BaThemeAlert } from '../../../../theme';
import { PasswordValidator, EqualPasswordsValidator } from '../../../../theme/validators';
import { EventosFirmas, EventosUsuario } from '../../../../model/CambioEventos';

import { GlobalState } from '../../../../global.state';
import { Plan } from '../../../../model/Dtos';
import * as _ from 'lodash';
@Component({
  selector: 'admin-cuentas',
  templateUrl: './cuentas.html',
  styleUrls: ['./cuentas.scss']
})
export class AdminCuentasComponent {

  filter: any = {};
  listaUsuarios = [];
  planes: Plan[] = [];
  moreResults = false;
  u: any = null;
  planSel: Plan;
  planId = "";



  constructor(fb: FormBuilder, public adminProvider: AdminProvider, private router: Router,
    private route: ActivatedRoute, private _spinner: BaThemeSpinner, private _alert: BaThemeAlert,
    private _state: GlobalState, public planProvider: PlanProvider) {

    this.filter.email = "";
    this.filter.page = 0;
    this.filter.num = 15;


  }
  ngOnInit() {
    this.traerPlanes();
  }

  filtrarUsuarios() {

    if (this.filter.email == "") {
      return;
    }
    this.adminProvider.fetchUserList(this.filter).subscribe(list => {
      this.listaUsuarios = this.listaUsuarios.concat(list);
      if (list.length >= this.filter.num) {
        this.moreResults = true;
        this.filter.page++;
      } else {
        this.moreResults = false;
      }
    })
  }

  cargarInfoUsuario(userId) {
    this.cerrar();
    this.adminProvider.fetchUserData(userId).subscribe(user => {
      this.u = user;
      console.log(user)
    })
  }

  keyPressed($event) {
    if (($event.which === 1 || $event.which === 13 || $event.key === "Enter")) {
      this.buscar();
    }
  }
  buscar() {
    this.filter.page = 0;
    this.listaUsuarios = [];
    this.filtrarUsuarios();
  }

  masDatos() {
    this.filtrarUsuarios();
  }

  traerPlanes() {
    this.planProvider.fetchPlans().subscribe(list => {
      if (list) {
        this.planes = list;
      }
    })
  }

  elegirPlan(event) {
    if (event) {
      this.planSel = _.find(this.planes, p => {
        return p.id === event;
      })
    }
  }

  activarPlan() {
    this.planSel.precio = (this.planSel.procesos * this.planSel.valorProceso) + "";
    this.planProvider.activarPlan(this.planSel, this.u.usuario.id).subscribe(list => {
      this._alert.info("Plan Activado para el usuario");
    }, error => {

      this._alert.error("Ocurrió un error");
    })
  }

  cerrar() {
    this.planSel = null;
    this.u = null;
  }

}
