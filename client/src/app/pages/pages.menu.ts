export const PAGES_MENU = [
  {
    path: 'pages',
    children: [
      {
        path: 'cuenta',
        data: {
          menu: {
            title: 'Cuenta',
            icon: 'fa fa-cube',
            selected: false,
            expanded: true,
            order: 500,
            roles: ["client"]
          }
        },
        children: [
          {
            path: 'micuenta',
            data: {
              menu: {
                title: 'Mi firma',
                roles: ["client"]
              }
            }
          },
          {
            path: 'gestionar-usuarios',
            data: {
              menu: {
                title: 'Gestionar usuarios',
                roles: ["client"],
                permisos: ["admin", "gestionar"]
              }
            }
          },
        ]

      },
      {
        path: 'admin',
        data: {
          menu: {
            title: 'Admin',
            icon: 'fa fa-cube',
            selected: false,
            expanded: true,
            order: 500,
            roles: ["admin"]
          }
        },
        children: [
          {
            path: 'cuentas',
            data: {
              menu: {
                title: 'Admin de cuentas',
                roles: ["admin"]
              }
            }
          },
          {
            path: 'planes',
            data: {
              menu: {
                title: 'Admin de planes',
                roles: ["admin"]
              }
            }
          },
          
        ]

      },
      {
        path: 'processes',
        data: {
          menu: {
            title: 'Procesos',
            icon: 'fa fa-reorder',
            selected: false,
            expanded: true,
            order: 400,
            roles: ["client"]
          }
        },
        children: [
          {
            path: 'agregar',
            data: {
              menu: {
                title: 'Agregar procesos',
                roles: ["client"],
                permisos: ["admin", "editar"]
              }
            }
          },
          {
            path: 'lista',
            data: {
              menu: {
                title: 'Consultar procesos',
                roles: ["client"],
                permisos: ["admin", "consultar"]
              }
            }
          },
        ]
      },
      {
        path: 'firmas',
        data: {
          menu: {
            title: 'Administrar Firmas',
            icon: 'fa fa-suitcase',
            order: 800,
          }
        }
      },
      {
        path: '',
        data: {
          menu: {
            title: 'Página de La Rama',
            url: 'http://procesos.ramajudicial.gov.co/consultaprocesos/',
            icon: 'ion-android-exit',
            order: 800,
            target: '_blank', 
            roles: ["client", "admin"]
          }
        }
      }
    ]
  }
];
