import { Component } from '@angular/core';
import { Http, Response } from '@angular/http';
import { UserProvider } from "../../providers/user-provider/user-provider";

@Component({
  selector: 'home',
  styleUrls: ['./home.scss'],
  templateUrl: './home.html'
})
export class HomeComponent {

  constructor(public http: Http, private userService: UserProvider ) {
    
    
  }

}
