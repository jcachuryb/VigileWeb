import { Routes, RouterModule } from '@angular/router';

import { IndexPage } from './index.component';
import { ModuleWithProviders } from '@angular/core';

// noinspection TypeScriptValidateTypes
export const routes: Routes = [
  {
    path: '',
    component: IndexPage
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
