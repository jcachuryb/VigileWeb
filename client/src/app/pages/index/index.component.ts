import { Component } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationProvider } from '../../providers/auth-provider/auth-provider';

@Component({
  selector: 'indexcomponent',
  templateUrl: './index.html',
  styleUrls: ['./index.scss', './css/responsive.css'],
})
export class IndexPage {

  public form: FormGroup;
  public email: AbstractControl;
  public password: AbstractControl;
  public submitted: boolean = false;

  constructor(fb: FormBuilder, public authProvider: AuthenticationProvider) {
    
  }

  public onSubmit(values: Object): void {
    this.submitted = true;
    if (this.form.valid) {
    }
  }
}
