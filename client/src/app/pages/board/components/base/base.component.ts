import { Component, HostListener } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { AdminProvider, PlanProvider, TaskProvider } from '../../../../providers';
import { BaThemeSpinner } from '../../../../theme/services';
import { BaThemeAlert } from '../../../../theme';
import { GlobalState } from '../../../../global.state';

import * as _ from 'lodash';

@Component({
  selector: 'base',
  templateUrl: './base.html',
  styleUrls: ['./base.scss']
})
export class BaseComponent {



  constructor(fb: FormBuilder, public adminProvider: AdminProvider, public taskProvider: TaskProvider,
    private router: Router, private route: ActivatedRoute, private _spinner: BaThemeSpinner, private _alert: BaThemeAlert,
    private _state: GlobalState) {
  }

  ngOnInit() {
    
  }


}
