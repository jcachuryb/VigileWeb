import { Component, HostListener } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { BoardProvider } from '../../../../providers';
import { BaThemeSpinner } from '../../../../theme/services';
import { BaThemeAlert } from '../../../../theme';
import { GlobalState } from '../../../../global.state';

import * as _ from 'lodash';

@Component({
  selector: 'tablero-detalle',
  templateUrl: './tablero-detalle.html',
  styleUrls: ['./tablero-detalle.scss']
})
export class TableroDetalleComponent {

  id: string;

  constructor(fb: FormBuilder, public boardProvider: BoardProvider,
    private router: Router, private route: ActivatedRoute, private _spinner: BaThemeSpinner, private _alert: BaThemeAlert,
    private _state: GlobalState) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.id = params.get("id");
    })
  }


}
