import { Routes, RouterModule, ActivatedRouteSnapshot } from '@angular/router';

import { ModuleWithProviders } from '@angular/core';

import { AuthGuard, LoginGuard } from '../../_guards/';
import { MainBoardComponent } from './board.component';
import { TablerosComponent } from './components/tableros';
import { TableroDetalleComponent } from './components/tablero-detalle';


// noinspection TypeScriptValidateTypes

// export function loadChildren(path) { return System.import(path); };

const routes: Routes = [
  {
    path: '',
    component: MainBoardComponent,
    children: [
      {path: '', redirectTo: 'inicio', pathMatch: 'full' },
      {path: 'inicio', component: TablerosComponent},
      {path: 'tablero/:id', component: TableroDetalleComponent},
    ]
  }
];

export const routing = RouterModule.forChild(routes);

