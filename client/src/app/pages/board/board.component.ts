import { Component } from '@angular/core';
import { Routes, Router, ActivatedRoute, Params } from '@angular/router';

import { BaMenuService } from '../../theme';
import { GlobalState } from '../../global.state';
import { BaThemeSpinner } from '../../theme/services';
import { UserProvider } from '../../providers/user-provider/user-provider';
import { ClientProvider } from '../../providers/client-provider/client-provider';

@Component({
  selector: 'board',
  template: 
  '<router-outlet></router-outlet>'
})
export class MainBoardComponent {

  constructor(private _menuService: BaMenuService, private _spinner: BaThemeSpinner,
    private userProvider: UserProvider, private clientProvider: ClientProvider,
    private _state: GlobalState, private router: Router) {

  }

}
