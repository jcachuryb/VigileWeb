import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { routing } from './board.routing';
import { NgaModule } from '../../theme/nga.module';
import { ModalModule } from 'angular2-modal';
import { BootstrapModalModule } from 'angular2-modal/plugins/bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MainBoardComponent } from './board.component';
import { TablerosComponent } from './components/tableros';
import { TableroDetalleComponent } from './components/tablero-detalle';



@NgModule({
  imports: [CommonModule,
    NgaModule,
    routing,
    FormsModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    BootstrapModalModule,
  ],


  declarations: [
    MainBoardComponent,
    TablerosComponent,
    TableroDetalleComponent
  ]
})
export class MainBoardModule {
}
