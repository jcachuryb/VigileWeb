import { Component } from '@angular/core';
import { Http, Response } from '@angular/http';
import { UserProvider } from "../../providers/user-provider/user-provider";

@Component({
  selector: 'profile',
  template: `<router-outlet></router-outlet>`,
  
})
export class ProfileComponent {

  constructor(public http: Http, private userService: UserProvider ) {
    
    
  }

}
