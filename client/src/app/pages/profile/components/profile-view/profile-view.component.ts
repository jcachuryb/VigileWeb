import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationProvider } from '../../../../providers/auth-provider/auth-provider';
import { UserProvider } from '../../../../providers/user-provider/user-provider';
import { ClientProvider } from '../../../../providers/client-provider/client-provider';
import { BaThemeSpinner } from '../../../../theme/services';
import { GlobalState } from '../../../../global.state';
import { BaThemeAlert } from '../../../../theme';

import { EventosFirmas, EventosUsuario } from "../../../../model/CambioEventos";

@Component({
  selector: 'profile-view',
  templateUrl: './profile-view.html',
  styleUrls: ['./profile-view.scss']
})
export class ProfileView {


  public formGeneral: FormGroup;
  public nombre: AbstractControl;
  public documento: AbstractControl;
  public telefono: AbstractControl;
  public ciudad: AbstractControl;
  // ---------------------------------------------
  public formPersonal: FormGroup;
  public nombrecompleto: AbstractControl;
  // ---------------------------------------------
  public esPersona: boolean;
  public emailVerified: string;




  constructor(private _state: GlobalState, public fb: FormBuilder, public authProvider: AuthenticationProvider, private router: Router,
    private route: ActivatedRoute, private _spinner: BaThemeSpinner, private userProvider: UserProvider,
    private clientProvider: ClientProvider, private _alert: BaThemeAlert) {
    this.loadForm();
  }
  ngOnInit() {
    this.loadCurrentData();
  }

  loadForm() {
    this.formGeneral = this.fb.group({
      'nombre': ['', Validators.compose([Validators.required, Validators.minLength(2)])],
      'documento': ['', Validators.compose([])],
      'telefono': ['', Validators.compose([])],
      'ciudad': ['', Validators.compose([Validators.minLength(3)])]
    });
    this.nombre = this.formGeneral.controls['nombre'];
    this.documento = this.formGeneral.controls['documento'];
    this.telefono = this.formGeneral.controls['telefono'];
    this.ciudad = this.formGeneral.controls['ciudad'];
    // ---------------------------------------------
    this.formPersonal = this.fb.group({
      'nombrecompleto': ['', Validators.compose([Validators.required, Validators.minLength(4)])]
    });
    this.nombrecompleto = this.formPersonal.controls['nombrecompleto'];
    // ---------------------------------------------
  }

  loadCompanyData() {
    this.clientProvider.loadUserCompany().subscribe(value => {
      let company = value.company;
      this.nombre.setValue(company.nombre);
      this.documento.setValue(company.documento);
      this.telefono.setValue(company.telefonos);
      this.ciudad.setValue(company.ciudad);
    })

  }
  loadCurrentData() {
    this.userProvider.loadUserData().subscribe(value => {
      this.emailVerified = value.usuario.email;
    });

    this.loadCompanyData();
  }

  actualizarEmpresa() {
    var company = {
      nombre: this.nombre.value,
      documento: this.documento.value,
      telefono: this.telefono.value,
      ciudad: this.ciudad.value
    }
    this.clientProvider.updateCompany(company).subscribe(value => {
      this._alert.info("Se actualizó la información.");
    })
  }
}
