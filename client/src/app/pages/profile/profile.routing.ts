import { Routes, RouterModule }  from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { ProfileComponent } from './profile.component';
import { ProfileView } from './components/profile-view';

// noinspection TypeScriptValidateTypes
export const routes: Routes = [
  {
    path: '',
    component: ProfileComponent,
    children: [
      {path: '', redirectTo: 'miperfil'},
      { path: 'miperfil', component: ProfileView },
    ]
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
