import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ProcessProvider, UserProvider } from '../../../../providers/';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';


import { Overlay } from 'angular2-modal';
import { Modal } from 'angular2-modal/plugins/bootstrap';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BaThemeAlert } from '../../../../theme';

import * as _ from 'lodash';
import * as moment from 'moment';
moment.locale('es');

@Component({
  selector: 'detalleproceso',
  templateUrl: './detalleproceso.html',
  styleUrls: ['./detalleproceso.scss']
})
export class ProcessDetailView {
  @Input() procesoId;
  @Output() close = new EventEmitter<any>();

  proceso: any = {};
  objId = "";
  esNuevo: Boolean;
  tieneProblema: Boolean;
  estaSuscrito: Boolean;
  ready: Boolean = false;
  isBusy: Boolean = false;
  enEdicion: Boolean = false;
  actualizarAlVoler: Boolean = false;

  actuaciones = [];
  actuacionesNum = 8;
  actuacionesPage = 0;
  moreResults = true;
  buscando = false;

  constructor(public processProvider: ProcessProvider, public userProvider: UserProvider, public router: Router,
    private route: ActivatedRoute, private _location: Location, private _alert: BaThemeAlert,
    private modalService: NgbModal, public modal: Modal) {
    this.esNuevo = false;
    this.tieneProblema = false;
  }

  ngOnInit() {
    // Subscribe to route params
    this.route.params.subscribe(params => {

      this.objId = this.procesoId;
      //this.objId = "n6F55SyOsO";
      if (!this.objId) {
        this._location.back();
      } else {
        this.processProvider.detalleProceso(this.objId)
          .subscribe(value => {
            this.proceso = value.data;
            this.esNuevo = typeof this.proceso.estadoActual.actuacion === "undefined";
            this.tieneProblema = !!this.proceso.problem.msg;
            this.ready = true;
            this.cargarBloqueSuscripcion();
            this.cargarActuaciones();
          },
            error => {
              this._alert.error("Ocurrió un error con los datos.");
              this.router.navigate(['/', null]);
            }
          );
      }
    });
  }

  cargarActuaciones() {
    this.buscando = true;
    let index = this.proceso.indexActuacion;
    this.processProvider.listarActuacionesProcesos(this.proceso.codProceso, index, this.actuacionesNum, this.actuacionesPage)
      .subscribe(value => {
        this.actuaciones = this.actuaciones.concat(value.data);
        if (value.data.length >= this.actuacionesNum) {
          this.moreResults = true;
          this.actuacionesPage++;
        } else {
          this.moreResults = false;
        }
        this.buscando = false;
      },
        error => {
          this.actuaciones = [];
          this.buscando = false;
          console.log(JSON.stringify(error));
        }
      );
  }

  cargarBloqueSuscripcion() {
    let suscriptores = this.proceso.suscriptores;

    let currentUser = this.userProvider.getCurrentUser();
    if (currentUser) {
      let res = _.filter(suscriptores, function (sus: any) {
        return sus.userId === currentUser.id;
      });
      this.estaSuscrito = res.length > 0;
    }

  }

  reactivar() {
    this.isBusy = true;
    this.processProvider.reactivarProceso(this.proceso.objectId).subscribe(result => {
      this.isBusy = false;
      this.tieneProblema = false;
      this.volverALista("reactivate");
      this._alert.success("Proceso reactivado con éxito");
    }, error=>{
      this._alert.error("Ocurrió un error con la operación");
      this.isBusy = false;
    });
  }

  eliminarProceso() {
    this.isBusy = true;
    var r = confirm("Confirme que desea remover al proceso del sistema. Una vez eliminado, todos los usuarios que estén suscritos al proceso recibirán la alerta");
    if (r) {
      this.processProvider.borrarProceso(this.proceso.objectId).subscribe(res => {
        this.isBusy = false;
        if (res.success) {
          this._alert.success('El proceso fue eliminado del sistema');
          this.volverALista("delete");
        } else {
          this._alert.error(res.msg)
        }
      });
    }
  }

  mostrarVistaEdicion() {
    this.enEdicion = true;

  }

  reprocesar(val: boolean) {
    this.processProvider.reConsultarProceso(this.proceso.objectId, val).subscribe(result => {
      this.proceso.reconsultar = val;
      this.actualizarAlVoler = true;
    })
  }

  finishEditing(event) {
    this.enEdicion = false;
    if (event.status === "success") {
      var data = event.data.data;
      this.proceso.codProceso = data.codProceso;
      this.proceso.ciudad = data.ciudad;
      this.proceso.entidad = data.entidad;
      this.proceso.suscrito = this.estaSuscrito;
      this.actualizarAlVoler = true;
      this.tieneProblema = false;
    }
    if (event.status === "cancel") {

    }
  }
  cambioSuscripcion() {
    let processId = this.proceso.objectId;
    if (this.estaSuscrito) {
      this.processProvider.unSubscribeFromProcess(processId).subscribe(val => {
        if (val.success) {
          this._alert.info("No recibirá más notificaciones sobre este proceso");
          this.estaSuscrito = false;
        }
      })
    } else {
      this.processProvider.subscribeToProcess(processId).subscribe(val => {
        if (val.success) {
          this._alert.success("Suscripción al proceso exitosa");
          this.estaSuscrito = true;
        }
      })

    }
  }

  volverALista(action) {
    let emit: any = {};
    emit.action = action;
    emit.suscrito = this.estaSuscrito;
    this.close.emit(emit);
  }

  cerrar(action) {
    this.close.emit({ action: "none", refresh: this.actualizarAlVoler, suscrito: this.estaSuscrito });
  }

  formatDate(date: string) {
    return moment(date).format("D MMMM YYYY h:mm A")
  }

}