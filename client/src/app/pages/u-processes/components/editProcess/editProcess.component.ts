import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ProcessProvider } from '../../../../providers/process-provider/process-provider';
import { CiudadEntidadService } from '../../../../providers/data/ciudad-entidad.service';
import * as _ from 'lodash';
import { BaThemeAlert } from '../../../../theme';

@Component({
  selector: 'editprocess',
  templateUrl: './editProcess.html',
  styleUrls: ['./editProcess.scss']
})
export class EditProcess {
  @Input() proceso;
  @Output() finish = new EventEmitter<any>();


  ciudadesList = [];
  entidadesList = [];
  selCiudad = null;
  selEntidad = null;

  nCodProcess = "";
  valido: Boolean;
  bloqueado: Boolean;


  data_proceso = [];
  data_persona = {};
  data_predio = {};
  data = {};

  constructor(public processProvider: ProcessProvider, public ciudadEntidadService: CiudadEntidadService
    , private _alert: BaThemeAlert) {
    this.ciudadEntidadService.getCiudades().then(cities => {
      this.ciudadesList = cities;
      this.loadValues();
    });
    this.valido = false;
    this.bloqueado = false;
  }

  loadValues() {
    this.nCodProcess = this.proceso.codProceso;
    var proceso = this.proceso;
    var sciudad, sentidad;
    _.forEach(this.ciudadesList, function (ciudad) {
      //if (value.values.codProceso === newCodigo) {
      if (ciudad.value === proceso.data.ciudad) {
        sciudad = ciudad;
        return false;
      }
    });
    this.selCiudad = sciudad;
    this.ciudadEntidadService.getEntidades(sciudad.value).then(entidades => {
      this.entidadesList = entidades;
      _.forEach(this.entidadesList, function (entidad) {
        //if (value.values.codProceso === newCodigo) {
        if (entidad.value === proceso.data.entidad) {
          sentidad = entidad;
          return false;
        }
      });
      this.selEntidad = sentidad;
    });
  }

  cityChange(event) {
    this.selCiudad = event;
    this.selEntidad = null;
    this.ciudadEntidadService.getEntidades(event.value).then(entidades => {
      this.entidadesList = entidades;
    });
  }

  btnGuardar() {
    // Validar
    if (!this.validarEntradas()) {
      return false;
    }
    this.bloqueado = true;
    this.processProvider.editarProceso(this.proceso.objectId, this.data)
      .subscribe(res => {
        this.bloqueado = false;
        if (res.success) {
          this._alert.success("Proceso actualizado con éxito");
          this.proceso = null;
          this.emit("success", this.data);
        } else {
          if (res.status == "fail") {
            this._alert.warn("No se guardaron los datos. Ya existe un proceso con los datos ingresados");
          }
          if (res.status == "error") {
            this._alert.error("Ocurrió un error al guardar los datos. Recargue la página.");
            console.log(res)
          }
        }
      });
  }

  btnCancelar() {
    this.emit("cancel", null);
  }

  validarEntradas() {
    if (this.nCodProcess === this.proceso.codProceso && this.selCiudad.value === this.proceso.data.ciudad
      && this.selEntidad.value === this.proceso.data.entidad) {
        this._alert.warn("No ha realizado ningún cambio.");
      return false;
    }
    if (!this.nCodProcess.length || !this.selCiudad.value.length || !this.selEntidad) {
      this._alert.warn("Todos los campos son requeridos.");
      return false;
    }
    var input = { codProceso: this.nCodProcess, ciudad: this.selCiudad.nombre, entidad: this.selEntidad.nombre };
    var values = { codProceso: this.nCodProcess, ciudad: this.selCiudad.value, entidad: this.selEntidad.value }
    this.data = { values: values, data: input };
    return true;
  }

  emit(status, data) {
    this.finish.emit({ status: status, data: data })
  }
}