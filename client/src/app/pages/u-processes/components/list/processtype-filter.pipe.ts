import * as _ from 'lodash';
import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'processTypeFilter'
})
export class ProcessTypeFilterPipe implements PipeTransform {

    transform(array: any[], args: String): any {
        if (args) {
            return _.filter(array, row=> row.tipo === args);
        }
        return array;
    }
}