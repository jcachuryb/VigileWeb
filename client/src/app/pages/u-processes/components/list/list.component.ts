import { Component, HostListener } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CiudadEntidadService, ProcessProvider, AlmacenService, UserProvider } from '../../../../providers';
import * as _ from 'lodash';
import { BaThemeAlert } from '../../../../theme';
@Component({
  selector: 'listprocess',
  templateUrl: './list.html',
  styleUrls: ['./list.scss']
})
export class ListProcesses {
  ngOnInit() {
    // Subscribe to route params
    this.route.params.subscribe(params => {
      this.proceso = params.objectId;
    });
  }

  constructor(public processProvider: ProcessProvider, public userProvider: UserProvider,
    public router: Router, private route: ActivatedRoute,
    public ciudadEntidadService: CiudadEntidadService, public almacen: AlmacenService, private _alert: BaThemeAlert) {
    this.proceso;
    this.usuario = this.userProvider.getCurrentUser();
    this.numDatos = 15;
    this.setUpFiltros();
  }

  usuario;
  proceso;
  // -----------------------
  ciudadesList = [];
  entidadesList = [];
  filterList = [];
  filterEstadosList = [];
  selFilter = null;

  buscando = false;
  consultaQuery = false;
  filtros = [];
  // -----------------------
  procesos = [];
  filterQuery = "";
  rowsOnPage = 10000;
  sortBy = "info.codProceso";
  sortOrder = "asc";
  // -----------------------
  page: number;
  numDatos: number;
  moreResults: Boolean = true;

  setUpFiltros() {
    this.ciudadEntidadService.getCiudades().then(cities => {
      this.ciudadesList = cities;
    });
    this.filterList = [
      { name: "Ninguno", key: "", query: "contains" },
      { name: "Código del proceso", key: "codProceso", query: "contains" },
      { name: "Demandante", key: "info.demandantes", query: "contains" },
      { name: "Demandado", key: "info.demandados", query: "contains" }
    ];
    this.filterEstadosList = [
      { name: "Nuevos", key: "estados", query: "containedIn" },
      { name: "Con problema", key: "estados", query: "contains" },
    ];
    this.selFilter = this.filterList[0].name;

    this.almacen.getListProcessFilters().then((data: Array<any>) => {
      this.filtros = [];
      this.resetValues();
      this.realizarBusqueda();
    }).catch(err => {
      this.filtros = [];
    });
  }

  filtrarDatos() {
    this.resetValues();
    this.loadFiltervalue();
    this.realizarBusqueda();
  }

  borrarBusqueda() {
    this.filtros = [];
    this.resetValues();
    this.realizarBusqueda();
    this.entidadesList = [];
    this.filtroCiudad.value = "";
    this.filtroProblema.checked = false;
    this.selFilter = this.filterList[0].name;
    this.filtroString.value = "";
    _.forEach(this.filterList, function (fItem) {
      fItem.selected = false;
    })

  }

  actualizarBusqueda() {
    this.resetValues();
    this.realizarBusqueda();
  }

  realizarBusqueda() {
    let currentUserId = this.usuario.id;
    this.buscando = true;
    this.consultaQuery = this.filtros.length > 0;
    this.processProvider.listarProcesos(this.filtros, this.numDatos, this.page)
      .subscribe(value => {
        _.forEach(value.data, function (a) {
          a.suscrito = _.filter(a.suscriptores, (s: any) => {
            return s.userId === currentUserId;
          }).length > 0;
          a.stMessage = "";
          if (a.problem.msg) {
            a.stMessage = "Con problema";
            a.hasProblem = true;
          } else {
            if (!a.ultimaActuacion) {
              a.stMessage = "Nuevo Proceso";
              a.isNew = true;
            } else {
              a.isOK = true;
            }
          }
          if (a.reconsultar) {
            a.stMessage = "Marcado para reconsultar";
          }
        });
        this.procesos = this.procesos.concat(value.data);
        if (value.data.length >= this.numDatos) {
          this.moreResults = true;
          this.page++;
        } else {
          this.moreResults = false;
        }
        this.buscando = false;
      },
        error => {
          this.procesos = [];
          this.buscando = false;
          console.log(JSON.stringify(error));
          this._alert.error("Ocurrió un error con los datos.");
          this.router.navigate(['/', null]);
        }
      );
  }

  @HostListener('window:scroll')
  _onWindowScroll(): void {
    if ($(window).scrollTop() + $(window).height() == $(document).height()) {
      if (this.moreResults) {
        this.realizarBusqueda();
      }
    }
  }

  resetValues() {
    this.procesos = [];
    this.page = 0;
    this.moreResults = true;
  }

  mostrarDetalleProceso(item) {
    this.almacen.setListProcessFilters(this.filtros);
    this.proceso = item.id;
  }

  ocultarDetalleProceso(event) {
    let procesoId = this.proceso;
    let procesos = this.procesos;
    let selected = _.filter(procesos, (p: any) => {
      return p.id === procesoId;
    })
    selected[0].suscrito = event.suscrito;
    if (event.action != "none") {
      _.forEach(this.procesos, function (p) {
        if (p.id === procesoId) {
          if (event.action === "delete") {
            _.pull(procesos, p);
          }
          if (event.action === "reactivate") {
            p.problem = {};
            p.hasProblem = false;
            p.stMessage = "Ignorar y reactivar";
          }
          return false;
        }
      })
    }
    if (event.refresh) {
      this.actualizarBusqueda();
    }
    this.proceso = null;
  }


  cityChange(event) {
    if (event === "") {
      _.pull(this.filtros, this.filtroCiudad);
    } else {
      if (this.filtros.indexOf(this.filtroCiudad) < 0) {
        this.filtros.push(this.filtroCiudad);
      }
    }

    this.ciudadEntidadService.getEntidades(event).then(entidades => {
      this.entidadesList = entidades;
      if (this.filtroEntidad) {
        _.pull(this.filtros, this.filtroEntidad);
      }
    });
  }

  entityChange(event) {
    if (event === "") {
      _.pull(this.filtros, this.filtroEntidad);
    } else {
      if (this.filtros.indexOf(this.filtroEntidad) < 0) {
        this.filtros.push(this.filtroEntidad);
      }
    }
  }

  filterProblema(event) {
    if (event.srcElement.checked) {
      this.filtros.push(this.filtroProblema);
    } else {
      _.pull(this.filtros, this.filtroProblema);
    }
  }

  selectFilter(fItem) {
    this.selFilter = fItem.name;
    this.filtroString.key = fItem.key;
    if (fItem.key === "") {
      _.pull(this.filtros, this.filtroString);
    } else {
      if (this.filtros.indexOf(this.filtroString) < 0) {
        this.filtros.push(this.filtroString);
      }
    }
  }
  loadFiltervalue() {

  }

  accionSuscribir(proceso) {
    if (!proceso.suscrito) {
      proceso.suscrito = true;
      this.processProvider.subscribeToProcess(proceso.id).subscribe(val => {

      });
    } else {
      proceso.suscrito = false;
      this.processProvider.unSubscribeFromProcess(proceso.id).subscribe(val => {

      })
    }
  }

  filtroString = { name: "string", key: "", query: "contains", value: "", active: false };
  filtroProblema = { name: "estado", key: "problem.msg", query: "exists", value: [], checked: false };
  filtroCiudad = { name: "ciudad", key: "data.ciudad", query: "equalTo", value: "" };
  filtroEntidad = { name: "entidad", key: "data.entidad", query: "equalTo", value: "" };




}
