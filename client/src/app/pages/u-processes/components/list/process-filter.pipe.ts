import * as _ from 'lodash';
import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'processFilter'
})
export class ProcessFilterPipe implements PipeTransform {

    transform(array: any[], query: string): any {
/*
        if (query) {
            return _.filter(array, row=> _.join(row.filtros,"~").indexOf(query) > -1);
        }
*/
        return array;
    }
}