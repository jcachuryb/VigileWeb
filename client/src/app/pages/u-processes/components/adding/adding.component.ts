import { Component } from '@angular/core';
import { ProcessProvider, ClientProvider, AccountProvider } from '../../../../providers/';
import { CiudadEntidadService } from '../../../../providers/data/ciudad-entidad.service';
import { GlobalState } from '../../../../global.state';


import { BaThemeAlert } from '../../../../theme';

import * as _ from 'lodash';

@Component({
  selector: 'adding',
  templateUrl: './adding.html',
  styleUrls: ['./adding.scss']
})
export class AddingProcess {
  ciudadesList = [];
  entidadesList = [];
  selCiudad = null;
  selEntidad = null;
  building: boolean;

  nCodProcess = "";
  entradasSonValidas: boolean;
  bloqueado: boolean;

  datosCuenta: any = {};
  accountDataReady = false;

  data = [];

  cifrasPorc = {
    activos: "",
    error: "",
    disponible: "",
    mostrar: false
  }

  stats = {
    activos: 0,
    disponible: 0,
    error: 0
  }


  constructor(public processProvider: ProcessProvider, public clientProvider: ClientProvider, public accountProvider: AccountProvider,
    public ciudadEntidadService: CiudadEntidadService,
    private _state: GlobalState, private _alert: BaThemeAlert) {
    this.ciudadEntidadService.getCiudades().then(cities => {
      this.ciudadesList = cities;
    });

  }

  ngOnInit() {
    this.cargarInfoCuenta();
    this.entradasSonValidas = false;
    this.bloqueado = false;
  }

  resetValues() {
    this.data = [];
  }

  cityChange(event) {
    this.selCiudad = event;
    this.selEntidad = null;
    this.nCodProcess = event.value;
    this.ciudadEntidadService.getEntidades(event.value).then(entidades => {
      this.entidadesList = entidades;
    });
  }

  entityChange(event) {
    
    try {
      this.nCodProcess = this.nCodProcess.substr(0,5); 
      this.nCodProcess += event.value.split('-')[2];
    } catch (error) {
      
    }
    this._state.notifyDataChanged('adding.cambioEntidad', event);
  }

  btnAgregar() {
    if (!this.data) return;
    this.bloqueado = true;
    // Crear Filtros
    var filtros = getFilters(this.data);

    this.processProvider.agregarProcesos(this.data)
      .subscribe(res => {
        this.cargarCifras();
        this.bloqueado = false;
        if (res.status === "success") {
          this.resetValues();
          this._alert.success("Procesos guardados con éxito");
        } else {
          if (res.status == "exceeds") {
            this._alert.error("No se guardaron los procesos ya que exceden el límite de procesos en su cuenta.");
          }
          if (res.status == "inactive") {
            this._alert.warn("No puede agregar procesos si no tiene una suscripción activa");
          }
          if (res.status == "fail") {
            this._alert.error("No se guardaron los procesos porque algunos ya existen " + res.repetidos);
          }
          if (res.status == "error") {
            console.log(res)
          }
        }
      });
  }

  validarEntradas() {
    this.entradasSonValidas = this.validarProcesos();
  }

  validarProcesos() {
    return this.data.length > 0;
  }

  validarPersona() {
    return false;
  }

  sacarProceso(item) {
    _.pull(this.data, item);
    this.validarEntradas();
  }

  construirNumero() {
    this.building = true;
  }

  codigoArmado(data) {
    if (data.action == "add") {
      this.nCodProcess = data.codProceso;
      var l = this.data.length;
      this.addProcess();
      if (this.data.length > l) {
        this.building = false;
      } else {
        this._alert.warn("Ya ingresó un proceso con ese Código. Intente con otro.");
      }
    } else {
      this.building = false;
    }
  }

  addToDoItem($event) {
    if (($event.which === 1 || $event.which === 13 || $event.key === "Enter") && this.nCodProcess.trim() != '') {
      this.addProcess();
    }
  }

  private addProcess() {
    // Validar código

    var input = { codProceso: this.nCodProcess, ciudad: this.selCiudad.nombre, entidad: this.selEntidad.nombre };;
    var values = { codProceso: this.nCodProcess, ciudad: this.selCiudad.value, entidad: this.selEntidad.value }
    var existe = false;
    var newCodigo = this.nCodProcess;
    _.forEach(this.data, function (value) {
      //if (value.values.codProceso === newCodigo) {
      if (JSON.stringify(value.values) === JSON.stringify(values)) {
        existe = true;
        return;
      }
    });
    if (existe) return;

    this.data.push({ values: values, data: input });
    this.validarEntradas();
    this.nCodProcess = '';
  }
  cargarCifras() {

    return this.clientProvider.getSelCompanyProcessCount().subscribe(
      val => {
        let stats = val.stats;
        let activos = stats.activos + stats.nuevos;

        let d = (((stats.limite - stats.total) / stats.limite) * 100);
        let e = ((stats.error / stats.limite) * 100);
        let a = ((activos / stats.limite) * 100);
        this.cifrasPorc.disponible = d + "%";
        this.cifrasPorc.error = e + "%";
        this.cifrasPorc.activos = a + "%";
        // ------
        this.stats.activos = activos;
        this.stats.error = stats.error;
        this.stats.disponible = stats.limite - stats.total;
        this.cifrasPorc.mostrar = true;
      },
      error => {

      }
    )
  }

  cargarInfoCuenta() {
    this.accountProvider.loadSelCompanyAccountInfo().subscribe(value => {
      if (value) {
        this.datosCuenta = value.cuenta;
        if (this.datosCuenta.active) {
          this.cargarCifras();
        }
        this.accountDataReady = true;
      } else {
        this._alert.error("Ocurrió un error al traer los datos");
        return false;
      }
    })
  }

  valueFormatter(data: any): string {
    return 'nombre';
  }


  listFormatter(data: any): string {
    return 'nombre';
  }
}


function getFilters(process) {
  var f = [];
  for (var i = 0; i < Object.keys(process).length; i++) {
    var key = Object.keys(process)[i];
    if (typeof process[key] == "string" && process[key].length >= 4) f.push(process[key]);
  }
  return f;
}