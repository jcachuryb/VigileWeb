import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ProcessProvider } from '../../../../providers/process-provider/process-provider';
import { GlobalState } from '../../../../global.state';
import { CiudadEntidadService } from '../../../../providers/data/ciudad-entidad.service';
import * as _ from 'lodash';

@Component({
    selector: 'construir-codigo',
    templateUrl: './construir.html',
    styleUrls: ['./construir.scss']
})
export class BuildProcessCodeComponent {

    @Input() codCiudad: any;
    @Input() data: any;
    @Output() done = new EventEmitter<any>();
    codProceso: string;
    ceros = "00000000000000";

    // CAMPOS
    entidad: string;
    year: string = "";
    despacho: string = "";
    radicacion: string = "";
    consecutivo: string = "";
    yearList = [];

    editDespacho: boolean

    constructor(private _state: GlobalState) {
        this._state.subscribe('adding.cambioEntidad', (isCollapsed) => {
            this.cambioEntidad();
            this.armarCodProceso();
        });
    }

    ngOnInit() {
        this.entidad = this.data.value.split("-")[2];
        this.loadYears();
        this.cambioEntidad();
        this.armarCodProceso();
    }

    cambioEntidad() {
        this.entidad = this.data.value.split("-")[2];
        this.editDespacho = this.data.data.despacho;
        this.despacho = this.editDespacho ? "" : "000";
    }

    armarCodProceso() {
        this.codProceso = this.codCiudad + this.entidad + this.despacho + this.year + this.radicacion + this.consecutivo;
    }

    keyDespacho(event) {
        this.despacho = this.cerosIzquierda(event, 3);
        this.armarCodProceso();
    }

    keyConsecutivo(event) {
        this.consecutivo = this.cerosIzquierda(event, 2);
        this.armarCodProceso();
    }
    keyRadicacion(event) {
        this.radicacion = this.cerosIzquierda(event, 5);
        this.armarCodProceso();
    }

    yearChange($event) {
        this.year = $event;
        this.armarCodProceso();
    }

    cerosIzquierda(value, length): string {
        var dif = length - value.length;
        if (!dif) {
            return value;
        }
        return this.ceros.substr(0, dif) + value;
    }


    loadYears() {
        var init = 1990;
        var end = new Date().getFullYear() - init;
        end += init;

        while (init <= end) {
            this.yearList.push("" + init++);
        }
    }

    btnAgregar() {
        this.done.emit({ codProceso: this.codProceso, action: "add" });
    }

    btnCancelar() {
        this.done.emit({ codProceso: null, action: "cancel" });
    }

}