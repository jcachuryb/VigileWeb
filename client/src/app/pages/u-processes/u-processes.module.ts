import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule as AngularFormsModule } from '@angular/forms';
import { AppTranslationModule } from '../../app.translation.module';
import { NgaModule } from '../../theme/nga.module';
import { NgbRatingModule } from '@ng-bootstrap/ng-bootstrap';
import { DataTableModule } from "angular2-datatable";
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';

import { routing } from './u-processes.routing';

import { UserProcesses } from './u-processes.component';
import { AddingProcess } from './components/adding';
import { BuildProcessCodeComponent } from './components/construircodigo';
import { ListProcesses } from './components/list';
import { EditProcess } from './components/editProcess';
import { ProcessDetailView } from './components/detalleproceso';
import { CategoriesComponent } from './components/categories';

import { ProcessFilterPipe } from "./components/list/process-filter.pipe";
import { ProcessTypeFilterPipe } from "./components/list/processtype-filter.pipe";

@NgModule({
  imports: [
    CommonModule,
    AngularFormsModule,
    AppTranslationModule,
    NgaModule,
    NgbDropdownModule,
    NgbRatingModule,
    routing,
    DataTableModule
  ],
  declarations: [
    AddingProcess,
    BuildProcessCodeComponent,
    ListProcesses,
    ProcessDetailView,
    EditProcess,
    CategoriesComponent,
    ProcessFilterPipe,
    ProcessTypeFilterPipe,
    UserProcesses,
  ]
})
export class ProcessesModule {
}
