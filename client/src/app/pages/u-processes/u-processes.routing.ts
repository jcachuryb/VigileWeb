import { Routes, RouterModule } from '@angular/router';

import { UserProcesses } from './u-processes.component';
import { AddingProcess } from './components/adding/';
import { ProcessDetailView } from './components/detalleproceso';
import { ListProcesses } from './components/list/';
import { CategoriesComponent } from './components/categories/';

// noinspection TypeScriptValidateTypes
const routes: Routes = [
  {
    path: '',
    component: UserProcesses,
    children: [
      {path: 'agregar', component: AddingProcess},
      { path: 'detalleproceso', component: ProcessDetailView },
      { path: 'lista', component: ListProcesses },
      { path: 'categorias', component: CategoriesComponent },
    ]
  }
];

export const routing = RouterModule.forChild(routes);
