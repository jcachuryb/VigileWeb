import {Component} from '@angular/core';

@Component({
  selector: 'processes',
  template: `<router-outlet></router-outlet>`
})
export class UserProcesses {

  constructor() {
  }
}
