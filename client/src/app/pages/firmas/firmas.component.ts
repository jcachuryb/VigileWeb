import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

import { ClientProvider, UserCompanyProvider, AccountProvider } from '../../providers';
import { ProviderUtils } from "../../providers/provider-utils";

import { BaThemeConfigProvider, colorHelper, BaThemeAlert } from '../../theme';
import { BaAppData, BaThemeSpinner } from '../../theme/services';
import { GlobalState } from '../../global.state';

import { Firma } from "../../model/Dtos";
import { EventosFirmas, EventosCuenta } from "../../model/CambioEventos";

import * as _ from 'lodash';
import * as moment from 'moment';
moment.locale('es');


@Component({
    selector: 'firmas',
    templateUrl: './firmas.html',
    styleUrls: ['./firmas.scss']
})
export class FirmasComponent {

    //Componente para ver invitaciones y administrar las firmas a las que se pertenece.
    private currentCompany;
    private firmas = [];
    private invitaciones = [];
    private listaPermisos = [{ title: "Consulta y revisión", label: "Revisar la lista de procesos añadidos.", id: "consultar" },
    { title: "Modificación de procesos", label: "Agregar, modificar, eliminar y archivar procesos.", id: "editar" },
    { title: "Asignación", label: "Determinar los apoderados y dependientes de los procesos. ", id: "asignar" },
    { title: "Gestión de usuarios", label: "Gestionar los usuarios asociados a la firma.", id: "gestionar" },
    { title: "Super usuario", label: "Tiene máximo poder en esta firma.", id: "admin" }];
    private isBusy = false;

    constructor(public userCompanyProvider: UserCompanyProvider, public router: Router, private route: ActivatedRoute,
        private _location: Location, public clientProvider: ClientProvider, public accountProvider: AccountProvider, private _baConfig: BaThemeConfigProvider,
        private _alert: BaThemeAlert, private _spinner: BaThemeSpinner,
        private _appData: BaAppData, private _utils: ProviderUtils, private _state: GlobalState) {

    }

    ngOnInit() {
        this.cargarInvitaciones();
        this.cargarFirmas();
        this._state.subscribe(EventosFirmas.INFO_CAMBIO_USUARIO_FIRMA, usuario => {
            this.currentCompany = this.clientProvider.getCurrentUserCompany();
        });
    }



    // INVITATIONS

    cargarInvitaciones() {
        this.userCompanyProvider.loadUserInvitations().subscribe(value => {
            if (value.status == "success") {
                this.invitaciones = value.data;
                this.invitaciones.forEach(invitacion => {
                    invitacion.listaPermisos = _.map(invitacion.permisos, p => {
                        return _.filter(this.listaPermisos, pp => {
                            return pp.id == p;
                        })
                    });
                });
            }
        }, error => {

        })
    }

    responderInvitacion(inv, value) {
        // Confirmar 
        this.isBusy = true;
        let accion = value ? "aceptar" : "rechazar"
        let val = confirm("Por favor, confirme que desea " + accion + " la invitación de " + inv.company.nombre)
        if (val) {
            this.userCompanyProvider.respondCompanyInvitation(inv.id, value).subscribe(value => {
                this.isBusy = false;
                if (value.status == "success") {
                    _.remove(this.invitaciones, function (i) {
                        return i.id === inv.id;
                    });
                    this._alert.success(value.msg);
                    // this._appData.updateFirmasInfo();
                    this.cargarFirmas();
                }
            }, error => {
                this._alert.error(value.msg);
            })
        }
    }

    // FIRMAS VIEW

    cargarFirmas() {
        this.userCompanyProvider.loadUserCompanies().subscribe(value => {
            if (value.status == "success") {
                this.firmas = value.data.companies;
                if (this.clientProvider.getCurrentUserCompany()) {
                    this.currentCompany = this.clientProvider.getCurrentUserCompany();
                }
            }
        }, error => {

        })
    }

    elegirFirma(firma) {
        if (this.currentCompany.objectId === firma.company.id) {
            return;
        }
        this._spinner.show();
        this.userCompanyProvider.selectUserCompany(firma.company.id).subscribe(value => {
            if (value.status == "fail") {
                this._alert.error(value.msg);
                this._spinner.hide();
            } else {
                this.clientProvider.loadSelectedCompany().subscribe(val => {
                    this._spinner.hide();
                    this.currentCompany = val.userCompany;
                    this._state.notifyDataChanged(EventosFirmas.INFO_CAMBIO_USUARIO_FIRMA, val.userCompany);
                    this._alert.info("Ahora está trabajando en la firma '" + firma.company.nombre + "'.");
                    this.accountProvider.isSelCompanyAccountActive().subscribe(val => {
                        this._state.notifyDataChanged(EventosCuenta.CAMBIO_ACTUAL_ACTIVA, val);
                    });
                }, error => {
                    this._alert.error("Ocurrió un error al cambiar de firmas.")
                    this._spinner.hide();
                })
            }
            // Notificar a la App que cambiamos de firma
        }, error => {
            this._spinner.hide();
            this._alert.error("Ocurrió un error ")
        })
    }

    salirDeFirma(firma) {
        this.userCompanyProvider.abandonCompany(firma.company.id).subscribe(value => {
            _.remove(this.firmas, firmas => {
                return firma.id === firmas.id;
            })
            this._alert.warn("Usted ha abandonado la firma " + firma.nombre);
        })
    }

    mostrarPermiso(permiso, permisos) {
        for (let i = 0; i < permisos.length; i++) {
            if (permisos[i] === permiso) {
                return true;
            }
        }
        return false;
    }

}