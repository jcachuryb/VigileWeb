import { Routes, RouterModule, ActivatedRouteSnapshot } from '@angular/router';
import { Pages } from './pages.component';
import { ModuleWithProviders } from '@angular/core';

import { AuthGuard, LoginGuard } from '../_guards/';

import { FirmasComponent } from './firmas';
import { PrimeraConfiguracion } from './account/components/completarRegistro';
import { PublicoComponente } from './publico/publico.component';
// noinspection TypeScriptValidateTypes

// export function loadChildren(path) { return System.import(path); };

export const routes: Routes = [
  {
    path: 'indexcomponent',
    loadChildren: 'app/pages/index/index.module#IndexModule'
  },

  {
    path: 'login',
    loadChildren: 'app/pages/login/login.module#LoginModule'
  },
  {
    path: 'recuperar-clave',
    loadChildren: 'app/pages/recuperar-clave/recuperar-clave.module#RecuperarClaveModule'
  },
  {
    path: 'registro',
    loadChildren: 'app/pages/register/main.module#MainRegisterModule'
  },
  {
    path: 'nuevo-usuario',
    component: PrimeraConfiguracion,
    canActivate: [AuthGuard]
  },
  {
    path: 'pages',
    component: Pages,
    children: [
      { path: '', redirectTo: 'cuenta', pathMatch: 'full' },
      { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
      { path: 'home', loadChildren: './home/home.module#HomeModule' },
      { path: 'editors', loadChildren: './editors/editors.module#EditorsModule' },
      { path: 'components', loadChildren: './components/components.module#ComponentsModule' },
      { path: 'charts', loadChildren: './charts/charts.module#ChartsModule' },
      { path: 'ui', loadChildren: './ui/ui.module#UiModule' },
      { path: 'forms', loadChildren: './forms/forms.module#FormsModule' },
      { path: 'tables', loadChildren: './tables/tables.module#TablesModule' },
      { path: 'maps', loadChildren: './maps/maps.module#MapsModule' },
      { path: 'processes', loadChildren: './u-processes/u-processes.module#ProcessesModule' },
      { path: 'cuenta', loadChildren: './account/account.module#AccountModule' },
      { path: 'perfil', loadChildren: './profile/profile.module#ProfileModule' },
      { path: 'admin', loadChildren: './admin/admin.module#MainAdminModule' },
      { path: 'pizarra', loadChildren: './board/board.module#MainBoardModule' },
      { path: 'firmas', component: FirmasComponent },
    ],
    canActivate: [AuthGuard],
    runGuardsAndResolvers: "always"
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
