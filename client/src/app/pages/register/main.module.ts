import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';

import { MainRegisterComponent } from './main.component';
import { Register } from './register.component';
import { ConfirmComponent } from './confirm.component';
import { routing }       from './main.routing';


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    NgaModule,
    routing
  ],
  declarations: [
    MainRegisterComponent,
    Register,
    ConfirmComponent
  ]
})
export class MainRegisterModule {}
