import { Routes, RouterModule }  from '@angular/router';

import { Register } from './register.component';
import { ConfirmComponent } from './confirm.component';
import { MainRegisterComponent } from './main.component';

const routes: Routes = [
  {
    path: '',
    component: MainRegisterComponent,
    children: [
      {path: '', redirectTo: 'formulario', pathMatch: 'full' },
      {path: 'formulario', component: Register},
      {path: 'confirmar-correo/:key', component: ConfirmComponent}
    ]
  }
];

export const routing = RouterModule.forChild(routes);
