import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { BaThemeSpinner, BaThemeAlert } from '../../theme/services';
import { AuthenticationProvider } from '../../providers/auth-provider/auth-provider';


import * as _ from 'lodash';

@Component({
    selector: 'confirmPage',
    templateUrl: './confirm.html',
    styleUrls: ['./register.scss']
})
export class ConfirmComponent {
    ready: Boolean = false;
    email: String = "";
    constructor(private _alert: BaThemeAlert, public router: Router, private route: ActivatedRoute, private _location: Location,
        private authProvider: AuthenticationProvider, private _spinner: BaThemeSpinner ) {
    }

    ngOnInit() {
        this._spinner.show();
        this.route.params.subscribe(params => {
            if (params.key) {
                this.authProvider.completarRegistro(params.key)
                    .subscribe(value => {
                        if (value.status === "success") {
                            this.email = value.datos.email;
                            this._spinner.hide();
                        } else {
                            this.email = "";
                            this.aLogin();
                        }
                    },
                    error => {
                        console.log(error);
                        alert("error");
                    });
            }
        });
    }

    aLogin() {
        this.router.navigate(['/login', { email: this.email }]);
    }

    validarCuenta() {

    }

}