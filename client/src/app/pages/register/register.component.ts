import { Component } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { EmailValidator, EqualPasswordsValidator, PasswordValidator } from '../../theme/validators';
import { AuthenticationProvider } from '../../providers/auth-provider/auth-provider';

@Component({
  selector: 'registerForm',
  templateUrl: './register.html',
  styleUrls: ['./register.scss', './register-media.css']
})
export class Register {

  public form: FormGroup;
  public name: AbstractControl;
  public email: AbstractControl;
  public password: AbstractControl;
  public repeatPassword: AbstractControl;
  public passwords: FormGroup;

  public submitted: boolean = false;
  public errorMsg: String = "";
  public nameReg: String = "";
  private captchaResp : String = "";

  constructor(fb: FormBuilder, public authProvider: AuthenticationProvider, private route: ActivatedRoute) {
    this.submitted = false;
    this.form = fb.group({
      'name': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
      'email': ['', Validators.compose([Validators.required, EmailValidator.validate])],
      'passwords': fb.group({
        'password': ['', Validators.compose([Validators.required, PasswordValidator.validate])],
        'repeatPassword': ['', Validators.compose([Validators.required, Validators.minLength(4)])]
      }, { validator: EqualPasswordsValidator.validate('password', 'repeatPassword') }),
      'recaptcha': ['', Validators.compose([Validators.required])]
    });

    this.name = this.form.controls['name'];
    this.email = this.form.controls['email'];
    this.passwords = <FormGroup>this.form.controls['passwords'];
    this.password = this.passwords.controls['password'];
    this.repeatPassword = this.passwords.controls['repeatPassword'];
    this.route.params.subscribe(params => {
      if (params.email) {
        this.form.controls['email'].setValue(params.email);
      }

    });
    jQuery('#nombre').focus();
  }

  public onSubmit(values: Object): void {
    if (this.form.valid) {
      this.dismissMsg();
      this.authProvider.registro(this.email.value, this.password.value, this.name.value, this.captchaResp)
        .subscribe(value => {
          if (value.status === "success") {
            this.nameReg = this.name.value;
            this.form.reset();
            this.submitted = true;
          } else {
            this.errorMsg = value.msg;
          }

        },
          error => {
            console.log(error);
          }
        );
    }
  }

  public dismissMsg() {
    this.errorMsg = "";
  }

  resolved(captchaResponse: string) {
    // console.log(`Resolved captcha with response ${captchaResponse}:`);
    this.captchaResp = captchaResponse;
  }
}
