import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';


@Component({
    selector: 'mainregistercomponent',
    template: '<router-outlet></router-outlet>',
    styleUrls: ['./register.scss']
})
export class MainRegisterComponent {

    constructor() {
    }

}