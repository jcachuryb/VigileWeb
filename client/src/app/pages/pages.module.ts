import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { routing } from './pages.routing';
import { NgaModule } from '../theme/nga.module';
import { AppTranslationModule } from '../app.translation.module';
import { ModalModule } from 'angular2-modal';
import { BootstrapModalModule } from 'angular2-modal/plugins/bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Pages } from './pages.component';
import { FirmasComponent } from './firmas';
import { PrimeraConfiguracion } from './account/components/completarRegistro';
import { PublicoComponente } from './publico/publico.component';
import { LoginModule } from './login/login.module';


@NgModule({
  imports: [CommonModule,
    AppTranslationModule,
    NgaModule,
    routing,
    FormsModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    BootstrapModalModule,
    LoginModule,
  ],


  declarations: [
    Pages,
    FirmasComponent,
    PrimeraConfiguracion,
    PublicoComponente
  ]
})
export class PagesModule {
}
