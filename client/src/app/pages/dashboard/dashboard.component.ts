import { Component } from '@angular/core';
import { Http, Response } from '@angular/http';
import { UserProvider } from "../../providers/user-provider/user-provider";

@Component({
  selector: 'dashboard',
  styleUrls: ['./dashboard.scss'],
  templateUrl: './dashboard.html'
})
export class Dashboard {

  constructor(public http: Http, private userService: UserProvider ) {
    
    
  }

}
