import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule as AngularFormsModule } from '@angular/forms';
import { AppTranslationModule } from '../../app.translation.module';
import { NgaModule } from '../../theme/nga.module';
import { NgbRatingModule } from '@ng-bootstrap/ng-bootstrap';
import { DataTableModule } from "angular2-datatable";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';

import { routing } from './publico.routing';

import { TarifasComponent } from './components/tarifas';
import { ContactoComponent } from './components/contacto';
import { PaginaHomeComponent } from './components/home';




@NgModule({
  imports: [
    CommonModule,
    AngularFormsModule,
    NgaModule,
    NgbDropdownModule,
    routing,
    
    ReactiveFormsModule,
    FormsModule,
    NgaModule,
  ],
  declarations: [
    TarifasComponent,
    PaginaHomeComponent,
    ContactoComponent,
  ]
})
export class PublicoModule {
}
