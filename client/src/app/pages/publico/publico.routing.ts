import { Routes, RouterModule } from '@angular/router';
import { PublicoComponente } from './publico.component';
import { ContactoComponent } from './components/contacto';
import { PaginaHomeComponent } from './components/home';
import { TarifasComponent } from './components/tarifas';
import { Login } from '../login';



// noinspection TypeScriptValidateTypes
const routes: Routes = [
  {
    path: 'inicio',
    component: PublicoComponente,
    children: [
      { path: '', redirectTo: 'intro', pathMatch: 'full' },
      {path: 'intro', component: PaginaHomeComponent},
      {path: 'contacto', component: ContactoComponent},
      {path: 'planes', component: TarifasComponent},
      {path: 'login', component: Login},
      {path: 'registro', loadChildren: 'app/pages/register/main.module#MainRegisterModule'},
    ]
  }
];

export const routing = RouterModule.forChild(routes);
