import { Component, HostListener } from '@angular/core';

import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'paginas-publicas',
  templateUrl: './publico.html',
  styleUrls: ['./publico.scss']
})
export class PublicoComponente {

  fixTop = false;

  ngAfterViewInit() {
    this._onWindowScroll();
  }


  @HostListener('window:scroll')
  _onWindowScroll(): void {
    // Get the header
    var header = jQuery('#v-header')[0];

    // Get the offset position of the navbar
    var sticky = header.offsetTop;
    this.fixTop = window.pageYOffset > sticky;
  }
}
