import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Location } from '@angular/common';


import { BaThemeConfigProvider, colorHelper, BaThemeAlert } from '../../../../theme';
import { BaAppData, BaThemeSpinner } from '../../../../theme/services';
import { GlobalState } from '../../../../global.state';


import * as _ from 'lodash';
import * as moment from 'moment';
moment.locale('es');


@Component({
    selector: 'tarifas',
    templateUrl: './tarifas.html',
    styleUrls: ['../../publico.scss', './tarifas.scss', './pago.scss']
})
export class TarifasComponent {

    constructor(public router: Router ) {
    }

    ngOnInit() {
        this.router.events.subscribe((evt) => {
            if (!(evt instanceof NavigationEnd)) {
                return;
            }
            window.scrollTo(0, 0)
        });
    }

    tarifas = [
        { titulo: "Gratuita", texto: "", precio: "0", procesos: 15, usuarios: 1, precioProceso: 0, precioUsuario: 0, icono: 'fa fa-user' },
        { titulo: "Personal", texto: "", precio: "70000", procesos: 30, usuarios: 4, precioProceso: 1000, precioUsuario: 8000, masivo: true },
        { titulo: "Inicial", texto: "", precio: "91000", procesos: 70, usuarios: 10, precioProceso: 1000, precioUsuario: 8000, masivo: true },
        { titulo: "Asociación", texto: "", precio: "164000", procesos: 100, usuarios: 10, precioProceso: 1000, precioUsuario: 8000, masivo: true },
        { titulo: "Organización", texto: "", precio: "408000", procesos: 300, usuarios: 10, precioProceso: 950, precioUsuario: 6000, masivo: true },
        { titulo: "Entidad", texto: "", precio: "550000", procesos: 500, usuarios: 10, precioProceso: 900, precioUsuario: 5000, masivo: true },
        { titulo: "Suits", texto: "", precio: "910000", procesos: 1000, usuarios: 100, precioProceso: 800, precioUsuario: 4000, masivo: true }
      ]
}