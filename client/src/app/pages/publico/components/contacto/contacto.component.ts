import { Component } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { PasswordValidator, EqualPasswordsValidator, EmailValidator, PhoneValidator } from '../../../../theme/validators';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { FeebackProvider } from '../../../../providers';


import { BaThemeConfigProvider, colorHelper, BaThemeAlert } from '../../../../theme';
import { BaAppData, BaThemeSpinner } from '../../../../theme/services';
import { GlobalState } from '../../../../global.state';


import * as _ from 'lodash';
import * as moment from 'moment';
moment.locale('es');


@Component({
    selector: 'contacto',
    templateUrl: './contacto.html',
    styleUrls: ['./contacto.scss', '../../publico.scss']
})
export class ContactoComponent {

    public busy: Boolean;
    public formContacto: FormGroup;
    public nombres: AbstractControl;
    public correo: AbstractControl;
    public telefono: AbstractControl;
    public pregunta: AbstractControl;
    public submitted: boolean = false;

    public captchaResp: string;

    constructor(fb: FormBuilder, private router: Router,
        private route: ActivatedRoute, private _spinner: BaThemeSpinner, private _alert: BaThemeAlert,
        private _state: GlobalState, private feedbackProvider: FeebackProvider) {
        this.formContacto = fb.group({
            'nombres': ['', Validators.compose([Validators.required, Validators.required])],
            'correo': ['', Validators.compose([Validators.required, EmailValidator.validate])],
            'telefono': ['', Validators.compose([PhoneValidator.validate])],
            'pregunta': ['', Validators.compose([Validators.required, Validators.minLength(20)])],
            'recaptcha': ['', Validators.compose([Validators.required])]
        });

        this.nombres = this.formContacto.controls['nombres'];
        this.correo = this.formContacto.controls['correo'];
        this.telefono = this.formContacto.controls['telefono'];
        this.pregunta = this.formContacto.controls['pregunta'];
        this.busy = false;
    }

    public onSubmit(values: Object): void {
        if (this.formContacto.valid) {

            let contactObj: any = {};

            contactObj.nombres = this.nombres.value;
            contactObj.correo = this.correo.value;
            contactObj.telefono = this.telefono.value;
            contactObj.pregunta = this.pregunta.value;
            this.feedbackProvider.postQuestion(contactObj, this.captchaResp)
                .subscribe(value => {
                    if (value.status === "success") {
                        this.formContacto.reset();
                        this._alert.success("Su pregunta ha sido enviada");
                    } 
                },
                    error => {
                        this._alert.error("Ha ocurrido un error. Intente más tarde");
                        console.log(error);
                    }
                );
        }
    }


    resolved(captchaResponse: string) {
        // console.log(`Resolved captcha with response ${captchaResponse}:`);
        this.captchaResp = captchaResponse;
    }
}