import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';


import { BaThemeConfigProvider, colorHelper, BaThemeAlert } from '../../../../theme';
import { BaAppData, BaThemeSpinner } from '../../../../theme/services';
import { GlobalState } from '../../../../global.state';


import * as _ from 'lodash';
import * as moment from 'moment';
moment.locale('es');


@Component({
    selector: 'home',
    templateUrl: './home.html',
    styleUrls: ['../../publico.scss', '../tarifas/tarifas.scss']
})
export class PaginaHomeComponent {




    procesosActivos = 60;
    firmasActivas = 20;
    notificaciones = 350;
    experiencia = 3;
  
  
    email = "";
    constructor(public router: Router, ) {
    }
  
  
  
  
  
  
    ngAfterViewInit() {
  
      /*------------------
        Navigation
      --------------------*/
      jQuery('.responsive-bar').on('click', function (event) {
        jQuery('.main-menu').slideToggle(400);
        event.preventDefault();
      });
      /*------------------
    Background set
  --------------------*/
      jQuery('.set-bg').each(function () {
        var bg = $(this).data('setbg');
        jQuery(this).css('background-image', 'url(' + bg + ')');
      });
      /*------------------
  Review
  --------------------*/
      let review_meta = jQuery(".review-meta-slider");
      let review_text = jQuery(".review-text-slider");
      review_text.on('changed.owl.carousel', function (event) {
        review_meta.trigger('next.owl.carousel');
      });
  
      review_meta.owlCarousel({
        loop: true,
        nav: false,
        dots: true,
        items: 1,
        margin: 20,
        autoplay: false,
        mouseDrag: false,
      });
  
  
      review_text.owlCarousel({
        loop: true,
        nav: true,
        dots: false,
        items: 1,
        margin: 20,
        autoplay: true,
        navText: ['<i class="ti-angle-left"><i>', '<i class="ti-angle-right"><i>'],
        animateOut: 'fadeOutDown',
        animateIn: 'fadeInDown',
      });
    }
  
  
    irARegistro() {
      this.router.navigate(['registro/formulario', { email: this.email }]);
    }
  
    tarifas = [
      { titulo: "Gratuita", texto: "", precio: "0", procesos: 15, usuarios: 1, precioProceso: 0, precioUsuario: 0, active: false },
      { titulo: "Personal", texto: "", precio: "70000", procesos: 30, usuarios: 1, precioProceso: 1000, precioUsuario: 8000, active: true }
    ]
}