import { Component } from '@angular/core';
import { Routes, Router, ActivatedRoute, Params } from '@angular/router';

import { BaMenuService } from '../theme';
import { GlobalState } from '../global.state';
import { PAGES_MENU } from './pages.menu';
import { BaThemeSpinner } from '../theme/services';
import { UserProvider } from "../providers/user-provider/user-provider";
import { ClientProvider } from "../providers/client-provider/client-provider";


import { EventosFirmas, EventosUsuario, EventosAplicacion } from "../model/CambioEventos";
import { Firma } from "../model/Dtos";

@Component({
  selector: 'pages',
  template: `
    <ba-sidebar></ba-sidebar>
    <ba-page-top></ba-page-top>
    <ba-feedback></ba-feedback>
    <div class="al-main">
      <div class="al-content">
        <ba-content-top></ba-content-top>
        <bloque-suscripcion></bloque-suscripcion>
        <router-outlet></router-outlet>
      </div>
    </div>
    <footer class="al-footer clearfix">
      <div class="al-footer-right" translate>{{'general.created_with'}} <i class="ion-heart"></i></div>
      <div class="al-footer-main clearfix">
        <div class="al-copy"><a href="https://www.facebook.com/juankakita" target="_blank">AB6</a> 2018</div>
        <ul class="al-share clearfix">
          <li><i class="socicon socicon-facebook"></i></li>
          <li><i class="socicon socicon-twitter"></i></li>
          <li><i class="socicon socicon-google"></i></li>
          <li><i class="socicon socicon-github"></i></li>
        </ul>
      </div>
    </footer>
    <ba-back-top position="200"></ba-back-top>
    `
})
export class Pages {

  constructor(private _menuService: BaMenuService, private _spinner: BaThemeSpinner,
    private userProvider: UserProvider, private clientProvider: ClientProvider,
    private _state: GlobalState, private router: Router) {
    this._state.subscribe(EventosAplicacion.ACTUALIZAR_MENU, value => {
     let userCompany = clientProvider.getCurrentUserCompany();
      this.updateMenu(userCompany.permisos);
    });

    
    this._state.subscribe(EventosFirmas.INFO_CAMBIO_USUARIO_FIRMA, userCompany => {
      this.updateMenu(userCompany.permisos);
    });
  }

  ngOnInit() {

  }

  public ngAfterViewInit(): void {
    // this.updateMenu();

    this._spinner.hide();
  }

  updateMenu(permisos) {
    this._menuService.updateMenuByRoutesAndPermissions();
  }

}
