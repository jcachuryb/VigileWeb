import { Component, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import * as $ from 'jquery';

import { GlobalState } from './global.state';
import { BaImageLoaderService, BaThemePreloader, BaThemeSpinner, BaAppData } from './theme/services';
import { BaThemeConfig } from './theme/theme.config';
import { layoutPaths } from './theme/theme.constants';
import { UserProvider, ClientProvider, AuthenticationProvider } from "./providers/";
import { EventosFirmas, EventosUsuario, EventosCuenta, EventosAplicacion } from "./model/CambioEventos";

import { Observable } from 'rxjs';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';



/*
 * App Component
 * Top Level Component
 */
@Component({
  selector: 'app',
  styleUrls: ['./app.component.scss'],
  template: `
    <main [class.menu-collapsed]="isMenuCollapsed" baThemeRun>
      <div class="additional-bg"></div>
      <router-outlet></router-outlet>
      <span defaultOverlayTarget></span>
    </main>
    <alert-service></alert-service>
  `
})
export class App {

  isMenuCollapsed: boolean = false;
  loadedAppData: boolean = false;
  afterLogin: boolean = false;

  constructor(private _state: GlobalState,
    private _imageLoader: BaImageLoaderService,
    private _spinner: BaThemeSpinner,
    private viewContainerRef: ViewContainerRef,
    private themeConfig: BaThemeConfig,
    private router: Router,
    private _appData: BaAppData,
    private authProvider: AuthenticationProvider,
    private userProvider: UserProvider, private clientProvider: ClientProvider,
    public toastr: ToastsManager, vcr: ViewContainerRef) {

    themeConfig.config();

    this._loadImages();

    // this.loadApplicationData();

    this._state.subscribe('menu.isCollapsed', (isCollapsed) => {
      this.isMenuCollapsed = isCollapsed;
    });

    this.LoadApp();
    this.toastr.setRootViewContainerRef(vcr);
    console.log("App Component Loaded");
  }

  public ngAfterViewInit(): void {
    // hide spinner once all loaders are completed
    BaThemePreloader.load().then((values) => {
      this._spinner.hide();
    });
  }

  private _loadImages(): void {
    // register some loaders
    // BaThemePreloader.registerLoader(this._imageLoader.load('/assets/img/sky-bg.jpg'));
  }


  private loadApplicationData() {
    BaThemePreloader.registerLoader(
      this._appData.loadData().then(res => {
        console.log("App Data Loaded");
        // --------
        let company = this.clientProvider.getCurrentCompany();
        if (company) {
          this._state._onEvent({ event: EventosAplicacion.ACTUALIZAR_MENU, data: company });
          if (company.configurada) {
            // this._state.notifyDataChanged(EventosAplicacion.ACTUALIZAR_MENU, true);
            if (this.afterLogin || this.router.url.includes("nuevo-usuario")) {
              this.afterLogin = false;
              // this._state.notifyDataChanged(EventosAplicacion.ACTUALIZAR_MENU, userCompany, true);
              this.router.navigate(['/#/pages/cuenta']).then(loaded => {
                this._state._onEvent({ event: EventosAplicacion.ACTUALIZAR_MENU, data: company })

              });
            }
          } else {
            this.router.navigate(['/nuevo-usuario']);
          }
        } else {
          this.router.navigate(['/']);
        }
        this.loadedAppData = true;
        return true;
      }).catch(err => {
        this.router.navigate(['/']);
      })
    );
  }

  LoadApp() {

    this.userProvider.isLogged().subscribe(val => {
      if (val.loggedIn) {
        this.loadApplicationData();
      }
    }, error => {
      this.exitApp();
    });

    this._state.subscribe(EventosUsuario.LOGIN, (hasLoggedIn) => {
      this.afterLogin = true;
      this.loadApplicationData();
    });
    this._state.subscribe(EventosFirmas.FIRMA_CONFIGURADA, (configurada) => {
      this.loadApplicationData();
    });

    this._state.subscribe(EventosUsuario.LOGOUT, datos => {
      this._spinner.show();
      this.authProvider.logOut().subscribe(value => {
        this._spinner.hide();
        this.router.navigate(['/']);
      },
        error => {
          this.router.navigate(['/']);
        }
      );
    });
  }

  exitApp() {
    return this.router.navigate(['/#/login']);
  }

}
