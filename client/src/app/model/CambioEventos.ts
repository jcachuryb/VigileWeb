export class EventosFirmas {
    public static readonly CAMBIO_INFO = "firma.cambioInfo";
    public static readonly INFO_CARGADA = "firma.infoCargada";
    public static readonly INFO_CAMBIO_USUARIO_FIRMA = "firma.cambioUsuarioFirma";
    public static readonly FIRMA_CONFIGURADA = "firma.configurada";
    
}

export class EventosUsuario {
    public static readonly LOGIN = "usuario.login";
    public static readonly LOGOUT = "usuario.logout";   
    public static readonly INFO_CARGADA = "usuario.infoCargada";   
}

export class EventosCuenta {
    public static readonly CAMBIO_ACTUAL_ACTIVA = "cuenta.es-activa";   
}

export class EventosAplicacion {
    public static readonly FORZAR_CARGAR_DATOS = "app.cargar-datos-app";   
    public static readonly ACTUALIZAR_MENU = "app.actualizar-menu";   
}