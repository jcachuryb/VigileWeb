export class Firma {
    id: string;
    nombre: string;
    ciudad: string;
    documento: string;
    telefonos: string[];
    nombreFirma: string;
    isOwner: boolean;
    permisos: any[];

}

export class Usuario {
    id: string;
    username: string;
    email: string;
}
export class UsuarioFirma {
    id: string;
    correo: string;
    nombre: string;
    usuarioId: string;
    firmaId: string;
    activo: boolean;
    permisos: string[];
    isOwner: boolean;
}

export class Persona {
    id: string;
    nombre: string;
    ciudad: string;
    documento: string;
    telefonos: string[];
}

export class Proceso {
    id: string;
    codProceso: string;
    ultimaAct: any;
    ciudad: string;
    entidad: string;
    data: any;
    info: any;
    activo: boolean;
    estadoActual: any;
    ultimoEscan: Date;
    suscriptores: any[];
    problem: any;
}

export class Plan {
    id: string;
    nombre: string = "";
    procesos: number = 0;
    usuarios: number = 0;
    valorUsuario: number = 0;
    valorProceso: number = 0;
    duracion: number = 30;
    precio : string = "";
}

export class PlanAction {
    action: string;
}

export class Task {
    id: string;
    tipo: string = "";
    comentarios : string = "";
    exito: Boolean ;
    inicicio: Date ;
    date : Date;
}

export class Board {
    id: string;
    nombre: string = "";
    actualizado : Date;
    activo: Boolean ;
}