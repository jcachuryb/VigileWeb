import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserProvider } from "../providers/user-provider/user-provider";

@Injectable()
export class LoginGuard implements CanActivate {

    constructor(private router: Router, public userProvider: UserProvider) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
       return true;
    }
}