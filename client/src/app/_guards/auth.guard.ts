import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { UserProvider } from "../providers/user-provider/user-provider";

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router, public userProvider: UserProvider) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        return this.userProvider.isLogged().map(res => {
            if (res.loggedIn) return true;
            this.router.navigate(['/']);
            return true;
        });
    }

    handleErrorObservable(error: Response | any) {
        return Observable.throw(error.statusText || error);
    }
}