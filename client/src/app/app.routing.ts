import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { AuthGuard } from './_guards/';
export const routes: Routes = [
  { path: '', redirectTo: 'inicio/intro', pathMatch: 'full' },
  // { path: 'registro/confirmarCorreo', redirectTo: 'registro/confirmarCorreo', pathMatch: 'full' },
  { path: '**', redirectTo: 'pages/cuenta' }
];


export const routing: ModuleWithProviders = RouterModule.forRoot(routes, { useHash: true  });
