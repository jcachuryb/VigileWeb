import { NgModule, ApplicationRef, LOCALE_ID } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateService } from '@ngx-translate/core';
/*
 * Platform and Environment providers/directives/pipes
 */
import { routing } from './app.routing';

// App is our top level component
import { App } from './app.component';
import { AppState, InternalStateType } from './app.service';
import { GlobalState } from './global.state';
import { NgaModule } from './theme/nga.module';
import { PagesModule } from './pages/pages.module';
import { ModalModule } from 'angular2-modal';
import { BootstrapModalModule } from 'angular2-modal/plugins/bootstrap';
import { SimpleNotificationsModule } from 'angular2-notifications';


//Custom Services
import { AuthGuard, LoginGuard } from "./_guards/";
import { UserProvider } from "./providers/user-provider/user-provider";
import { FeebackProvider } from "./providers/user-provider/feedback-provider";
import { AuthenticationProvider } from "./providers/auth-provider/auth-provider";
import { ProcessProvider } from "./providers/process-provider/process-provider";
import { ClientProvider } from "./providers/client-provider/client-provider";
import { UserCompanyProvider } from "./providers/client-provider/usercompany-provider";
import { AccountProvider } from "./providers/client-provider/account-provider";
import { CiudadEntidadService } from "./providers/data/ciudad-entidad.service";
import { AlmacenService } from "./providers/data/almacen.service";
import { ProviderUtils } from "./providers/provider-utils";
import { PublicoModule } from './pages/publico/publico.module';
import { AdminProvider, TaskProvider, BoardProvider } from './providers';
import { PlanProvider } from './providers/admin-provider/plan-provider';


// Application wide providers
const APP_PROVIDERS = [
  AppState,
  GlobalState
];

const CUSTOM_SERVICES = [
  AdminProvider,
  PlanProvider,
  BoardProvider,
  TaskProvider,
  AuthGuard,
  LoginGuard,
  UserProvider,
  FeebackProvider,
  ProcessProvider,
  ClientProvider,
  UserCompanyProvider,
  AccountProvider,
  CiudadEntidadService,
  AlmacenService,
  AuthenticationProvider,
  ProviderUtils
];

export type StoreType = {
  state: InternalStateType,
  restoreInputValues: () => void,
  disposeOldHosts: () => void
};

/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
@NgModule({
  bootstrap: [App],
  declarations: [
    App
  ],
  imports: [ // import Angular's modules
    BrowserModule,
    HttpModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    NgaModule.forRoot(),
    NgbModule.forRoot(),
    
    ModalModule.forRoot(),
    BootstrapModalModule,
    BrowserAnimationsModule,
    SimpleNotificationsModule.forRoot(),
    PagesModule,
    routing,
    PublicoModule
  ],
  providers: [ // expose our Services and Providers into Angular's dependency injection
    APP_PROVIDERS,
    CUSTOM_SERVICES,
    {
      provide: LOCALE_ID,
      useValue: 'es-ES'
    }
  ]
})

export class AppModule {

  constructor(public appState: AppState) {
  }
}
