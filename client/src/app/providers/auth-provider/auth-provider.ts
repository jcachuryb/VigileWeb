/// <reference path="../../../../typings/index.d.ts" />

import { Injectable } from '@angular/core';

import { Http } from '@angular/http';
import { Observable } from 'rxjs';

import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/toPromise';

import { ProviderUtils } from "../provider-utils";

var _ = require('lodash');

@Injectable()
export class AuthenticationProvider {


    constructor(public http: Http, private _utils: ProviderUtils) { }

    registro(email, password, nombre, captcha) {
        return this.http.post('/api/auth/registro', { email, password, nombre, captcha })
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }

    completarRegistro(key) {
        return this.http.post('/api/auth/completarRegistro', { key: key })
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }

    loginCredentials(email, password) {
        return this.http.post('/api/auth/loginCredentials', { email, password: password })
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }

    solicitudRecuperarClave(email, captcha) {
        return this.http.post('/api/auth/solicitarCambioClave', { email, captcha })
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }

    recuperarClaveConfirmacion(usuario, token, nuevaClave) {
        return this.http.post('/api/auth/recuperarClaveConfirmacion', { usuario, nuevaClave, token })
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }

    logOut() {
        return this.http.post('/api/auth/logout', null)
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }
}