/// <reference path="../../../../typings/index.d.ts" />

import { Injectable } from '@angular/core';

import { Http } from '@angular/http';
import { Observable } from 'rxjs';

import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/toPromise';

import { ProviderUtils } from "../provider-utils";

var _ = require('lodash');

@Injectable()
export class UserCompanyProvider {
    constructor(public http: Http, private _utils: ProviderUtils) { }

    currentCompany;

    selectUserCompany(companyId) {
        return this.http.post('/api/company/selectCompany', { company: companyId })
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }

    loadCompanyUsers() {
        return this.http.post('/api/company/getCompanyUsers', {})
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }

    loadUserCompanies() {
        return this.http.post('/api/company/getUserCompanies', {})
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }

    loadUserInvitations() {
        return this.http.post('/api/company/loadCompanyInvitations', {})
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }

    respondCompanyInvitation(invitationId, response) {
        return this.http.post('/api/company/respondCompanyInvitation', { id: invitationId, response: response })
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }

    addUsertoCompany(userEmail, data) {
        return this.http.post('/api/company/addUserToCompany', { email: userEmail, data: data })
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }

    abandonCompany(companyId) {
        return this.http.post('/api/company/abandonCompany', { companyId: companyId })
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }

    updateUserCompany(userId, data) {
        return this.http.post('/api/company/updateUserCompany', { userId: userId, data: data })
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }

    removeUsertoCompany(userEmail, data) {
        return this.http.post('/api/company/addUsefdfrToCompany', { email: userEmail, data: data })
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }

    cancelUserInvitation(userId) {
        return this.http.post('/api/company/cancelUserInvitation', { userId })
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }

    removeUserFromCompany(userId) {
        return this.http.post('/api/company/removeUserFromCompany', { userId })
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }
}