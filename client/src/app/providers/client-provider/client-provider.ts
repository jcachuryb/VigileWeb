/// <reference path="../../../../typings/index.d.ts" />

import { Injectable } from '@angular/core';

import { Http } from '@angular/http';
import { Observable } from 'rxjs';

import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/toPromise';

import { ProviderUtils } from "../provider-utils";
import { GlobalState } from '../../global.state';


import { EventosFirmas, EventosUsuario } from "../../model/CambioEventos";
import { Firma, UsuarioFirma } from "../../model/Dtos";

var _ = require('lodash');

@Injectable()
export class ClientProvider {
    constructor(public http: Http, private _utils: ProviderUtils, private _state: GlobalState) {

        this._state.subscribe(EventosUsuario.LOGOUT, datos => {
            this.currentCompany = null;
            this.currentUserCompany = null;
        });
    }

    currentCompany;
    currentUserCompany;

    updateCompany(companyData) {
        return this.http.post('/api/client/updateClientCompany', { company: companyData })
            .map(res => {
                return this.loadUserCompany();
            })
            .catch(this._utils.handleErrorObservable);
    }

    loadUserCompany() {
        return this.http.post('/api/client/getUserCompany', {})
            .map(res => {
                var body = res.json();
                this.currentCompany = body.company;
                return body;
            })
            .catch(this._utils.handleErrorObservable);
    }

    loadSelectedCompany() {
        return this.http.post('/api/client/getCurrentUserCompany', {})
            .map(res => {
                var body = res.json();
                this.currentUserCompany = body.userCompany;
                return body;
            })
            .catch(this._utils.handleErrorObservable);
    }

    loadCurrentUserCompanyAccountInfo() {
        return Observable.combineLatest(
            this.http.post('/api/client/getUserCompany', {}),
            this.http.post('/api/client/getOwnCompanyProcessSummary', {})
        ).map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable)
    }

    loadCompanyStats() {
        return this.http.post('/api/client/getOwnCompanyProcessSummary', {})
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }

    getSelCompanyProcessCount() {
        return this.http.post('/api/client/getSelCompanyProcessSummary', {})
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }

    getCurrentCompany() {
        return this.currentCompany;
    }

    getCurrentUserCompany() {
        return this.currentUserCompany;
    }

}