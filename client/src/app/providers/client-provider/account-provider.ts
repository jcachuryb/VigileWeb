/// <reference path="../../../../typings/index.d.ts" />

import { Injectable } from '@angular/core';

import { Http } from '@angular/http';
import { Observable } from 'rxjs';

import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/toPromise';

import { ProviderUtils } from "../provider-utils";

var _ = require('lodash');

@Injectable()
export class AccountProvider {
    constructor(public http: Http, private _utils: ProviderUtils) { }

    private isActiveCurrentAccount = true;

    getSelectedAccountActiva() {
        return this.isActiveCurrentAccount;
    }

    loadSelCompanyAccountInfo() {
        return this.http.post('/api/account/loadSelCompanyAccountInfo', {})
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }

    loadOwningCompanyAccountInfo() {
        return this.http.post('/api/account/loadOwningCompanyAccountInfo', {})
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }

    isOwningAccountActive() {
        return this.http.post('/api/account/isOwningAccountActive', {})
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }


    isSelCompanyAccountActive() {
        return this.http.post('/api/account/isSelCompanyAccountActive', {})
            .map(res => {
                var body = res.json();
                this.isActiveCurrentAccount = body.activa ? body.activa : false;
                return body.activa;
            })
            .catch(this._utils.handleErrorObservable);
    }

}