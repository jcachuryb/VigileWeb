import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

import { Router } from '@angular/router';

import * as _ from 'lodash';
import * as moment from 'moment';

@Injectable()
export class ProviderUtils {

    dateFormats: any[] = ["LL", "D MMMM YYYY h:mm A"]

    constructor(private _router: Router) {

    }

    extractData(res: any) {
        var body;
        if (res.length == undefined) {
            body = res.json();
        } else {
            body = [];
            for (var i = 0; i < res.length; i++) {
                body.push(res[i].json());
            }
        }
        return body || {};
    }
    handleErrorObservable(error: Response | any) {
        try {
            if (error.status === 500) {
                this._router.navigate(['/login']);
            }
            console.log(error.statusText || error);
            return Observable.throw(error.statusText || error);
        } catch (error) {
            console.log(error);
        }
    }

    formatDate(date: string, dateFormatID){
        let df = dateFormatID < this.dateFormats.length ? dateFormatID : 0;
        return moment(date).format(this.dateFormats[df]);
    }


}
