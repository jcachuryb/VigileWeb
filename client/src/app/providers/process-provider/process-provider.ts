/// <reference path="../../../../typings/index.d.ts" />

import { Injectable } from '@angular/core';

import { Http } from '@angular/http';
import { Observable } from 'rxjs';

import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/toPromise';

import { ProviderUtils } from "../provider-utils";

var _ = require('lodash');

@Injectable()
export class ProcessProvider {


    constructor(public http: Http, private _utils: ProviderUtils) { }

    agregarProcesos(procesos) {
        return this.http.post('/api/processes/addMultiple', { procesos })
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }


    listarProcesos(filtros, num, page) {
        return this.http.post('/api/processes/listProcesses', { filtros, num, page })
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }


    reactivarProceso(objectId) {
        return this.http.post('/api/processes/reactivate', { processId: objectId })
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }


    reConsultarProceso(objectId, reconsultar) {
        return this.http.post('/api/processes/reconsultar', { processId: objectId, reconsultar: reconsultar })
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }


    borrarProceso(objectId) {
        return this.http.post('/api/processes/deleteProcess', { processId: objectId })
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }


    editarProceso(objectId, proceso) {
        return this.http.post('/api/processes/updateBadProcess', { processId: objectId, proceso: proceso })
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }

    detalleProceso(objectId) {
        return this.http.post('/api/processes/loadProcessDetail', { processId: objectId })
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }

    listarActuacionesProcesos(codProceso, index,  num, page) {
        return this.http.post('/api/processes/loadActuacionesProceso', { codProceso, index,  num, page })
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }

    subscribeToProcess(idProceso) {
        return this.http.post('/api/processes/subscribeToProcess', { processId: idProceso })
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }
    unSubscribeFromProcess(idProceso) {
        return this.http.post('/api/processes/unSubscribeFromProcess', { processId: idProceso })
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }
}