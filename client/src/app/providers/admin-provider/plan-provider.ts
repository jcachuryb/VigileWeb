/// <reference path="../../../../typings/index.d.ts" />

import { Injectable } from '@angular/core';

import { Http } from '@angular/http';
import { Observable } from 'rxjs';

import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/toPromise';

import { ProviderUtils } from "../provider-utils";
import { Plan, PlanAction } from '../../model/Dtos';

var _ = require('lodash');

@Injectable()
export class PlanProvider {
    constructor(public http: Http, private _utils: ProviderUtils) { }

    fetchPlans() {
        return this.http.post('/api/plan/fetchPlans', {})
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }

    addPlanToUser(plan: Plan, action: PlanAction) {
        return this.http.post('/api/plan/addPlanToUser', { plan, action })
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }

    savePlan(plan) {
        return this.http.post('/api/plan/savePlan', { plan: plan })
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }

    removePlan(planId: string) {
        return this.http.post('/api/plan/removePlan', { planId })
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }

    // ----------------------------------------------------------------

    activarPlan(plan: Plan, userId) {
        return this.http.post('/api/suscripcion/activarPlan', { plan, userId })
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }

}