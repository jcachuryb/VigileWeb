/// <reference path="../../../../typings/index.d.ts" />

import { Injectable } from '@angular/core';

import { Http } from '@angular/http';
import { Observable } from 'rxjs';

import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/toPromise';

import { ProviderUtils } from "../provider-utils";
import { Plan, PlanAction } from '../../model/Dtos';

var _ = require('lodash');

@Injectable()
export class TaskProvider {
    constructor(public http: Http, private _utils: ProviderUtils) { }

    fetchTasks(tipo, desde, hasta, num, pagina) {
        return this.http.post('/api/task/fetchTasks', {tipo, desde, hasta, num, pagina})
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }

    removeRecord(idTask) {
        return this.http.post('/api/task/removeRecord', { idTask })
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }

}