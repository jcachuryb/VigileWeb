/// <reference path="../../../../typings/index.d.ts" />

import { Injectable } from '@angular/core';

import { Http } from '@angular/http';
import { Observable } from 'rxjs';

import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/toPromise';

import { ProviderUtils } from "../provider-utils";
import { Plan, PlanAction } from '../../model/Dtos';

var _ = require('lodash');

@Injectable()
export class AdminProvider {
    constructor(public http: Http, private _utils: ProviderUtils) { }

    fetchUserList(filter: any) {
        return this.http.post('/api/admin/fetchUserList', {filter})
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }

    fetchUserData(userId: string) {
        return this.http.post('/api/admin/fetchUserData', {userId})
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }

    addToUserPlan(plan: Plan, action: PlanAction) {
        return this.http.post('/api/admin/addToUserPlan', {plan, action})
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }
 
    createPlan(plan: Plan) {
        return this.http.post('/api/admin/createPlan', {plan})
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }

    deletePlan(planId: string) {
        return this.http.post('/api/admin/deletePlan', {planId})
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }

    editPlan(plan: Plan) {
        return this.http.post('/api/admin/editPlan', {plan})
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }



    loadSelCompanyAccountInfo() {
        return this.http.post('/api/account/loadSelCompanyAccountInfo', {})
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }

}