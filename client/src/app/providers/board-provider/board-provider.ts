/// <reference path="../../../../typings/index.d.ts" />

import { Injectable } from '@angular/core';

import { Http } from '@angular/http';
import { Observable } from 'rxjs';

import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/toPromise';

import { ProviderUtils } from "../provider-utils";
import { Board } from '../../model/Dtos';

var _ = require('lodash');

@Injectable()
export class BoardProvider {
    constructor(public http: Http, private _utils: ProviderUtils) { }

    fetchBoards() {
        return this.http.post('/api/board/fetchBoards', {})
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }

    addBoard(nombre) {
        return this.http.post('/api/board/addBoard', { nombre })
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }

}

function convertirObjeto(obj) {

    let b = new Board();
    b.nombre = obj.nombre;
    b.id = obj.objectId;
    b.activo = obj.activo;
    b.actualizado = obj.updatedAt;
    return b;
}

function mapearSalida(res: any) {
    var body;
    if (res.length == undefined) {
        body = res.json();
        if (body instanceof Array) {
            var objs = [];
            for (var i = 0; i < body.length; i++) {
                objs.push(convertirObjeto(body[i]));
            }
            body = objs;
        } else {
            body = convertirObjeto(body);
        }
    } else {
        body = [];
        for (var i = 0; i < res.length; i++) {
            body.push(convertirObjeto(res[i].json()));
        }
    }
    return body || {};
}