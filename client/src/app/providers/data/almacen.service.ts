import { Injectable } from '@angular/core';
var _ = require('lodash');

@Injectable()
export class AlmacenService {

    data;

    constructor() {
         this.data = { filtros: [] };
    }

    setListProcessFilters(filtros) {
        return new Promise((resolve, reject) => {
            this.data.filtros = filtros;
            resolve(true);
        });
    }

    getListProcessFilters() {
        return new Promise((resolve, reject) => {
            resolve(this.data.filtros);
        });
    }
}
