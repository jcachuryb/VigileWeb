import { Injectable } from '@angular/core';
var _ = require('lodash');

@Injectable()
export class CiudadEntidadService {

    ciudades = null;

    constructor() {
    }


    getData() {
        return [
            {
                nombre: 'APARTADO', value: '05045', entidades: [
                    {
                        nombre: 'JUZGADOS 1 AL 2 CIRCUITO ESPECIALIZADO EN RESTITUCIÓN DE TIERRAS DE APARTADO', value: '535-True-3121-05045-Juzgado de Circuito-Civil Especializado en Restitución de Tierras',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                ]
            },
            {
                nombre: 'ARAUCA', value: '81001', entidades: [
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DE ARAUCA (ESCRITURAL)', value: '551-True-2331-81001-Tribunal Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DE ARAUCA (ORALIDAD)', value: '555-True-2333-81001-Tribunal Administrativo-Oralidad',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DE ARAUCA (MIXTO)', value: '556-True-2339-81001-Tribunal Administrativo-Sección Unica Mixta (Escrit-oral)',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE ARAUCA', value: '552-True-2208-81001-Tribunal Superior-Unica',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DE ARAUCA (ESCRITURAL)', value: '553-True-3331-81001-Juzgado Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DE ARAUCA (ORALIDAD)', value: '554-True-3333-81001-Juzgado Administrativo-Oralidad',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                ]
            },
            {
                nombre: 'ARMENIA', value: '63001', entidades: [
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DEL QUINDIO (ESCRITURAL)', value: '242-True-2331-63001-Tribunal Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DEL QUINDIO (ORAL)', value: '417-True-2333-63001-Tribunal Administrativo-Oralidad',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR SALA PENAL DE ARMENIA', value: '509-True-2204-63001-Tribunal Superior-Penal',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR SALA CIVIL-FAMILIA-LABORAL DE ARMENIA', value: '299-True-2214-63001-Tribunal Superior-Sala Civil- Familia - Laboral',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DE ARMENIA (ESCRITURAL)', value: '1-True-3331-63001-Juzgado Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DE ARMENIA (ORAL)', value: '418-True-3333-63001-Juzgado Administrativo-Oralidad',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES DEL CIRCUITO DE ARMENIA', value: '5-True-3104-63001-Juzgado de Circuito-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS 1 y 3 CIVILES DEL CIRCUITO DE ARMENIA', value: '3-True-3103-63001-Juzgado de Circuito-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADO 2 CIVIL DEL CIRCUITO DE ARMENIA', value: '560-True-3103-63001-Juzgado de Circuito-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS DE FAMILIA DEL CIRCUITO DE ARMENIA', value: '378-True-3110-63001-Juzgado de Circuito-Familia',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS LABORALES DEL CIRCUITO DE ARMENIA', value: '2-True-3105-63001-Juzgado de Circuito-Laboral',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES MUNICIPALES DE ARMENIA', value: '334-True-4004-63001-Juzgado Municipal-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES MUNICIPALES DE ARMENIA', value: '335-True-4003-63001-Juzgado Municipal-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS SISTEMA PENAL ACUSATORIO DE ARMENIA', value: '4-True-4004-63001-Juzgado Municipal-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                ]
            },
            {
                nombre: 'BARRANCABERMEJA', value: '68081', entidades: [
                    {
                        nombre: 'JUZGADO 1 CIRCUITO ESPECIALIZADO EN RESTITUCIÓN DE TIERRAS DE BARRANCABERMEJA', value: '516-True-3121-68081-Juzgado de Circuito-Civil Especializado en Restitución de Tierras',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                ]
            },
            {
                nombre: 'BARRANQUILLA', value: '08001', entidades: [
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE BARRANQUILLA', value: '385-True-2231-08001-Tribunal Superior-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DE BARRANQUILLA (ORAL)', value: '464-True-3333-08001-Juzgado Administrativo-Oralidad',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DE BARRANQUILLA (ESCRITURAL)', value: '10-True-3331-08001-Juzgado Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES DEL CIRCUITO ESP. DE BARRANQUILLA', value: '12-True-3104-08001-Juzgado de Circuito-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES DEL CIRCUITO DE BARRANQULLA', value: '11-True-3104-08001-Juzgado de Circuito-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES DEL CIRCUITO DE BARRANQUILLA', value: '6-True-3103-08001-Juzgado de Circuito-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS DE FAMILIA DEL CIRCUITO DE BARRANQULLA', value: '9-True-3110-08001-Juzgado de Circuito-Familia',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS LABORALES DEL CIRCUITO DE BARRANQUILLA', value: '8-True-3105-08001-Juzgado de Circuito-Laboral',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES MUNICIPALES DE BARRANQUILLA', value: '13-True-4004-08001-Juzgado Municipal-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES MUNICIPALES DE BARRANQUILLA', value: '7-True-4003-08001-Juzgado Municipal-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                ]
            },
            {
                nombre: 'BELLO', value: '05088', entidades: [
                    {
                        nombre: 'JUZGADO LABORAL DE BELLO', value: '574-True-3105-05088-Juzgado de Circuito-Laboral',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS DE FAMILIA DE BELLO', value: '568-True-3110-05088-Juzgado de Circuito-Familia',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES DEL CIRCUITO DE BELLO', value: '569-True-3103-05088-Juzgado de Circuito-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES MUNICIPALES DE BELLO', value: '570-True-4003-05088-Juzgado Municipal-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES DEL CIRCUITO CON FUNCIÓN DE CONOCIMIENTO DE BELLO', value: '571-True-3104-05088-Juzgado de Circuito-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES MUNICIPALES CON FUNCIÓN DE GARANTÍAS DE BELLO', value: '572-True-4088-05088-Juzgado Municipal-Penal con función de control de garantías',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES MUNICIPALES CON FUNCIÓN DE CONOCIMIENTO DE BELLO', value: '573-True-4009-05088-Juzgado Municipal-Penal con función de control de conocimiento',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS DE PEQUEÑAS CAUSAS MUNICIPAL DE BELLO', value: '575-True-4189-05088-Juzgado de pequeñas causas-Promiscuo',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                ]
            },
            {
                nombre: 'BOGOTA, D.C.', value: '11001', entidades: [
                    {
                        nombre: 'CONSEJO DE ESTADO - SALA DE CONSULTA Y SERVICIO CIVIL', value: '261-True-0306-11001-Consejo de Estado-Sala de Consulta y Servicio Civil',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'CONSEJO DE ESTADO - SECRETARIA GENERAL', value: '262-True-0315-11001-Consejo de Estado-Secretaria General',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'CONSEJO DE ESTADO - SALA PLENA', value: '19-True-0330-11001-Consejo de Estado-Sala Plena',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'CONSEJO DE ESTADO - SIN SECCION', value: '263-True-0331-11001-Consejo de Estado-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'CONSEJO DE ESTADO - SECCION PRIMERA', value: '20-True-0324-11001-Consejo de Estado-Sección Primera',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'CONSEJO DE ESTADO - SECCION PRIMERA (ORAL)', value: '504-True-0341-11001-Consejo de Estado-SECCION PRIMERA - ORAL',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'CONSEJO DE ESTADO - SECCION SEGUNDA', value: '21-True-0325-11001-Consejo de Estado-Sección Segunda',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'CONSEJO DE ESTADO - SECCION SEGUNDA (ORAL)', value: '505-True-0342-11001-Consejo de Estado-SECCION SEGUNDA - ORAL',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'CONSEJO DE ESTADO - SECCION TERCERA', value: '22-True-0326-11001-Consejo de Estado-Sección Tercera',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'CONSEJO DE ESTADO - SECCION TERCERA (ORAL)', value: '506-True-0336-11001-Consejo de Estado-SECCION TERCERA - ORAL',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'CONSEJO DE ESTADO - SECCION CUARTA', value: '23-True-0327-11001-Consejo de Estado-Sección Cuarta',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'CONSEJO DE ESTADO - SECCION CUARTA (ORAL)', value: '507-True-0337-11001-Consejo de Estado-SECCION CUARTA  - ORAL',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'CONSEJO DE ESTADO - SECCION QUINTA', value: '24-True-0328-11001-Consejo de Estado-Sección Quinta',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'CORTE SUPREMA DE JUSTICIA - SECRETARIA GENERAL', value: '264-True-0215-11001-Corte Suprema de Justicia-Secretaria General',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'CORTE SUPREMA DE JUSTICIA - SALA PENAL', value: '15-True-0204-11001-Corte Suprema de Justicia-Penal',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'CORTE SUPREMA DE JUSTICIA - SALA PLENA', value: '244-True-0230-11001-Corte Suprema de Justicia-Sala Plena',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'CORTE SUPREMA DE JUSTICIA - SALA LABORAL', value: '16-True-0205-11001-Corte Suprema de Justicia-Laboral',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'CORTE SUPREMA DE JUSTICIA - SALA CIVIL FAMILIA AGRARIA', value: '14-True-0203-11001-Corte Suprema de Justicia-Civil',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DE CUNDINAMARCA - SECRETARIA GENERAL', value: '266-True-2315-25000-Tribunal Administrativo-Secretaria General',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DE CUNDINAMARCA - SECCION PRIMERA (ESCRITURAL)', value: '267-True-2324-25000-Tribunal Administrativo-Sección Primera',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DE CUNDINAMARCA - SECCION PRIMERA (ORAL)', value: '500-True-2341-25000-Tribunal Administrativo-SECCION PRIMERA - ORAL',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DE CUNDINAMARCA - SECCION SEGUNDA (ESCRITURAL)', value: '268-True-2325-25000-Tribunal Administrativo-Sección Segunda',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DE CUNDINAMARCA - SECCION SEGUNDA (ORAL)', value: '501-True-2342-25000-Tribunal Administrativo-SECCION SEGUNDA - ORAL',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DE CUNDINAMARCA - SECCION TERCERA (ESCRITURAL)', value: '269-True-2326-25000-Tribunal Administrativo-Sección Tercera',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DE CUNDINAMARCA - SECCION TERCERA (ORAL)', value: '502-True-2336-25000-Tribunal Administrativo-SECCION TERCERA - ORAL',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DE CUNDINAMARCA - SECCION CUARTA (ESCRITURAL)', value: '270-True-2327-25000-Tribunal Administrativo-Sección Cuarta',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DE CUNDINAMARCA - SECCION CUARTA (ORAL)', value: '503-True-2337-25000-Tribunal Administrativo-SECCION CUARTA  - ORAL',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DE CUNDINAMARCA - SIN SECCIONES (ESCRITURAL)', value: '25-True-2331-25000-Tribunal Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DE CUNDINAMARCA - SIN SECCIONES (ORAL)', value: '498-True-2333-25000-Tribunal Administrativo-Oralidad',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE CUNDINAMARCA - SALA PENAL', value: '29-True-2204-25000-Tribunal Superior-Penal',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE CUNDINAMARCA - SALA CIVIL', value: '31-True-2203-25000-Tribunal Superior-Civil',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE CUNDINAMARCA - SALA LABORAL', value: '30-True-2205-25000-Tribunal Superior-Laboral',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE CUNDINAMARCA - SALA CIVIL-FAMILIA', value: '276-True-2213-25000-Tribunal Superior-Sala Civil-Familia',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE BOGOTA - SALA PENAL', value: '26-True-2204-11001-Tribunal Superior-Penal',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE BOGOTA - SALA CIVIL', value: '28-True-2203-11001-Tribunal Superior-Civil',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE BOGOTA - SALA FAMILIA', value: '212-True-2210-11001-Tribunal Superior-Familia',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE BOGOTA - SALA LABORAL', value: '27-True-2205-11001-Tribunal Superior-Laboral',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE BOGOTA SALA LABORAL DE DESCONGESTIÓN', value: '341-True-2205-11001-Tribunal Superior-Laboral',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR SALA LABORAL DE DESCONGESTIÓN -ANTIGUA-CASUR"', value: '342-True-2205-11001-Tribunal Superior-Laboral',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DE BOGOTA (ESCRITURAL)', value: '398-True-3331-11001-Juzgado Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DE BOGOTA (ORAL)', value: '499-True-3333-11001-Juzgado Administrativo-Oralidad',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES DEL CIRCUITO DE BOGOTA', value: '397-True-3103-11001-Juzgado de Circuito-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADO 44 CIVIL DEL CIRCUITO DE BOGOTA', value: '536-True-3103-11001-Juzgado de Circuito-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS FAMILIA DEL CIRCUITO DE BOGOTA', value: '347-True-3110-11001-Juzgado de Circuito-Familia',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADO 5 DE FAMILIA DEL CIRCUITO DE BOGOTA', value: '444-True-3110-11001-Juzgado de Circuito-Familia',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADO 8 DE FAMILIA DEL CIRCUITO DE BOGOTA', value: '442-True-3110-11001-Juzgado de Circuito-Familia',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADO 13 DE FAMILIA DEL CIRCUITO DE BOGOTA', value: '445-True-3110-11001-Juzgado de Circuito-Familia',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADO 19 DE FAMILIA DEL CIRCUITO DE BOGOTA', value: '446-True-3110-11001-Juzgado de Circuito-Familia',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADO 23 DE FAMILIA DEL CIRCUITO DE BOGOTA', value: '443-True-3110-11001-Juzgado de Circuito-Familia',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'SISTEMA PENAL ACUSATORIO JUZGADOS PENALES DE BOGOTA (DIRECCION SECCIONAL)', value: '37-True-1281-11001-Dirección Seccional de la Rama Judicial-JUZGADO CIVIL (CIRCUITO/MUNICIPAL)',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'SISTEMA PENAL ACUSATORIO JUZGADOS PENALES DE BOGOTA (DEL CIRCUITO)', value: '39-True-3109-11001-Juzgado de Circuito-Penal con función de control de conocimiento',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS LABORALES DEL CIRCUITO DE BOGOTA', value: '36-True-3105-11001-Juzgado de Circuito-Laboral',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS 39, 40, 41 y 42 CIVILES MUNICIPALES', value: '312-True-4003-11001-Juzgado Municipal-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES MUNICIPALES DE BOGOTA(CRA 10)', value: '34-True-4003-11001-Juzgado Municipal-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS 63,64,65,70 y 71 CIVILES MUNICIPALES - CRA DECIMA', value: '288-True-4003-11001-Juzgado Municipal-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES MUNICIPALES DE BOGOTA(NEMQUETEBA)', value: '35-True-4003-11001-Juzgado Municipal-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES DEL CIRCUITO ESPECIALIZADOS DE BOGOTA', value: '541-True-3107-11001-Juzgado de Circuito-Penal Especializado',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS 66,67,68 Y 69 CIVILES MUNICIPALES CONVIDA', value: '286-True-4003-11001-Juzgado Municipal-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS 55,56,57 Y 58 CIVILES MUNICIPALES - CONVIDA', value: '287-True-4003-11001-Juzgado Municipal-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS 1 Y 2 MUNICIPALES DE PEQUEÑAS CAUSAS', value: '537-True-4103-11001-Juzgado de pequeñas causas-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                ]
            },
            {
                nombre: 'BUCARAMANGA', value: '68001', entidades: [
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DE SANTANDER (ORAL)', value: '301-True-2333-68001-Tribunal Administrativo-Oralidad',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DE SANTANDER (ESCRITURAL)', value: '414-True-2331-68001-Tribunal Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE BUCARAMANGA - SALA PENAL', value: '42-True-2204-68001-Tribunal Superior-Penal',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE BUCARAMANGA - SALA CIVIL', value: '44-True-2203-68001-Tribunal Superior-Civil',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE BUCARAMANGA - SALA LABORAL', value: '43-True-2205-68001-Tribunal Superior-Laboral',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE BUCARAMANGA - SALA FAMILIA', value: '45-True-2210-68001-Tribunal Superior-Familia',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DE BUCARAMANGA (ESCRITURAL)', value: '297-True-3331-68001-Juzgado Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DE BUCARAMANGA (ORAL)', value: '466-True-3333-68001-Juzgado Administrativo-Oralidad',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS 1 AL 7 ADMINISTRATIVOS DE DESCONGESTIÓN DE BUCARAMANGA (ESCRITURAL)', value: '450-True-3331-68001-Juzgado Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS 1 AL 7 ADMINISTRATIVOS DE DESCONGESTIÓN DE BUCARAMANGA (ORAL)', value: '465-True-3333-68001-Juzgado Administrativo-Oralidad',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADO 15 ADMINISTRATIVO DE BUCARAMANGA', value: '586-True-3331-68001-Juzgado Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES DEL CIRCUITO DE BUCARAMANGA', value: '51-True-3104-68001-Juzgado de Circuito-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES DEL CIRCUITO DE BUCARAMANGA', value: '46-True-3103-68001-Juzgado de Circuito-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS DE FAMILIA DEL CIRCUITO DE BUCARAMANGA', value: '416-True-3110-68001-Juzgado de Circuito-Familia',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS LABORALES DEL CIRCUITO DE BUCARAMANGA', value: '48-True-3105-68001-Juzgado de Circuito-Laboral',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES MUNICIPALES DE BUCARAMANGA', value: '52-True-4004-68001-Juzgado Municipal-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES MUNICIPALES DE BUCARAMANGA', value: '47-True-4003-68001-Juzgado Municipal-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS 1 AL 7 DE EJECUCIÓN CIVILES MUNICIPALES DE BUCARAMANGA', value: '452-True-4303-68001-Juzgado Ejecucion Municipal-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS 1 AL 2 DE EJECUCIÓN CIVILES DE CIRCUITO DE BUCARAMANGA', value: '453-True-3403-68001-Juzgado Ejecucion Circuito-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADO 1 CIRCUITO ESPECIALIZADO EN RESTITUCIÓN DE TIERRAS DE BUCARAMANGA', value: '517-True-3121-68001-Juzgado de Circuito-Civil Especializado en Restitución de Tierras',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS DE EJECUCIÓN DE PENAS Y MEDIDAS DE SEGURIDAD', value: '587-True-3187-68001-Juzgado de Circuito-JUZGADO DE CIRCUITO -EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                ]
            },
            {
                nombre: 'BUGA', value: '76111', entidades: [
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE BUGA - SALA CIVIL FAMILIA', value: '367-True-2213-76111-Tribunal Superior-Sala Civil-Familia',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE BUGA - SALA PENAL', value: '366-True-2204-76111-Tribunal Superior-Penal',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE BUGA - SALA LABORAL', value: '53-True-2205-76111-Tribunal Superior-Laboral',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADO 1 AL 3 CIRCUITO ESPECIALIZADO EN RESTITUCIÒN DE TIERRAS DE BUGA', value: '513-True-3121-76111-Juzgado de Circuito-Civil Especializado en Restitución de Tierras',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES DEL CIRCUITO DE BUGA', value: '578-True-3103-76111-Juzgado de Circuito-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES MUNICIPALES DE BUGA', value: '579-True-4003-76111-Juzgado Municipal-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS FAMILIA DEL CIRCUITO DE BUGA', value: '580-True-3110-76111-Juzgado de Circuito-Familia',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                ]
            },
            {
                nombre: 'CALI', value: '76001', entidades: [
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DEL VALLE (ESCRITURAL)', value: '54-True-2331-76001-Tribunal Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DEL VALLE (ORAL)', value: '422-True-2333-76001-Tribunal Administrativo-Oralidad',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE CALI - SALA PENAL', value: '55-True-2204-76001-Tribunal Superior-Penal',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE CALI - SALA CIVIL', value: '57-True-2203-76001-Tribunal Superior-Civil',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE CALI - FAMILIA', value: '58-True-2210-76001-Tribunal Superior-Familia',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE CALI - SALA LABORAL', value: '56-True-2205-76001-Tribunal Superior-Laboral',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE CALI - SALA CIVIL ESP. EN RESTITUCIÓN DE TIERRAS', value: '361-True-2221-76001-Tribunal Superior-Civil Especializado en Restitución de Tierras',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DEL CIRCUITO DE CALI (ESCRITURAL)', value: '67-True-3331-76001-Juzgado Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DEL CIRCUITO DE CALI (ORAL)', value: '467-True-3333-76001-Juzgado Administrativo-Oralidad',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES DE CIRCUITO DE EJECUCIÓN DE CALI', value: '448-True-3103-76001-Juzgado de Circuito-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS 1 AL 8 y 16 AL 19 CIVILES DEL CIRCUITO DE CALI', value: '59-True-3103-76001-Juzgado de Circuito-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS 9 AL 15 CIVILES DEL CIRCUITO DE CALI', value: '336-True-3103-76001-Juzgado de Circuito-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS DEL CIRCUITO ESPECIALIZADOS EN RESTITUCIÓN DE TIERRAS DE CALI', value: '362-True-3121-76001-Juzgado de Circuito-Civil Especializado en Restitución de Tierras',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS DE FAMILIA DEL CIRCUITO DE CALI', value: '62-True-3110-76001-Juzgado de Circuito-Familia',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS 1-12 LABORALES Y PEQUEÑAS CAUSAS LABORALES DEL CIRCUITO DE CALI', value: '454-True-3105-76001-Juzgado de Circuito-Laboral',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS 13-18 LABORALES DEL CIRCUITO DE CALI', value: '447-True-3105-76001-Juzgado de Circuito-Laboral',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES MUNICIPALES LEY 600 DE CALI', value: '368-True-4004-76001-Juzgado Municipal-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS 1,6,11,16,21,26 Y 31 CIVILES MUNICIPALES DE CALI', value: '337-True-4003-76001-Juzgado Municipal-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS 2,3,4,5,7,8,9,10,12,13,14,15,17,18,19 Y 20 CIVILES MUNICIPALES CALI', value: '338-True-4003-76001-Juzgado Municipal-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS 22,23,24,25,27,28,29,30,32,33,34 Y 35 CIVILES MUNICIPALES CALI', value: '339-True-4003-76001-Juzgado Municipal-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES MUNICIPALES DE EJECUCIÓN DE CALI', value: '449-True-4003-76001-Juzgado Municipal-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                ]
            },
            {
                nombre: 'CARTAGENA', value: '13001', entidades: [
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DE BOLIVAR (ESCRITURAL)', value: '68-True-2331-13001-Tribunal Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DE BOLIVAR (ORAL)', value: '468-True-2333-13001-Tribunal Administrativo-Oralidad',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE CARTAGENA - SALA PENAL', value: '69-True-2204-13001-Tribunal Superior-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE CARTAGENA - SALA CIVIL FAMILIA', value: '71-True-2213-13001-Tribunal Superior-Sala Civil-Familia',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE CARTAGENA - SALA LABORAL', value: '70-True-2205-13001-Tribunal Superior-Laboral',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE CARTAGENA - ESPECIALIZADO EN RESTITUCIÓN DE TIERRAS', value: '532-True-2221-13001-Tribunal Superior-Civil Especializado en Restitución de Tierras',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DEL CIRCUITO DE CARTAGENA (ESCRITURAL)', value: '314-True-3331-13001-Juzgado Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DEL CIRCUITO DE CARTAGENA (ORAL)', value: '469-True-3333-13001-Juzgado Administrativo-Oralidad',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES DEL CIRCUITO DE CARTAGENA', value: '76-True-3104-13001-Juzgado de Circuito-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES DEL CIRCUITO ESP. DE CARTAGENA', value: '77-True-3107-13001-Juzgado de Circuito-Penal Especializado',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES DEL CIRCUITO DE CARTAGENA', value: '72-True-3103-13001-Juzgado de Circuito-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS DE FAMILIA DEL CIRCUITO DE CARTAGENA', value: '75-True-3110-13001-Juzgado de Circuito-Familia',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS LABORALES DEL CIRCUITO DE CARTAGENA', value: '74-True-3105-13001-Juzgado de Circuito-Laboral',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES MUNICIPALES DE CARTAGENA', value: '78-True-4004-13001-Juzgado Municipal-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES MUNICIPALES', value: '510-True-4003-13001-Juzgado Municipal-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS DE EJECUCIÓN CIVIL MUNICIPAL', value: '559-True-4003-13001-Juzgado Municipal-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS 1 AL 2 CIRCUITO ESPECIALIZADO EN RESTITUCIÓN DE TIERRAS DE CARTAGENA', value: '531-True-3121-13001-Juzgado de Circuito-Civil Especializado en Restitución de Tierras',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                ]
            },
            {
                nombre: 'CAUCASIA', value: '05154', entidades: [
                    {
                        nombre: 'JUZGADO 1 CIRCUITO ESPECIALIZADO EN RESTITUCIÓN DE TIERRAS DE CAUCASIA', value: '534-True-3121-05154-Juzgado de Circuito-Civil Especializado en Restitución de Tierras',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                ]
            },
            {
                nombre: 'CUCUTA', value: '54001', entidades: [
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DE N. SANTANDER (ESCRITURAL)', value: '80-True-2331-54001-Tribunal Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DE N. SANTANDER (ORAL)', value: '470-True-2333-54001-Tribunal Administrativo-Oralidad',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR SALA CIVIL FAMILIA DE CÚCUTA', value: '359-True-2213-54001-Tribunal Superior-Sala Civil-Familia',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR SALA CIVIL DE CÚCUTA', value: '360-True-2203-54001-Tribunal Superior-Civil',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR SALA CIVIL ESPECIALIZADA EN RESTITUCIÓN DE TIERRAS DE CÚCUTA', value: '519-True-2221-54001-Tribunal Superior-Civil Especializado en Restitución de Tierras',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE CUCUTA - SALA PENAL', value: '81-True-2204-54001-Tribunal Superior-Penal',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE CUCUTA - SALA CIVIL FAMILIA', value: '82-True-2213-54001-Tribunal Superior-Sala Civil-Familia',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE CUCUTA - SALA LABORAL', value: '348-True-2205-54001-Tribunal Superior-Laboral',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DE CÚCUTA (ESCRITURAL)', value: '340-True-3331-54001-Juzgado Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DE CÚCUTA (ORAL)', value: '471-True-3333-54001-Juzgado Administrativo-Oralidad',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES DEL CIRCUITO DE CUCUTA', value: '83-True-3103-54001-Juzgado de Circuito-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS DE FAMILIA DEL CIRCUITO DE CUCUTA', value: '86-True-3110-54001-Juzgado de Circuito-Familia',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS LABORALES DEL CIRCUITO DE CUCUTA', value: '85-True-3105-54001-Juzgado de Circuito-Laboral',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES MUNICIPALES DE CUCUTA', value: '84-True-4003-54001-Juzgado Municipal-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS 1 AL 2 CIRCUITO ESPECIALIZADO EN RESTITUCIÓN DE TIERRAS DE CUCUTA', value: '518-True-3121-54001-Juzgado de Circuito-Civil Especializado en Restitución de Tierras',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS DE PEQUEÑAS CAUSAS Y COMPETENCIA MÚLTIPLE', value: '582-True-4189-54001-Juzgado de pequeñas causas-Promiscuo',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                ]
            },
            {
                nombre: 'DUITAMA', value: '15238', entidades: [
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DE DUITAMA - BOYACA (ESCRITURAL)', value: '441-True-3331-15238-Juzgado Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                ]
            },
            {
                nombre: 'EL CARMEN DE BOLIVAR', value: '13244', entidades: [
                    {
                        nombre: 'JUZGADO 2 CIRCUITO ESPECIALIZADO EN RESTITUCIÓN DE TIERRAS DE CARMEN DE BOLÌVAR', value: '530-True-3121-13244-Juzgado de Circuito-Civil Especializado en Restitución de Tierras',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                ]
            },
            {
                nombre: 'ENVIGADO', value: '05266', entidades: [
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DE ENVIGADO (ESCRITURAL)', value: '90-True-3331-05266-Juzgado Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES DEL CIRCUITO DE ENVIGADO', value: '283-True-3104-05266-Juzgado de Circuito-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES DEL CIRCUITO DE ENVIGADO', value: '279-True-3103-05266-Juzgado de Circuito-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS DE FAMILIA DE ENVIGADO', value: '281-True-3110-05266-Juzgado de Circuito-Familia',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS LABORALES DE ENVIGADO', value: '280-True-3105-05266-Juzgado de Circuito-Laboral',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES MUNICIPALES DE ENVIGADO', value: '282-True-4004-05266-Juzgado Municipal-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES MUNICIPALES DE ENVIGADO', value: '379-True-4003-05266-Juzgado Municipal-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                ]
            },
            {
                nombre: 'FLORENCIA', value: '18001', entidades: [
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DEL CAQUETÁ (ESCRITURAL)', value: '311-True-2331-18001-Tribunal Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DEL CAQUETÁ (ORAL)', value: '472-True-2333-18001-Tribunal Administrativo-Oralidad',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE FLORENCIA - SALA PENAL LEY 906 (SISTEMA PENAL ACUSATORIO)', value: '350-True-2204-18001-Tribunal Superior-Penal',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE FLORENCIA - SALA PENAL LEY 600', value: '351-True-2209-18001-Tribunal Superior-Penal con función de control de conocimiento',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE FLORENCIA - SALA CIVIL', value: '349-True-2203-18001-Tribunal Superior-Civil',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DE FLORENCIA (ESCRITURAL)', value: '315-True-3331-18001-Juzgado Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DE FLORENCIA (ORAL)', value: '473-True-3333-18001-Juzgado Administrativo-Oralidad',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES DEL CIRCUITO DE FLORENCIA - LEY 906 (SISTEMA PENAL ACUSATORIO)', value: '352-True-3188-18001-Juzgado de Circuito-Penal con función de control de garantías',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES DEL CIRCUITO DE FLORENCIA - LEY 600', value: '357-True-3104-18001-Juzgado de Circuito-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES ESPECIALIZADOS DE CIRCUITO DE FLORENCIA - LEY 600', value: '358-True-3107-18001-Juzgado de Circuito-Penal Especializado',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES DEL CIRCUITO DE FLORENCIA', value: '317-True-3103-18001-Juzgado de Circuito-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS DE FAMILIA DE CIRCUITO DE FLORENCIA', value: '320-True-3110-18001-Juzgado de Circuito-Familia',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PROMISCUOS DE FAMILIA DE FLORENCIA', value: '356-True-3184-18001-Juzgado de Circuito-Promiscuo de Familia',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS LABORALES DEL CIRCUITO DE FLORENCIA', value: '319-True-3105-18001-Juzgado de Circuito-Laboral',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES MUNICIPALES DE FLORENCIA - LEY 906 (SISTEMA PENAL ACUSATORIO)', value: '353-True-4088-18001-Juzgado Municipal-Penal con función de control de garantías',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES MUNICIPALES DE FLORENCIA - LEY 600', value: '354-True-4004-18001-Juzgado Municipal-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES MUNICIPALES DE FLORENCIA', value: '318-True-4003-18001-Juzgado Municipal-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                ]
            },
            {
                nombre: 'IBAGUE', value: '73001', entidades: [
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DEL TOLIMA (ESCRITURAL)', value: '91-True-2331-73001-Tribunal Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DEL TOLIMA (ORAL)', value: '474-True-2333-73001-Tribunal Administrativo-Oralidad',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE IBAGUE - SALA PENAL', value: '92-True-2204-73001-Tribunal Superior-Penal',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE IBAGUE - SALA CIVIL FAMILIA', value: '94-True-2213-73001-Tribunal Superior-Sala Civil-Familia',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE IBAGUE - SALA LABORAL', value: '93-True-2205-73001-Tribunal Superior-Laboral',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DE IBAGUE (ESCRITURAL)', value: '103-True-3331-73001-Juzgado Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DE IBAGUE (ORAL)', value: '475-True-3333-73001-Juzgado Administrativo-Oralidad',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES DEL CIRCUITO DE IBAGUE', value: '99-True-3104-73001-Juzgado de Circuito-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES DEL CIRCUITO ESP. DE IBAGUE', value: '216-True-3104-73001-Juzgado de Circuito-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES DEL CIRCUITO DE IBAGUE', value: '95-True-3103-73001-Juzgado de Circuito-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS DE FAMILIA DEL CIRCUITO DE IBAGUE', value: '98-True-3110-73001-Juzgado de Circuito-Familia',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS LABORALES DEL CIRCUITO DE IBAGUE', value: '97-True-3105-73001-Juzgado de Circuito-Laboral',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES MUNICIPALES DE IBAGUE', value: '101-True-4004-73001-Juzgado Municipal-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES MUNICIPALES DE IBAGUE', value: '96-True-4003-73001-Juzgado Municipal-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS 1 AL 2 CIRCUITO ESPECIALIZADO EN RESTITUCIÓN DE TIERRAS DE IBAGUE', value: '514-True-3121-73001-Juzgado de Circuito-Civil Especializado en Restitución de Tierras',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                ]
            },
            {
                nombre: 'ITAGUI', value: '05360', entidades: [
                    {
                        nombre: 'JUZGADOS PENALES DEL CIRCUITO DE ITAGUI', value: '108-True-3104-05360-Juzgado de Circuito-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES DEL CIRCUITO DE ITAGUI', value: '278-True-3103-05360-Juzgado de Circuito-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS DE FAMILIA DEL CIRCUITO DE ITAGUI', value: '107-True-3110-05360-Juzgado de Circuito-Familia',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS LABORALES DEL CIRCUITO DE ITAGUI', value: '106-True-3105-05360-Juzgado de Circuito-Laboral',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES MUNICIPALES DE ITAGUI', value: '109-True-4004-05360-Juzgado Municipal-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES MUNICIPALES DE ITAGUI', value: '105-True-4003-05360-Juzgado Municipal-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                ]
            },
            {
                nombre: 'MANIZALES', value: '17001', entidades: [
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DE CALDAS (ESCRITURAL)', value: '126-True-2331-17001-Tribunal Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DE CALDAS (ORAL)', value: '476-True-2333-17001-Tribunal Administrativo-Oralidad',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE MANIZALES - SALA PENAL', value: '127-True-2204-17001-Tribunal Superior-Penal',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE MANIZALES - SALA CIVIL FAMILIA', value: '129-True-2213-17001-Tribunal Superior-Sala Civil-Familia',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE MANIZALES - SALA LABORAL', value: '128-True-2205-17001-Tribunal Superior-Laboral',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DE MANIZALES (ESCRITURAL)', value: '136-True-3331-17001-Juzgado Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DE MANIZALES (ORAL)', value: '477-True-3333-17001-Juzgado Administrativo-Oralidad',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES DEL CIRCUITO DE MANIZALES', value: '134-True-3104-17001-Juzgado de Circuito-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES ESPECIALIZADOS DEL CIRCUITO DE MANIZALES', value: '306-True-3107-17001-Juzgado de Circuito-Penal Especializado',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES DEL CIRCUITO DE MANIZALES', value: '130-True-3103-17001-Juzgado de Circuito-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS DE FAMILIA DEL CIRCUITO DE MANIZALES', value: '133-True-3110-17001-Juzgado de Circuito-Familia',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS LABORALES DEL CIRCUITO DE MANIZALES', value: '132-True-3105-17001-Juzgado de Circuito-Laboral',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES MUNICIPALES DE MANIZALES', value: '135-True-4004-17001-Juzgado Municipal-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES CON FUNCIÓN DE CONOCIMIENTO DE MANIZALES', value: '305-True-4009-17001-Juzgado Municipal-Penal con función de control de conocimiento',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES MUNICIPALES DE MANIZALES', value: '131-True-4003-17001-Juzgado Municipal-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                ]
            },
            {
                nombre: 'MEDELLIN', value: '05001', entidades: [
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DE ANTIOQUIA (ESCRITURAL)', value: '298-True-2331-05001-Tribunal Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DE ANTIOQUIA (ORAL)', value: '463-True-2333-05001-Tribunal Administrativo-Oralidad',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE ANTIOQUIA - SALA PENAL', value: '115-True-2204-05000-Tribunal Superior-Penal',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE ANTIOQUIA - SALA CIVIL FAMILIA', value: '117-True-2213-05000-Tribunal Superior-Sala Civil-Familia',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE ANTIOQUIA - SALA LABORAL', value: '116-True-2205-05000-Tribunal Superior-Laboral',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE ANTIOQUIA - SALA CIVIL ESP. EN RESTITUCIÓN DE TIERRAS', value: '383-True-2221-05001-Tribunal Superior-Civil Especializado en Restitución de Tierras',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE MEDELLIN - SALA PENAL', value: '111-True-2204-05001-Tribunal Superior-Penal',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE MEDELLIN - SALA CIVIL', value: '113-True-2203-05001-Tribunal Superior-Civil',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE MEDELLIN - SALA FAMILIA', value: '114-True-2210-05001-Tribunal Superior-Familia',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE MEDELLIN - SALA LABORAL', value: '112-True-2205-05001-Tribunal Superior-Laboral',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DE MEDELLIN (ESCRITURAL)', value: '125-True-3331-05001-Juzgado Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DE MEDELLIN (ORAL)', value: '462-True-3333-05001-Juzgado Administrativo-Oralidad',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES DEL CIRCUITO DE MEDELLIN', value: '122-True-3104-05001-Juzgado de Circuito-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES DEL CIRCUITO ESP. DE ANTIOQUIA', value: '380-True-3104-05001-Juzgado de Circuito-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES DEL CIRCUITO ESP. DE MEDELLIN', value: '221-True-3104-05001-Juzgado de Circuito-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES DEL CIRCUITO DE MEDELLIN', value: '118-True-3103-05001-Juzgado de Circuito-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES DEL CIRCUITO CON FUNCIÓN DE CONOCIMIENTO', value: '549-True-3109-05001-Juzgado de Circuito-Penal con función de control de conocimiento',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENAL DEL CIRCUITO ESPECIALIZADO EXTINCIÓN DE DOMINIO DE ANTIOQUIA', value: '577-True-3120-05001-Juzgado de Circuito-Penal Especializados de Extinción de Dominio',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES MUNICIPALES CON FUNCIÓN DE GARANTÍAS', value: '558-True-4088-05001-Juzgado Municipal-Penal con función de control de garantías',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES MUNICIPALES DE MEDELLIN', value: '124-True-4004-05001-Juzgado Municipal-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS DE FAMILIA DEL CIRCUITO DE MEDELLIN', value: '121-True-3110-05001-Juzgado de Circuito-Familia',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS LABORALES DEL CIRCUITO DE MEDELLIN', value: '120-True-3105-05001-Juzgado de Circuito-Laboral',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES MUNICIPALES DE MEDELLIN', value: '119-True-4003-05001-Juzgado Municipal-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS 1 AL 2 Y 101 CIRCUITO ESPECIALIZADO EN RESTITUCIÓN DE TIERRAS DE ANTIOQUIA', value: '382-True-3121-05001-Juzgado de Circuito-Civil Especializado en Restitución de Tierras',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                ]
            },
            {
                nombre: 'MOCOA', value: '86001', entidades: [
                    {
                        nombre: 'JUZGADO 1 CIRCUITO ESPECIALIZADO EN RESTITUCIÒN DE TIERRAS DE MOCOA', value: '511-True-3121-86001-Juzgado de Circuito-Civil Especializado en Restitución de Tierras',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                ]
            },
            {
                nombre: 'MONTERIA', value: '23001', entidades: [
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DE CORDOBA - SIN SECCIONES (ESCRITURAL)', value: '413-True-2331-23001-Tribunal Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DE CORDOBA - SIN SECCIONES (ORAL)', value: '478-True-2333-23001-Tribunal Administrativo-Oralidad',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE MONTERIA -- SALA CIVIL - FAMILIA - LABORAL', value: '410-True-2214-23001-Tribunal Superior-Sala Civil- Familia - Laboral',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE MONTERIA -- SALA PENAL', value: '539-True-2204-23001-Tribunal Superior-Penal',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DE MONTERÍA (ESCRITURAL)', value: '437-True-3331-23001-Juzgado Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DE MONTERÍA (ORAL)', value: '479-True-3333-23001-Juzgado Administrativo-Oralidad',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES DEL CIRCUITO DE MONTERIA', value: '408-True-3104-23001-Juzgado de Circuito-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS FAMILIA DEL CIRCUITO DE MONTERIA', value: '406-True-3110-23001-Juzgado de Circuito-Familia',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES DEL CIRCUITO DE MONTERÍA', value: '438-True-3103-23001-Juzgado de Circuito-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS LABORALES DEL CIRCUITO DE MONTERÍA', value: '439-True-3105-23001-Juzgado de Circuito-Laboral',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES MUNICIPALES DE MONTERIA', value: '407-True-4004-23001-Juzgado Municipal-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS 1, 2 y 3 CIVILES MUNICIPALES DE MONTERIA', value: '405-True-4003-23001-Juzgado Municipal-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS 4 y 5 CIVILES MUNICIPALES DE MONTERÍA', value: '561-True-4003-23001-Juzgado Municipal-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS 1 AL 2 CIRCUITO ESPECIALIZADO EN RESTITUCIÓN DE TIERRAS DE MONTERIA', value: '527-True-3121-23001-Juzgado de Circuito-Civil Especializado en Restitución de Tierras',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                ]
            },
            {
                nombre: 'NEIVA', value: '41001', entidades: [
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DEL HUILA (ESCRITURAL)', value: '296-True-2331-41001-Tribunal Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DEL HUILA (ORAL)', value: '480-True-2333-41001-Tribunal Administrativo-Oralidad',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE NEIVA - SALA PENAL', value: '138-True-2204-41001-Tribunal Superior-Penal',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE NEIVA - SALA CIVIL LABORAL FAMILIA', value: '139-True-2214-41001-Tribunal Superior-Sala Civil- Familia - Laboral',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DE NEIVA (ESCRITURAL)', value: '144-True-3331-41001-Juzgado Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DE NEIVA (ORAL)', value: '481-True-3333-41001-Juzgado Administrativo-Oralidad',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES DEL CIRCUITO DE NEIVA', value: '145-True-3104-41001-Juzgado de Circuito-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES DEL CIRCUITO DE NEIVA', value: '140-True-3103-41001-Juzgado de Circuito-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS DE FAMILIA DEL CIRCUITO DE NEIVA', value: '143-True-3110-41001-Juzgado de Circuito-Familia',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS LABORALES DEL CIRCUITO DE NEIVA', value: '142-True-3105-41001-Juzgado de Circuito-Laboral',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES MUNICIPALES DE NEIVA', value: '141-True-4003-41001-Juzgado Municipal-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES MUNICIPALES DE NEIVA', value: '147-True-4004-41001-Juzgado Municipal-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS DE PEQUEÑAS CAUSAS MÚLTIPLES DE NEIVA', value: '584-True-4189-41001-Juzgado de pequeñas causas-Promiscuo',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADO DE PEQUEÑAS CAUSAS LABORALES DE NEIVA', value: '585-True-4105-41001-Juzgado de pequeñas causas-Laboral',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                ]
            },
            {
                nombre: 'PALMIRA', value: '76520', entidades: [
                    {
                        nombre: 'JUZGADOS LABORALES DEL CIRCUITO DE PALMIRA', value: '387-True-3105-76520-Juzgado de Circuito-Laboral',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS DE FAMILIA DEL CIRCUITO DE PALMIRA', value: '455-True-3110-76520-Juzgado de Circuito-Familia',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES DEL CIRCUITO DE PALMIRA', value: '456-True-3103-76520-Juzgado de Circuito-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                ]
            },
            {
                nombre: 'PASTO', value: '52001', entidades: [
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DE NARIÑO (ESCRITURAL)', value: '423-True-2331-52001-Tribunal Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DE NARIÑO (ORAL)', value: '482-True-2333-52001-Tribunal Administrativo-Oralidad',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE PASTO SALA CIVIL - FAMILIA', value: '424-True-2213-52001-Tribunal Superior-Sala Civil-Familia',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE PASTO - SALA PENAL', value: '427-True-2204-52001-Tribunal Superior-Penal',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE PASTO - SALA LABORAL', value: '428-True-2205-52001-Tribunal Superior-Laboral',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DE PASTO (ORAL)', value: '435-True-3333-52001-Juzgado Administrativo-Oralidad',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DE PASTO (ESCRITURAL)', value: '483-True-3331-52001-Juzgado Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES DEL CIRCUITO DE PASTO', value: '425-True-3103-52001-Juzgado de Circuito-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES DEL CIRCUITO DE PASTO', value: '430-True-3104-52001-Juzgado de Circuito-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS LABORALES DEL CIRCUITO DE PASTO', value: '431-True-3105-52001-Juzgado de Circuito-Laboral',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS DE FAMILIA DEL CIRCUITO DE PASTO', value: '432-True-3110-52001-Juzgado de Circuito-Familia',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES MUNICIPALES DE PASTO', value: '429-True-4004-52001-Juzgado Municipal-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS MENORES CIVIL MUNICIPAL DE PASTO', value: '433-True-3185-52001-Juzgado de Circuito-Menores',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADO 1 CIRCUITO ESPECIALIZADO EN RESTITUCIÓN DE TIERRAS DE PASTO', value: '524-True-3121-52001-Juzgado de Circuito-Civil Especializado en Restitución de Tierras',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                ]
            },
            {
                nombre: 'Seleccione la Ciudad...', value: '66001', entidades: [
                ]
            },
            {
                nombre: 'POPAYAN', value: '19001', entidades: [
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DEL CAUCA (ESCRITURAL)', value: '148-True-2331-19001-Tribunal Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DEL CAUCA (ORAL)', value: '486-True-2333-19001-Tribunal Administrativo-Oralidad',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE POPAYAN - SALA PENAL', value: '150-True-2204-19001-Tribunal Superior-Penal',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE POPAYAN - SALA CIVIL - FAMILIA', value: '151-True-2214-19001-Tribunal Superior-Sala Civil- Familia - Laboral',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE POPAYÁN - SALA LABORAL', value: '303-True-2205-19001-Tribunal Superior-Laboral',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DEL CIRCUITO DE POPAYAN (ESCRITURAL)', value: '149-True-3331-19001-Juzgado Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DEL CIRCUITO DE POPAYAN (ORAL)', value: '487-True-3333-19001-Juzgado Administrativo-Oralidad',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS 9 Y 10 ADMINISTRATIVOS', value: '545-True-1100-19001-Consejo Seccional de la Judicatura-Sin Especialidad',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES DEL CIRCUITO DE POPAYAN', value: '156-True-3104-19001-Juzgado de Circuito-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES DEL CIRCUITO ESPECIALIZADO', value: '284-True-3107-19001-Juzgado de Circuito-Penal Especializado',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES DEL CIRCUITO DE POPAYAN', value: '152-True-3103-19001-Juzgado de Circuito-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS DE FAMILIA DEL CIRCUITO DE POPAYAN', value: '155-True-3110-19001-Juzgado de Circuito-Familia',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS LABORALES DEL CIRCUITO DE POPAYAN', value: '154-True-3105-19001-Juzgado de Circuito-Laboral',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES MUNICIPALES DE POPAYAN', value: '158-True-4004-19001-Juzgado Municipal-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES MUNICIPALES DE POPAYAN', value: '153-True-4003-19001-Juzgado Municipal-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADO 1 CIRCUITO ESPECIALIZADO EN RESTITUCIÓN DE TIERRAS DE POPAYAN', value: '529-True-3121-19001-Juzgado de Circuito-Civil Especializado en Restitución de Tierras',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                ]
            },
            {
                nombre: 'QUIBDO', value: '27001', entidades: [
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DE CHOCO (ESCRITURAL)', value: '376-True-2331-27001-Tribunal Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DE CHOCO (ORAL)', value: '460-True-2333-27001-Tribunal Administrativo-Oralidad',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE QUIBDO -- SALA CIVIL - FAMILIA - LABORAL', value: '538-True-2214-27001-Tribunal Superior-Sala Civil- Familia - Laboral',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE QUIBDO - SALA UNICA', value: '377-True-2208-27001-Tribunal Superior-Unica',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE QUIBDO - SALA UNICA - LEY 906', value: '542-True-2208-27001-Tribunal Superior-Unica',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DE QUIBDÓ (ESCRITURAL)', value: '304-True-3331-27001-Juzgado Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DE QUIBDÓ (ORAL)', value: '461-True-3333-27001-Juzgado Administrativo-Oralidad',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES DEL CIRCUITO DE QUIBDO', value: '163-True-3104-27001-Juzgado de Circuito-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES DEL CIRCUITO DE QUIBDO - LEY 906', value: '543-True-3104-27001-Juzgado de Circuito-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES DEL CIRCUITO DE QUIBDO', value: '159-True-3103-27001-Juzgado de Circuito-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS DE FAMILIA DEL CIRCUITO DE QUIBDO', value: '162-True-3110-27001-Juzgado de Circuito-Familia',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS LABORALES DEL CIRCUITO DE QUIBDO', value: '161-True-3105-27001-Juzgado de Circuito-Laboral',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES MUNICIPALES DE QUIBDO', value: '164-True-4004-27001-Juzgado Municipal-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES MUNICIPALES DE QUIBDO - LEY 906', value: '544-True-4004-27001-Juzgado Municipal-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES MUNICIPALES DE QUIBDO', value: '160-True-4003-27001-Juzgado Municipal-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADO 1 CIVIL ESPECIALIZADO DE RESTITUCIÓN DE TIERRAS', value: '546-True-3121-27001-Juzgado de Circuito-Civil Especializado en Restitución de Tierras',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES DEL CIRCUITO - LEY 600', value: '547-True-3104-27001-Juzgado de Circuito-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'SALA DISCIPLINARIA DEL CONSEJO SECCIONAL DE LA JUDICATURA DEL CHOCÓ', value: '581-True-1102-27001-Consejo Seccional de la Judicatura-Sala Disciplinaria',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                ]
            },
            {
                nombre: 'RIOHACHA', value: '44001', entidades: [
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DE LA GUAJIRA (ESCRITURAL)', value: '165-True-2331-44001-Tribunal Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DE LA GUAJIRA (ORAL)', value: '488-True-2333-44001-Tribunal Administrativo-Oralidad',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR - SALA PENAL DE RIOHACHA', value: '167-True-2204-44001-Tribunal Superior-Penal',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR SALA CIVIL-FAMILIA DE RIOHACHA', value: '550-True-2214-44001-Tribunal Superior-Sala Civil- Familia - Laboral',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DEL CIRCUITO DE RIOHACHA (ESCRITURAL)', value: '166-True-3331-44001-Juzgado Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DEL CIRCUITO DE RIOHACHA (ORAL)', value: '489-True-3333-44001-Juzgado Administrativo-Oralidad',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES DEL CIRCUITO DE RIOHACHA', value: '172-True-3104-44001-Juzgado de Circuito-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES DEL CIRCUITO DE RIOHACHA', value: '168-True-3103-44001-Juzgado de Circuito-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS DE FAMILIA DEL CIRCUITO DE RIOHACHA', value: '171-True-3110-44001-Juzgado de Circuito-Familia',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS LABORALES DEL CIRCUITO DE RIOHACHA', value: '170-True-3105-44001-Juzgado de Circuito-Laboral',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES MUNICIPALES DE RIOHACHA', value: '173-True-4004-44001-Juzgado Municipal-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES DEL MUNICIPALES DE RIOHACHA', value: '169-True-4003-44001-Juzgado Municipal-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                ]
            },
            {
                nombre: 'RIONEGRO', value: '05615', entidades: [
                    {
                        nombre: 'JUZGADOS CIVILES DEL CIRCUITO DE RIONEGRO', value: '252-True-3103-05615-Juzgado de Circuito-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES MUNICIPALES DE RIONEGRO', value: '256-True-4003-05615-Juzgado Municipal-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS LABORALES DEL CIRCUITO DE RIONEGRO', value: '257-True-3105-05615-Juzgado de Circuito-Laboral',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES DEL CIRCUITO DE RIONEGRO', value: '258-True-3104-05615-Juzgado de Circuito-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES MUNICIPALES DE RIONEGRO', value: '259-True-4004-05615-Juzgado Municipal-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PROMISCUOS DE FAMILIA DE RIONEGRO', value: '260-True-3184-05615-Juzgado de Circuito-Promiscuo de Familia',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                ]
            },
            {
                nombre: 'SAN GIL', value: '68679', entidades: [
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE SAN GIL', value: '567-True-2208-68679-Tribunal Superior-Unica',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DE SAN GIL', value: '562-True-3300-68679-Juzgado Administrativo-Sin Especialidad',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES DEL CIRCUITO DE SAN GIL', value: '563-True-3104-68679-Juzgado de Circuito-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES DEL CIRCUITO DE SAN GIL', value: '565-True-3103-68679-Juzgado de Circuito-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADO PROMISCUO FAMILIA DE SAN GIL', value: '564-True-3184-68679-Juzgado de Circuito-Promiscuo de Familia',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADO PROMISCUO MUNICIPAL DE SAN GIL', value: '566-True-4089-68679-Juzgado Municipal-Promiscuo',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                ]
            },
            {
                nombre: 'SANTA MARTA', value: '47001', entidades: [
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DEL MAGDALENA (ESCRITURAL)', value: '174-True-2331-47001-Tribunal Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DEL MAGDALENA (ORAL)', value: '492-True-2333-47001-Tribunal Administrativo-Oralidad',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE SANTA MARTA - SALA PENAL', value: '175-True-2204-47001-Tribunal Superior-Penal',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE SANTA MARTA - SALA CIVIL', value: '176-True-2203-47001-Tribunal Superior-Civil',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES DEL CIRCUITO DE SANTA MARTA', value: '177-True-3103-47001-Juzgado de Circuito-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES MUNICIPALES DE SANTA MARTA', value: '178-True-4003-47001-Juzgado Municipal-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS LABORALES DEL CIRCUITO DE SANTA MARTA', value: '179-True-3105-47001-Juzgado de Circuito-Laboral',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS DE FAMILIA DEL CIRCUITO DE SANTA MARTA', value: '180-True-3110-47001-Juzgado de Circuito-Familia',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES DEL CIRCUITO DE SANTA MARTA', value: '181-True-3104-47001-Juzgado de Circuito-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES DEL CIRCUITO ESP. DE SANTA MARTA', value: '182-True-3107-47001-Juzgado de Circuito-Penal Especializado',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES MUNICIPALES DE SANTA MARTA', value: '183-True-4004-47001-Juzgado Municipal-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DE SANTA MARTA (ESCRITURAL)', value: '184-True-3331-47001-Juzgado Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DE SANTA MARTA (ORAL)', value: '493-True-3333-47001-Juzgado Administrativo-Oralidad',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS 1 AL 2 CIRCUITO ESPECIALIZADO EN RESTITUCIÓN DE TIERRAS DE SANTA MARTA', value: '526-True-3121-47001-Juzgado de Circuito-Civil Especializado en Restitución de Tierras',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                ]
            },
            {
                nombre: 'SANTA ROSA DE VITERBO', value: '15693', entidades: [
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE SANTA ROSA DE VITERBO - SALA UNICA', value: '400-True-2208-15693-Tribunal Superior-Unica',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS DE FAMILIA DEL CIRCUITO DE SANTA ROSA DE VITERBO', value: '402-True-3110-15693-Juzgado de Circuito-Familia',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADO PROMISCUO DEL CIRCUITO DE SANTA ROSA DE VITERBO', value: '403-True-3189-15693-Juzgado de Circuito-Promiscuo',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                ]
            },
            {
                nombre: 'SINCELEJO', value: '70001', entidades: [
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DE SUCRE (ESCRITURAL)', value: '185-True-2331-70001-Tribunal Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DE SUCRE (ORAL)', value: '494-True-2333-70001-Tribunal Administrativo-Oralidad',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DEL CIRCUITO DE SINCELEJO (ESCRITURAL)', value: '186-True-3331-70001-Juzgado Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DEL CIRCUITO DE SINCELEJO (ORAL)', value: '495-True-3333-70001-Juzgado Administrativo-Oralidad',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE SINCELEJO - SALA PENAL', value: '187-True-2204-70001-Tribunal Superior-Penal',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE SINCELEJO - SALA CIVIL- FAMILIA - LABORAL', value: '222-True-2214-70001-Tribunal Superior-Sala Civil- Familia - Laboral',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES DEL CIRCUITO DE SINCELEJO', value: '189-True-3103-70001-Juzgado de Circuito-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES MUNICIPALES DE SINCELEJO', value: '190-True-4003-70001-Juzgado Municipal-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS LABORALES DEL CIRCUITO DE SINCELEJO', value: '191-True-3105-70001-Juzgado de Circuito-Laboral',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS DE FAMILIA DEL CIRCUITO DE SINCELEJO', value: '192-True-3110-70001-Juzgado de Circuito-Familia',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES DEL CIRCUITO DE SINCELEJO', value: '193-True-3104-70001-Juzgado de Circuito-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES MUNICIPALES DE SINCELEJO', value: '194-True-4004-70001-Juzgado Municipal-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS 1 AL 4 CIRCUITO ESPECIALIZADO EN RESTITUCIÓN DE TIERRAS DE SINCELEJO', value: '515-True-3121-70001-Juzgado de Circuito-Civil Especializado en Restitución de Tierras',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                ]
            },
            {
                nombre: 'TUMACO', value: '52835', entidades: [
                    {
                        nombre: 'JUZGADO 1 CIRCUITO ESPECIALIZADO EN RESTITUCIÓN DE TIERRAS DE TUMACO', value: '522-True-3121-52835-Juzgado de Circuito-Civil Especializado en Restitución de Tierras',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                ]
            },
            {
                nombre: 'TUNJA', value: '15001', entidades: [
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DEL BOYACÁ (ESCRITURAL)', value: '328-True-2331-15001-Tribunal Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE TUNJA - SALA LABORAL', value: '386-True-2205-15001-Tribunal Superior-Laboral',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE TUNJA -- SALA CIVIL - FAMILIA', value: '540-True-2213-15001-Tribunal Superior-Sala Civil-Familia',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DE TUNJA (ESCRITURAL)', value: '404-True-3331-15001-Juzgado Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES DEL CIRCUITO DE TUNJA', value: '230-True-3104-15001-Juzgado de Circuito-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS DE FAMILIA DEL CIRCUITO', value: '394-True-3110-15001-Juzgado de Circuito-Familia',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS 1 Y 4 CIVIL DEL CIRCUITO DE TUNJA', value: '420-True-3103-15001-Juzgado de Circuito-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS 2 Y 3 CIVIL DEL CIRCUITO DE TUNJA', value: '421-True-3103-15001-Juzgado de Circuito-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADO PRIMERO LABORAL DEL CIRCUITO', value: '389-True-3105-15001-Juzgado de Circuito-Laboral',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADO SEGUNDO LABORAL DEL CIRCUITO', value: '391-True-3105-15001-Juzgado de Circuito-Laboral',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADO TERCERO LABORAL DEL CIRCUITO', value: '392-True-3105-15001-Juzgado de Circuito-Laboral',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADO CUARTO LABORAL DEL CIRCUITO', value: '388-True-3105-15001-Juzgado de Circuito-Laboral',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS SISTEMA PENAL ACUSATORIO TUNJA', value: '200-True-4004-15001-Juzgado Municipal-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS 1, 2, 5, 7 CIVIL MUNICIPAL', value: '390-True-4003-15001-Juzgado Municipal-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS 3, 4, 6 CIVIL MUNICIPAL', value: '393-True-4003-15001-Juzgado Municipal-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS DE EJECUCION DE PENAS DE TUNJA', value: '238-True-3187-15001-Juzgado de Circuito-JUZGADO DE CIRCUITO -EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                ]
            },
            {
                nombre: 'VALLEDUPAR', value: '20001', entidades: [
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE VALLEDUPAR - SALA PENAL', value: '364-True-2204-20001-Tribunal Superior-Penal',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE VALLEDUPAR - SALA CIVIL FAMILIA LABORAL', value: '365-True-2214-20001-Tribunal Superior-Sala Civil- Familia - Laboral',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS (ESCRITURAL)', value: '332-True-3331-20001-Juzgado Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS 1, 2, 3 y 4 LABORALES DEL CIRCUITO', value: '331-True-3105-20001-Juzgado de Circuito-Laboral',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS 1 AL 3 CIRCUITO ESPECIALIZADO EN RESTITUCIÓN DE TIERRAS DE VALLEDUPAR', value: '528-True-3121-20001-Juzgado de Circuito-Civil Especializado en Restitución de Tierras',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADO 1 LABORAL DE PEQUEÑAS CAUSAS', value: '576-True-4105-20001-Juzgado de pequeñas causas-Laboral',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                ]
            },
            {
                nombre: 'VILLAVICENCIO', value: '50001', entidades: [
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DEL META (ESCRITURAL)', value: '327-True-2331-50001-Tribunal Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL ADMINISTRATIVO DEL META (ORAL)', value: '496-True-2333-50001-Tribunal Administrativo-Oralidad',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR SALA PENAL DE VILLAVICENCIO', value: '309-True-2204-50001-Tribunal Superior-Penal',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'TRIBUNAL SUPERIOR DE VILLAVICENCIO SALA CIVIL-FAMILIA-LABORAL', value: '307-True-2214-50001-Tribunal Superior-Sala Civil- Familia - Laboral',
                        data: { despacho: false, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS DE FAMILIA DEL CIRCUITO DE VILLAVICENCIO', value: '235-True-3110-50001-Juzgado de Circuito-Familia',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'CONSEJO SECCIONAL SALA DISCIPLINARIA DEL META', value: '308-True-1102-50001-Consejo Seccional de la Judicatura-Sala Disciplinaria',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES DEL CIRCUITO DE VILLAVICENCIO', value: '236-True-3104-50001-Juzgado de Circuito-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES MUNICIPALES DE VILLAVICENCIO', value: '240-True-4004-50001-Juzgado Municipal-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DEL CIRCUITO DE VILLAVICENCIO (ESCRITURAL)', value: '231-True-3331-50001-Juzgado Administrativo-SIN SECCIONES/ESCRITURAL',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS ADMINISTRATIVOS DEL CIRCUITO DE VILLAVICENCIO (ORAL)', value: '497-True-3333-50001-Juzgado Administrativo-Oralidad',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS PENALES ESPECIALIZADOS DEL CIRCUITO DE VILLAVICENCIO', value: '310-True-3107-50001-Juzgado de Circuito-Penal Especializado',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS LABORALES DEL CIRCUITO DE VILLAVICENCIO', value: '234-True-3105-50001-Juzgado de Circuito-Laboral',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES DEL CIRCUITO DE VILLAVICENCIO', value: '232-True-3103-50001-Juzgado de Circuito-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS CIVILES MUNICIPALES DE VILLAVICENCIO', value: '233-True-4003-50001-Juzgado Municipal-Civil',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADOS SISTEMA PENAL ACUSATORIO VILLAVICENCIO', value: '436-True-4004-50001-Juzgado Municipal-Penal',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADO 1 CIRCUITO ESPECIALIZADO EN RESTITUCIÓN DE TIERRAS DE VILLAVICENCIO', value: '525-True-3121-50001-Juzgado de Circuito-Civil Especializado en Restitución de Tierras',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                    {
                        nombre: 'JUZGADO 2 CIRCUITO ESPECIALIZADO EN RESTITUCIÓN DE TIERRAS DE VILLAVICENCIO', value: '557-True-3121-50001-Juzgado de Circuito-Civil Especializado en Restitución de Tierras',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                ]
            },
            {
                nombre: 'YOPAL', value: '85001', entidades: [
                    {
                        nombre: 'JUZGADO 1 CIRCUITO ESPECIALIZADO EN RESTITUCIÒN DE TIERRAS DE YOPAL', value: '512-True-3121-85001-Juzgado de Circuito-Civil Especializado en Restitución de Tierras',
                        data: { despacho: true, year: true, radicacion: true, consecutivo: true }
                    },
                ]
            },
        ]
            ;



    }

    getCiudades(): Promise<any> {
        return new Promise((resolve, reject) => {
            if (!this.ciudades) {
                this.ciudades = _.map(this.getData(), function (val: any) {
                    return { nombre: val.nombre, value: val.value };
                });

            }
            resolve(this.ciudades);
        });

    }

    getEntidades(ciudad: String): Promise<any> {
        return new Promise((resolve, reject) => {
            _.each(this.getData(), function (c: any) {
                if (c.value === ciudad) {
                    resolve(c.entidades);
                }
            });
            resolve([]);
        });
    }
}
