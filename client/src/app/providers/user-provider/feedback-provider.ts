/// <reference path="../../../../typings/index.d.ts" />

import { Injectable } from '@angular/core';

import { Http } from '@angular/http';

//import { DBProvider } from './db';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs';
import { UserProvider } from "../user-provider/user-provider";
import { ProviderUtils } from "../provider-utils";
var _ = require('lodash');

@Injectable()
export class FeebackProvider {

    constructor(public http: Http, private userProvider: UserProvider, private _utils: ProviderUtils) { }

    postFeedback(typeMessage, message, datos) {
        var user = this.userProvider.getCurrentUser();
        var email = user.email;
        return this.http.post('/api/users/feedback', { email: email, mensaje: message, data: datos, tipo: typeMessage })
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }

    postQuestion(contacto, captcha) {
        return this.http.post('/api/users/contact', { contacto, captcha })
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }
}