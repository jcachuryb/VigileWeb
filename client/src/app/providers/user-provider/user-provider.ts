/// <reference path="../../../../typings/index.d.ts" />

import { Injectable } from '@angular/core';

import { Http } from '@angular/http';

//import { DBProvider } from './db';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs';
import { ProviderUtils } from "../provider-utils";
import { GlobalState } from '../../global.state';


import { EventosFirmas, EventosUsuario } from "../../model/CambioEventos";
import { Firma, Usuario, Persona } from "../../model/Dtos";

var _ = require('lodash');

@Injectable()
export class UserProvider {

    private currentUser: any;
    private roles: any;

    constructor(public http: Http, private _utils: ProviderUtils, private _state: GlobalState) {

        this._state.subscribe(EventosUsuario.LOGOUT, datos => {
            this.currentUser = null;
        });
    }

    isLogged() {
        return this.http.post('/api/users/logged', null)
            .map(this._utils.extractData)
            .catch(this._utils.handleErrorObservable);
    }

    public loadUserData() {
        return this.http.post('/api/users/loadUserData', null)
            .map(res => {
                var body = res.json();
                this.currentUser = body.usuario;
                this.roles = body.roles;
                // this._state.notifyDataChanged(EventosUsuario.INFO_CARGADA, body);
                return body;
            })
            .catch(this._utils.handleErrorObservable);
    }

    public setCurrentUser(loggedUser) {
        this.currentUser = loggedUser;
    }

    getCurrentPerson() {

    }

    loadUserMenu() {
        return this.http.post('/api/users/loadUserMenu', null)
        .map(this._utils.extractData)
        .catch(this._utils.handleErrorObservable);
    }

    getCurrentUser() {
        return this.currentUser;
    }

    savePerson() {

    }

}